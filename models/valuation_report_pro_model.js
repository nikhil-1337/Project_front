const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const debug = require("debug")("http");


const vReportPro = new Schema({
  serial_no: { type: String },
  bankInfo: {
    sBankName: { type: String },
    sBranchName: { type: String }
  },
  generalDetals: {
    name_of_applicant: { type: String },
    property_address: { type: String },
    area_details: { type: String },
    proposed_buyer: { type: String },
    loan_application_number: { type: String },
    legal_address_of_property: { type: String },
    date_of_inspection: { type: Date },
    purpose_of_inspection: { type: String }
  },
  surrLocDetails: {
    ward_no: { type: String },
    vicinity: { type: String },
    plot_site: { type: String },
    pro_type: { type: String },
    conditions_of_road: { type: String },
    nearest_landmark: { type: String },
    local_transport: [String],
    local_transport_other: { type: String },
    pro_identified_through: { type: String }
  },
  propDetails: {
    usage_entire_pro: { type: String },
    additional_amenities: { type: String },
    type_of_premises: { type: String },
    occupied_by: { type: String },
    name_of_occupant: { type: String },
    property_rented: { type: String },
    if_relative: { type: String },
    if_rented: { type: String },
    Boundry: {
      north_boundarie: { type: String },
      south_boundarie: { type: String },
      east_boundarie: { type: String },
      west_boundarie: { type: String }
    }
  },
  structDetail: {
    type_of_structure: { type: String },
    no_of_floors: { type: String },
    no_of_dwelling: { type: String },
    no_of_wings: { type: String },
    no_of_lifts: { type: String },
    construction_year: { type: String },
    future_life: { type: String },
    age_of_property: { type: String },
    situated_floor_no: { type: String },
    quality_fittings: { type: String },
    Internal_composition: { type: String },
    flooring: { type: String }
  },
  QualityConst: {
    AExteriors: {
      beam_column: { type: String },
      appearance_maintenance: { type: String },
      common_remarks: { type: String }
    },
    BExteriors: {
      finishing: { type: String },
      roofing_terracing: { type: String },
      no_of_lift: { type: String }
    }
  },
  planApproval: {
    constr_per_approved: { type: String },
    details_approved_plans: { type: String },
    approval_date: { type: Date },
    use_permission: { type: String },
    violations_observed: { type: String },
    local_byelaws: { type: String }
  },
  valuation: {
    built_up_area: { type: String },
    loading_sbua: { type: String },
    approved_rates: { type: String },
    final_value: { type: String },
    recommended_rate: { type: String },
    land_area: { type: String },
    current_rates_for_land: { type: String },
    land_recommended_rate: { type: String },
    land_value: { type: String },
    bua_premises: { type: String },
    construction_cost: { type: String },
    total_value_const: { type: String },
    depreciate_value_const: { type: String },
    total_valuation: { type: String },
    say: { type: String }
  },
  remarks: { type: String },
  updated_at: { type: Date, default: Date.now },
  created_at: { type: Date, default: Date.now },
  images: { type: String }
});
module.exports = mongoose.model("vreportpro", vReportPro);
