const mongoose = require('mongoose')
const Schema = mongoose.Schema
const debug = require("debug")("http");


const Loan = new Schema({
  sName: { type: String },
  sPurpose: [String],
  dRecivedDate: { type: Date },
  dSentDate: { type: Date },
  sBankName: String,
  sBranchName: [String],
  sVisitDoneBy: { type: String },
  nFees: { type: Number },
  nTax: { type: Number },
  nTotalFees: { type: Number },
  ePaidunpaid: { type: String ,default:'Unpaid'},
  sCashOrCheque: { type: String },
  sRemarks: { type: String }
});
module.exports = mongoose.model('loan', Loan)
