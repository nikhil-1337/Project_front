const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const debug = require("debug")("http");


// user Schema
const Users = new Schema({
  sFullname: {
    type: String
  },
  sEmail: {
    type: String
  },
  sPassword: {
    type: String
  },
  sVerificationToken: { type: String },
  eUserType: {
    type: Boolean,
    default: false
  },
  eStatus: { type: String, enum: ["y", "n"], default: "n" },
  resetPasswordToken: String,
  sSecretToken: String,
  sDeviceToken: String,
  dCreatedDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("users", Users);

