const mongoose = require('mongoose')
const debug = require("debug")("http");


const Schema = mongoose.Schema
const Bank = new Schema({
  sBankName: String,
  // sBranch: 
  //   {
  //     sBranchName: [String],
  //     sBranchDetail: [String],
  //   }
  // ,
  sBankDetail: String,
  dCreatedDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});
module.exports = mongoose.model('bank', Bank)
