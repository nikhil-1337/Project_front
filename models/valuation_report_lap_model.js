const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const debug = require("debug")("http");


const vReportLap = new Schema({
  serial_no: { type: String },
  bankInfo: {
    sBankName: { type: String },
    sBranchName: { type: String }
  },
  generalInfo: {
    valuation_request: { type: String },
    property_occupied_by: { type: String },
    purpose_of_valuation: { type: String },
    date_of_valuation: { type: Date },
    valuation_done_by: { type: String },
    assessment_of_fmv: { type: String }
  },
  customerDetails: {
    customer_name: { type: String },
    application_no: { type: String },
    pro_purchaser_name: { type: String },
    person_met: { type: String }
  },
  propertyDetails: {
    address_of_property: { type: String },
    address_as_document: { type: String },
    nearby_landmark: { type: String },
    comment_on_property: { type: String }
  },
  documentDetails: {
    approved_layout_plan: { type: String },
    layout_date_num: { type: String },
    layout_authority: { type: String },
    approved_building_plan: { type: String },
    building_date_num: { type: String },
    building_authority: { type: String },
    construction_per: { type: String },
    construction_date_num: { type: String },
    construction_authority: { type: String },
    bu_permission: { type: String },
    bu_date_num: { type: String },
    bu_authority: { type: String },
    usage_as_plan: { type: String },
    type_of_usage: { type: String },
    document_list: { type: String }
  },
  occupancyDetail: {
    status_of_occupancy: { type: String },
    occupied_by: { type: String },
    relationship_with_customer: { type: String },
    occupied_since: { type: String },
    status_property: { type: String },
    rental_value: { type: String }
  },
  physicalDetail: {
    Boundaries: {
      north_doc: { type: String },
      north_site: { type: String },
      south_doc: { type: String },
      south_site: { type: String },
      east_doc: { type: String },
      east_site: { type: String },
      west_doc: { type: String },
      west_site: { type: String },
      boundaries_match: { type: String },
      boundaries_remark: { type: String }
    },
    Plot: {
      ploat_area: { type: String },
      flat_demarcated_site: { type: String },
      approved_use: { type: String },
      Infrastructure_area: { type: String },
      nature_of_locality: { type: String },
      class_of_locality: { type: String },
      distance_from_office: { type: String },
      property_location: { type: String },
      elec_service: { type: String },
      property_type: { type: String }
    },
    propDesc: {
      property_structure: { type: String },
      no_of_units: { type: String },
      no_of_kitchen: { type: String },
      no_of_bed: { type: String },
      usage_remark: { type: String },
      maintenance_level: { type: String },
      structure: { type: String },
      interiors: { type: String },
      exteriors: { type: String },
      age_of_property: { type: String },
      amenities: { type: String }
    }
  },
  Valuation: {
    areaDetailsA: {
      land_as_plan_a: { type: String },
      land_as_area_a: { type: String },
      ground_as_plan_a: { type: String },
      ground_as_area_a: { type: String },
      first_as_plan_a: { type: String },
      first_as_area_a: { type: String },
      second_as_plan_a: { type: String },
      second_as_area_a: { type: String },
      rate_as_plan_a: { type: String },
      rate_as_area_a: { type: String },
      rate_adopted_a: { type: String },
      cost_construction_a: { type: String },
      land_value_a: { type: String },
      building_value_a: { type: String },
      depreciation_considered_a: { type: String },
      fair_market_value_a: { type: String }
    },
    areaDetailsB: {
      land_as_plan_b: { type: String },
      land_as_area_b: { type: String },
      carpet_as_plan_b: { type: String },
      carpet_as_area_b: { type: String },
      area_detail_as_plan: { type: String },
      area_detail_as_area: { type: String },
      prevailing_rate: { type: String },
      total_value: { type: String },
      amenities_cost: { type: String },
      depreciation_building: { type: String },
      total_fair_value: { type: String },
      say: { type: String },
      remarks: { type: String }
    }
  },
  declaration: {
    plan: {
      plot_area_plan: { type: String },
      no_of_flore_plan: { type: String },
      units_area_plan: { type: String },
      built_area_plan: { type: String },
      ground_area_plan: { type: String },
      first_area_plan: { type: String },
      fsi_area_plan: { type: String },
      coverage_area_plan: { type: String },
      setbacks_area_plan: { type: String },
      front_area_plan: { type: String },
      rear_area_plan: { type: String },
      left_area_plan: { type: String },
      right_area_plan: { type: String }
    },
    site: {
      plot_area_site: { type: String },
      no_of_flore_site: { type: String },
      units_area_site: { type: String },
      built_area_site: { type: String },
      ground_area_site: { type: String },
      first_area_site: { type: String },
      fsi_area_site: { type: String },
      coverage_area_site: { type: String },
      setbacks_area_site: { type: String },
      front_area_site: { type: String },
      rear_area_site: { type: String },
      left_area_site: { type: String },
      right_area_site: { type: String }
    },
    deviation: {
      plot_area_deviation: { type: String },
      no_of_flore_deviation: { type: String },
      units_area_deviation: { type: String },
      built_area_deviation: { type: String },
      ground_area_deviation: { type: String },
      first_area_deviation: { type: String },
      fsi_area_deviation: { type: String },
      coverage_area_deviation: { type: String },
      setbacks_area_deviation: { type: String },
      front_area_deviation: { type: String },
      rear_area_deviation: { type: String },
      left_area_deviation: { type: String },
      right_area_deviation: { type: String }
    }
  },
  images: { type: String },
  updated_at: { type: Date, default: Date.now },
  created_at: { type: Date, default: Date.now }
});
module.exports = mongoose.model("vreportlap", vReportLap);
