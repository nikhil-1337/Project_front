const mongoose = require('mongoose')
const Schema = mongoose.Schema
const debug = require("debug")("http");


const vReport = new Schema({
  serial_no: { type: String },
  bankInfo: {
    sBankName: { type: String },
    sBranchName: { type: String }
  },
  generalInfo: {
    name_of_customer: { type: String },
    customer_id: { type: String },
    purpose_of_valuation: { type: String },
    location: { type: String },
    date_of_valuation: { type: Date },
    estimated_market_value: { type: String },
    documents_provided: [String],
    documents_provided_other: { type: String }
  },
  propertyDetails: {
    revenue_survey_no: { type: String },
    fp_no: { type: String },
    tps_no: { type: String },
    op_no: { type: String },
    flat_no: { type: String },
    mouje: { type: String },
    society_name: { type: String },
    landmark: { type: String },
    taluka: { type: String },
    district: { type: String },
    local_transport: [String],
    local_transport_other: { type: String },
    distance_to_city_center: { type: String },
    class_of_locality: { type: String },
    quality_of_infrastructure: { type: String },
    Boundry: {
      north_boundarie: { type: String },
      south_boundarie: { type: String },
      east_boundarie: { type: String },
      west_boundarie: { type: String }
    },
    boundaries_in_doc: { type: Boolean },
    topographical_condition: { type: String },
    status_of_land: { type: String },
    type_of_property: { type: String },
    approved_usage: { type: String },
    actual_usage: { type: String },
    type_of_structure: { type: String },
    no_of_floors: { type: String },
    occupancy_details: { type: String },
    property_connection: { type: String },
    proximity: { type: String },
    development_area: { type: String },
    approval_details: { type: String },
    construction_details: { type: String },
    usages_of_pro: { type: String },
    building_plan: { type: String },
    side_margin: { type: String },
    front_margin: { type: String },
    quality_of_construction: { type: String },
    maintenance_of_property: { type: String },
    current_life_structure: { type: String },
    estimated_life_structure: { type: String }
  },
  RecomValuation: {
    stage_of_const: { type: String },
    work_complited: { type: String },
    disbursement_recommended: { type: String },
    date_of_pro_visit: { type: Date },
    government_rates: { type: String },
    valuation_property: { type: String },
    rent_value: { type: String },
    recommended_rate_flat: { type: String },
    values: {
      value_of_flat: { type: String },
      value_of_terrace: { type: String },
      total_value: { type: String },
      say: { type: String }
    }
  },

  remarks: { type: String },
  images: { type: String },
  google_map: { type: String },
  google_map2: { type: String },
  updated_at: { type: Date, default: Date.now },
  created_at: { type: Date, default: Date.now }
});
module.exports = mongoose.model('vReportA', vReport)
