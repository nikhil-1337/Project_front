const mongoose = require('mongoose')
const debug = require("debug")("http");
const Schema = mongoose.Schema
const Branch = new Schema({
  sBank: Schema.Types.ObjectId,
  sBranchName: String,
  sBranchDetail: String,
  dCreatedDate: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});
module.exports = mongoose.model('branch', Branch)
