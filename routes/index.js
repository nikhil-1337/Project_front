const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");
const PDFDocument = require("pdfkit");
const crypto = require("crypto");
const router = express.Router();
const debug = require("debug")("http");


/* Middleware functions for auth */
const authorization = require("./../middleware/authorization");
const { isAdminOnly } = require("./../middleware/authorization");
const { ensureAuthenticated } = require("./../middleware/authorization");
const { isNotAuthenticated } = require("./../middleware/authorization");

/**Service call bank loan & report */
const userService = require("../services/userSer");
const bankSer = require("../services/addBank");
const loanSer = require("../services/loanSer");
const valA = require("../services/valuationReportA");
const valB = require("../services/valuationReportB");
const valLap = require("../services/valuationReportLap");
const loanPdf = require("../services/loanPdf")
const authenticationService = require("../services/authentication");
require("../services/passport")(passport);

/**Models importing */
const Bank = require("../models/bank_model");
const Branch = require("../models/branch_model");

const User = require("../models/user");
const Loan = require("../models/loan_model");
const ReportA = require("../models/valuation_report_model");
const ReportB = require("../models/valuation_report_pro_model");
const ReportLap = require("../models/valuation_report_lap_model");

/**User Services start */
// Get Login
router.get("/login", isNotAuthenticated, (req, res) => {
  res.render("users/login",{
    title: "Login"
  });
});

// Get Registraiton page
router.get("/register", isNotAuthenticated, (req, res) => {
  res.render("users/register",{
    title: "Register"
  });
});
// Post Registeration page -- service call create user
router.post("/register", userService.createUser);

// service call email varificaiton
router.get("/verification/:token", authenticationService.emailVerification);
router.get("/forgotpass", authenticationService.emailVerification);

// auth Passport verification
router.post("/authenticate", (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/login",
    failureFlash: true
  })(req, res, next);
});

router.get("/forgot", (req, res) => {
  res.render("users/forgot", {
    user: req.user,
    title:"Forgot Password"
  });
});
router.post("/forgotpass", authenticationService.forgotPass);
router.get("/reset/:token", authenticationService.resetTokenVerification);
router.post("/reset/:token", authenticationService.resetPass);

router.post("/updateProfile", ensureAuthenticated, userService.updateProfile);
/*****************************Basic user services end here*********************************/
/* Add Bank,update Bank, Delete Bank */

// Dashboard
router.get("/", ensureAuthenticated, (req, res) => {
  res.render("dashboard", {
    admin: req.user.eUserType,
    user: req.user,
    title:"Dashboard"
  });
});

router.get("/dashboard", ensureAuthenticated, (req, res) => {
  res.render("dashboard", {
    admin: req.user.eUserType,
    user: req.user,
    title: "Dashboard"
  });
});

// Get Bank data
router.get("/bank_data", isAdminOnly, ensureAuthenticated, (req, res) => {
  Bank.find({}).then(function(bank) {
    console.log(bank);
    res.render("bank_data", {
      user: req.user,
      admin: req.user.eUserType,
      bank: bank,
      title: "Bank Data"
    });
  });
});
/**Get Add Bank Data */
router.get("/add_bank", isAdminOnly, ensureAuthenticated, (req, res) => {
  res.render("add_bank", {
    user: req.user,
    admin: req.user.eUserType,
    title: "Add Data"

  });
});

/**Add Bank */
router.post("/add_bank", ensureAuthenticated, isAdminOnly, bankSer.addBank);

/** Get Edit Bank Data */
router.get("/bank_edit/:id", isAdminOnly, ensureAuthenticated, (req, res) => {
  Bank.findById(req.params.id, function(err, bank) {
    if (err) {
      if (err.kind === "ObjectId") {
        req.flash("error_msg", "Not Found with " + req.params.id);
        res.redirect("/bank_data");
      }
    } else
      res.render("bank_edit", {
        user: req.user,
        admin: req.user.eUserType,
        bank: bank,
        title: "Bank Edit"
      });
  });
});

/**Chang Edited Data */
router.put(
  "/bank_edit/:id",
  isAdminOnly,
  ensureAuthenticated,
  bankSer.editBank
);
/**Delete Bank Data */
router.delete(
  "/deleteBank/:id",
  isAdminOnly,
  ensureAuthenticated,
  bankSer.deleteBank
);
/* Branch Add, Update, Delete */
router.get("/branch_data", isAdminOnly, ensureAuthenticated, (req, res) => {
  Branch.aggregate([
    {
      $lookup: {
        from: "banks",
        localField: "sBank",
        foreignField: "_id",
        as: "bank"
      }
    }, {
      $unwind: "$bank"
    }, {
      $project: { "bank.sBankName": 1, data: "$$ROOT" }
    }
  ]).then(function(data) {
    console.log("::::::::::", data);
    res.render("branch_data", {
      user: req.user,
      admin: req.user.eUserType,
      data:data,
      title: "Branch Data"

    });
  });
});
router.get("/add_bank_branch", isAdminOnly, ensureAuthenticated, (req, res) => {
  Bank.find({}).then(function(bank) {
    res.render("add_bank_branch", {
      user: req.user,
      admin: req.user.eUserType,
      bank: bank,
      title: "Add Branch"

    });
  });
});
router.get("/branch_edit/:id", isAdminOnly, ensureAuthenticated, (req, res) => {
  console.log("branch_edit:id_get", req.params.id);
  Branch.findById(req.params.id, function(err, bank) {
    if (err) {
      if (err.kind === "ObjectId") {
        req.flash("error_msg", "Not Found with " + req.params.id);
        res.redirect("/bank_data");
      }
    } else
      res.render("branch_edit", {
        user: req.user,
        admin: req.user.eUserType,
        bank: bank,
        title: "Branch Edit"
      });
  });
});
router.post(
  "/add_bank_branch",
  isAdminOnly,
  ensureAuthenticated,
  bankSer.addBranch
);
router.put(
  "/branch_edit/:id",
  isAdminOnly,
  ensureAuthenticated,
  bankSer.editBranch
);
router.delete(
  "/deleteBranch/:id",
  isAdminOnly,
  ensureAuthenticated,
  bankSer.deleteBranch
);
/* User add update delete by admin */
router.get("/user_data", ensureAuthenticated,isAdminOnly, (req, res) => {
  User.find({
    eUserType: false
  }).then(function(user) {
    res.render("user_data", {
      user: req.user,
      admin: req.user.eUserType,
      users: user,
      title: "User Data"

    });
  });
});
router.get("/profile", ensureAuthenticated, (req, res) => {
  res.render("profile", {
    user: req.user,
    admin: req.user.eUserType,
    title: "Profile"

  });
});
router.delete(
  "/deleteUser/:id",
  isAdminOnly,
  ensureAuthenticated,
  userService.deleteUser
);
router.get(
  "/makeAdmin/:id",
  isAdminOnly,
  ensureAuthenticated,
  userService.makeAdmin
);
/* Loan client form */
router.get("/loan_form", ensureAuthenticated, (req, res) => {
  Bank.find({}).then(function(bank) {
    res.render("loan_form", {
      user: req.user,
      admin: req.user.eUserType,
      bank: bank,
      title: "Loan Form"

    });
  });
});
router.get("/getBank", ensureAuthenticated, (req, res) => {
  Bank.find(
    {},
    {
      sBankName: 1
    }
  ).then(function(data) {
    console.log(data);
    res.send(data);
  });
});
router.post("/getBranch", ensureAuthenticated, (req, res) => {
  console.log(req.body);
  Bank.findOne({
    sBankName: req.body.sBankName
  },(error,data2)=>{
    Branch.find({sBank:data2._id},(error,data)=>{
      res.send(data);
    })
  })
});
router.post("/loan_form", ensureAuthenticated, loanSer.addLoan);

router.get(
  "/loan_client_data",
  isAdminOnly,
  ensureAuthenticated,
  (req, res) => {
    Loan.find({}).then(function(loan) {
      res.render("loan_client_data", {
        user: req.user,
        admin: req.user.eUserType,
        loan: loan,
        title:"Loan Client Data"
      });
    });
  }
);
router.get("/loan_client_edit", ensureAuthenticated, (req, res) => {
  res.render("loan_client_edit", {
    user: req.user,
    admin: req.user.eUserType,
    title: "Loan Client Edit"
  });
});
router.delete(
  "/deleteLoan/:id",
  isAdminOnly,
  ensureAuthenticated,
  loanSer.deleteLoan
);
router.get(
  "/loan_client_edit/:id",
  isAdminOnly,
  ensureAuthenticated,
  (req, res) => {
    Loan.findById(req.params.id, function(err, loan) {
      if (err) {
        if (err.kind === "ObjectId") {
          req.flash("error_msg", "Not Found with " + req.params.id);
          res.redirect("/loan_client_data");
        }
      } else
        res.render("loan_client_edit", {
          user: req.user,
          admin: req.user.eUserType,
          loan: loan,
          title:"Loan Client Edit"
        });
    });
  }
);
router.post("/loan_client_edit/:id", ensureAuthenticated, loanSer.editLoan);

/* Valuation Report A */
router.get("/valuation_report_a", ensureAuthenticated, (req, res) => {
  res.render("valuation_report_a", {
    user: req.user,
    admin: req.user.eUserType,
    token: Math.floor(Math.random() * Date.now()),
    title:"Report A"

  });
});
router.post(
  "/valuation_report_a",
  ensureAuthenticated,
  valA.valuation_report_a
);
router.get("/valuation_report_a_view", ensureAuthenticated, (req, res) => {
  ReportA.find({}).then(function(reportA) {
    res.render("valuation_report_a_view", {
      user: req.user,
      admin: req.user.eUserType,
      reportA: reportA,
      title:"Report A - Data"
    });
  });
});
router.get("/valuation_report_a_edit/:id", ensureAuthenticated, (req, res) => {
  ReportA.findById(req.params.id, function(err, reportA) {
    if (err) {
      if (err.kind === "ObjectId") {
        req.flash("error_msg", "Not Found with " + req.params.id);
        res.redirect("/");
      }
    } else
      res.render("valuation_report_a_edit", {
        user: req.user,
        admin: req.user.eUserType,
        reportA: reportA,
        title:"Report A - Edit"
      });
  });
});
router.delete("/valuation_report_a/:id", ensureAuthenticated, valA.DeleteA);
router.put("/valuation_report_a_edit/:id", ensureAuthenticated, valA.editA);

/**
 * Valuation Report B
 */
router.post(
  "/valuation_report_b",
  ensureAuthenticated,
  valB.valuation_report_b
);
router.get("/valuation_report_b", ensureAuthenticated, (req, res) => {
  res.render("valuation_report_b", {
    user: req.user,
    admin: req.user.eUserType,
    token: Math.floor(Math.random() * Date.now()),
    title: "Report B"
  });
});
router.post(
  "/valuation_report_b",
  ensureAuthenticated,
  valB.valuation_report_b
);
router.get("/valuation_report_b_view", ensureAuthenticated, (req, res) => {
  ReportB.find({}).then(function(reportB) {
    res.render("valuation_report_b_view", {
      user: req.user,
      admin: req.user.eUserType,
      reportB: reportB,
      title: "Report B - Data"
    });
  });
});
router.get("/valuation_report_b_edit/:id", ensureAuthenticated, (req, res) => {
  ReportB.findById(req.params.id, function(err, reportB) {
    if (err) {
      if (err.kind === "ObjectId") {
        req.flash("error_msg", "Not Found with " + req.params.id);
        res.redirect("/");
      }
    } else
      res.render("valuation_report_b_edit", {
        user: req.user,
        admin: req.user.eUserType,
        reportB: reportB,
        title: "Report B - Edit"
      });
  });
});
router.delete("/valuation_report_b/:id", ensureAuthenticated, valB.DeleteB);
router.put("/valuation_report_b_edit/:id", ensureAuthenticated, valB.editB);

/**
 * Valuation report LAP
 */
router.get("/valuation_report_lap", ensureAuthenticated, (req, res) => {
  res.render("valuation_report_lap", {
    user: req.user,
    admin: req.user.eUserType,
    token: Math.floor(Math.random() * Date.now()),
    title: "Report Lap"
  });
});
router.post(
  "/valuation_report_lap",
  ensureAuthenticated,
  valLap.valuation_report_lap
);
router.get("/valuation_report_lap_view", ensureAuthenticated, (req, res) => {
  ReportLap.find({}).then(function(reportLap) {
    res.render("valuation_report_lap_view", {
      user: req.user,
      admin: req.user.eUserType,
      reportLap: reportLap,
      title: "Report Lap - View"
    });
  });
});

router.get(
  "/valuation_report_lap_edit/:id",
  ensureAuthenticated,
  (req, res) => {
    ReportLap.findById(req.params.id, function(err, reportLap) {
      if (err) {
        if (err.kind === "ObjectId") {
          req.flash("error_msg", "Not Found with " + req.params.id);
          res.redirect("/");
        }
      } else
        res.render("valuation_report_lap_edit", {
          user: req.user,
          admin: req.user.eUserType,
          reportLap: reportLap,
          title: "Report Lap - Edit"
        });
    });
  }
);
router.delete(
  "/valuation_report_lap/:id",
  ensureAuthenticated,
  valLap.DeleteLap
);
router.put(
  "/valuation_report_lap_edit/:id",
  ensureAuthenticated,
  valLap.editLap
);

router.get("/reports_data", ensureAuthenticated, (req, res) => {
  const data = [];
  ReportA.find({}).then(function(a) {});

  res.render("reports_data", {
    user: req.user,
    admin: req.user.eUserType
  });
});

router.get("/getUnpaid", ensureAuthenticated, (req, res) => {
  Loan.aggregate([
    {
      $group: {
        _id: "$ePaidunpaid",
        data: {
          $push: "$$ROOT"
        },
        count: {
          $sum: 1
        }
      }
    }
  ])
    .then(function(data) {
      console.log(data);
      var d = {
        ...data
      };
      const unpaid = d[0].data;
      res.send({
        admin: req.user.eUserType,
        user: req.user,
        unpaid: unpaid,
        paidTotal: d[1].count,
        unpaidTotal: d[0].count,
        total: d[0].count + d[1].count
      });
    })
    .catch(console.error());
});

router.get("/pdfLoanData/:id", ensureAuthenticated, loanPdf.loanPdf)

router.get("/pdfA/:id",ensureAuthenticated,loanPdf.reportApdf)
router.get("/pdfB/:id", ensureAuthenticated, loanPdf.reportBpdf)
router.get("/pdfC/:id", ensureAuthenticated, loanPdf.reportLapPdf);

router.get("/logout", ensureAuthenticated, authenticationService.logout);
module.exports = router;