const User = require("../models/user");
const mongoose = require("mongoose");
const Bank = require("../models/bank_model");
const cli = require("../config/cli").console;
var bodyParser = require("body-parser");
const debug = require("debug")("http");

const Branch = require("../models/branch_model")
mongoose.Promise = Promise;

const addBank = (req, res) => {
  debug(req.body)
  let errors = [];
  if (errors.length > 0) {
    res.render("add_bank", {
      errors: errors
    });
  } else {
    Bank.findOne({
      sBankName: req.body.sBankName
    }).then(bank => {
      if (bank) {
        req.flash("error", "Bank Already Exists");
        cli.red("Bank already Exists.");
        res.redirect("add_bank");
      } else {
        const bank = new Bank({
          sBankName: req.body.sBankName,
          sBankDetail: req.body.sBankDetail
        });
        bank.save();
        req.flash("success_msg", "Bank Added");
        cli.green("Bank Added");
        res.redirect("add_bank");
      }
    });
  }
};

const editBank = (req, res) => {
  req.checkBody("sBankName", "Bank Name Required!").notEmpty();
  req.checkBody("sBankDetail", "Bank Detail Required").notEmpty();
  req.getValidationResult().then(function(result) {
    var errors = result.useFirstErrorOnly().mapped();
    if (result.array().length > 0) {
      res.redirect("/bank_data", {
        errors: errors,
        sBankName: req.body.sBankName,
        sBankDetail: req.body.sBankDetail
      });
    } else {
      Bank.findOne({
        _id: req.params.id
      }).then(function(bank) {
        bank.sBankName = req.body.sBankName;
        bank.sBankDetail = req.body.sBankDetail;
        bank.save(function(err, data) {
          if (err) {
            res.redirect("/bank_data", {
              err: err,
              sBankName: req.body.sBankName,
              sBankDetail: req.body.sBankDetail
            });
          } else {
            req.flash("success_msg", "Bank Edited successfully.");
            res.status(200).redirect("/bank_data");
          }
        });
      });
    }
  });
};

const deleteBank = (req, res) => {
  console.log("Bank_data:id_delete", req.params.id);
  Bank.remove({
    _id: req.params.id
  }).then(() => {
    req.flash("success_msg", "Bank removed");
    res.redirect("/bank_data");
  });
};

const addBranch = (req, res) => {
   debug(req.body);
   Bank.findOne(
     {
       sBankName: req.body.sBankName
     },
     (error, data) => {
       console.log(data._id);
      var body = {
        sBank:data._id,
        sBranchName: req.body.sBranchName,
        sBranchDetail: req.body.sBranchDetail
      }       
      Branch.findOne({sBankName:req.body.sBranchName},(error,data)=>{
        if(data){
          req.flash("error_msg", "Branch Already Exists");
          res.redirect("add_bank_branch");
        } else {
          var newData = new Branch(body);
          newData.save((error, success) => {
            req.flash("success_msg", "Branch Added");
            res.redirect("add_bank_branch");
          });
        }
      })
     }
   );

  }


const editBranch = (req, res) => {
  req.checkBody("sBranchName", "Branch Name Required!").notEmpty();
  req.checkBody("sBranchDetail", "Branch Detail Required").notEmpty();
  req.getValidationResult().then(function(result) {
    var errors = result.array();
    if (result.array().length > 0) {
      res.redirect("/branch_data", {
        errors: errors,
        sBranchName: req.body.sBranchName,
        sBranchDetail: req.body.sBranchDetail
      });
    } else {
      Bank.findOne(
        {
          sBankName: req.body.sBankName
        },
        (error, data2) => {
        
          Branch.findOne({ _id: req.params.id }, (error, data) => {
            data.sBank = data2._id
            data.sBranchName = req.body.sBranchName;
            data.sBranchDetail = req.body.sBranchDetail;
              data.save((error, success) => {
                req.flash("success_msg", "Branch Updated");
                res.redirect("/branch_data");
              });
            
          })
        }
      );

    }
  });
};

const deleteBranch = (req, res) => {
  console.log("Bank_data:id_delete", req.params.id);
  Branch.remove({
    _id: req.params.id
  }).then(() => {
    req.flash("success_msg", "Branch removed");
    res.redirect("/branch_data");
  });
};

module.exports = {
  addBank,
  editBank,
  deleteBank,

  addBranch,
  editBranch,
  deleteBranch
};
