const User = require("../models/user");
const mongoose = require("mongoose");
const Bank = require("../models/bank_model");
const cli = require("../config/cli").console;
var bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator/check");
const RepoartLap = require("../models/valuation_report_lap_model");
const debug = require("debug")("http");


mongoose.Promise = Promise;
const multer = require("multer");
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/uploads/c");
  },
  filename: function(req, file, cb) {
    var datetimestamp = Date.now() + Math.random();
    cb(
      null,
      file.fieldname +
        "-" +
        datetimestamp +
        "." +
        file.originalname.split(".")[file.originalname.split(".").length - 1]
    );
  }
});

var upload = multer({
  storage: storage
}).fields([
  {
    name: "files",
    maxCount: 1
  }
]);

const valuation_report_lap = (req, res, next) => {
  upload(req, res, function(err) {
    req
      .checkBody("serial_no")
      .notEmpty()
      .withMessage("serial_no Required..");
    req
      .checkBody("sBankName")
      .notEmpty()
      .withMessage("sBankName Required..");
    req
      .checkBody("sBranchName")
      .notEmpty()
      .withMessage("sBranchName Required..");
    req
      .checkBody("valuation_request")
      .notEmpty()
      .withMessage("valuation_request Required..");
    req
      .checkBody("property_occupied_by")
      .notEmpty()
      .withMessage("property_occupied_by Required..");
    req
      .checkBody("purpose_of_valuation")
      .notEmpty()
      .withMessage("purpose_of_valuation Required..");
    req
      .checkBody("date_of_valuation")
      .notEmpty()
      .withMessage("date_of_valuation Required..");
    req
      .checkBody("valuation_done_by")
      .notEmpty()
      .withMessage("valuation_done_by Required..");
    req
      .checkBody("assessment_of_fmv")
      .notEmpty()
      .withMessage("assessment_of_fmv Required..");
    req
      .checkBody("customer_name")
      .notEmpty()
      .withMessage("customer_name Required..");
    req
      .checkBody("application_no")
      .notEmpty()
      .withMessage("application_no Required..");
    req
      .checkBody("pro_purchaser_name")
      .notEmpty()
      .withMessage("pro_purchaser_name Required..");
    req
      .checkBody("person_met")
      .notEmpty()
      .withMessage("person_met Required..");
    req
      .checkBody("address_of_property")
      .notEmpty()
      .withMessage("address_of_property Required..");
    req
      .checkBody("address_as_document")
      .notEmpty()
      .withMessage("address_as_document Required..");
    req
      .checkBody("nearby_landmark")
      .notEmpty()
      .withMessage("nearby_landmark Required..");
    req
      .checkBody("comment_on_property")
      .notEmpty()
      .withMessage("comment_on_property Required..");
    req
      .checkBody("approved_layout_plan")
      .notEmpty()
      .withMessage("approved_layout_plan Required..");
    req
      .checkBody("layout_date_num")
      .notEmpty()
      .withMessage("layout_date_num Required..");
    req
      .checkBody("layout_authority")
      .notEmpty()
      .withMessage("layout_authority Required..");

    req
      .checkBody("construction_per")
      .notEmpty()
      .withMessage("construction_per Required..");
    req;

    req
      .checkBody("bu_permission")
      .notEmpty()
      .withMessage("bu_permission Required..");

    req
      .checkBody("usage_as_plan")
      .notEmpty()
      .withMessage("usage_as_plan Required..");
    req
      .checkBody("type_of_usage")
      .notEmpty()
      .withMessage("type_of_usage Required..");
    req
      .checkBody("document_list")
      .notEmpty()
      .withMessage("document_list Required..");
    req
      .checkBody("status_of_occupancy")
      .notEmpty()
      .withMessage("status_of_occupancy Required..");
    req
      .checkBody("occupied_by")
      .notEmpty()
      .withMessage("occupied_by Required..");
    req
      .checkBody("relationship_with_customer")
      .notEmpty()
      .withMessage("relationship_with_customer Required..");
    req
      .checkBody("occupied_since")
      .notEmpty()
      .withMessage("occupied_since Required..");
    req
      .checkBody("status_property")
      .notEmpty()
      .withMessage("status_property Required..");
    req
      .checkBody("rental_value")
      .notEmpty()
      .withMessage("rental_value Required..");
    req
      .checkBody("north_doc")
      .notEmpty()
      .withMessage("north_doc Required..");
    req
      .checkBody("north_site")
      .notEmpty()
      .withMessage("north_site Required..");
    req
      .checkBody("south_doc")
      .notEmpty()
      .withMessage("south_doc Required..");
    req
      .checkBody("south_site")
      .notEmpty()
      .withMessage("south_site Required..");
    req
      .checkBody("east_doc")
      .notEmpty()
      .withMessage("east_doc Required..");
    req
      .checkBody("east_site")
      .notEmpty()
      .withMessage("east_site Required..");
    req
      .checkBody("west_doc")
      .notEmpty()
      .withMessage("west_doc Required..");
    req
      .checkBody("west_site")
      .notEmpty()
      .withMessage("west_site Required..");
    req
      .checkBody("boundaries_match")
      .notEmpty()
      .withMessage("boundaries_match Required..");
    req
      .checkBody("boundaries_remark")
      .notEmpty()
      .withMessage("boundaries_remark Required..");
    req
      .checkBody("ploat_area")
      .notEmpty()
      .withMessage("ploat_area Required..");
    req
      .checkBody("flat_demarcated_site")
      .notEmpty()
      .withMessage("flat_demarcated_site Required..");
    req
      .checkBody("approved_use")
      .notEmpty()
      .withMessage("approved_use Required..");
    req
      .checkBody("Infrastructure_area")
      .notEmpty()
      .withMessage("Infrastructure_area Required..");
    req
      .checkBody("nature_of_locality")
      .notEmpty()
      .withMessage("nature_of_locality Required..");
    req
      .checkBody("class_of_locality")
      .notEmpty()
      .withMessage("class_of_locality Required..");
    req
      .checkBody("distance_from_office")
      .notEmpty()
      .withMessage("distance_from_office Required..");
    req
      .checkBody("property_location")
      .notEmpty()
      .withMessage("property_location Required..");
    req
      .checkBody("elec_service")
      .notEmpty()
      .withMessage("elec_service Required..");
    req
      .checkBody("property_type")
      .notEmpty()
      .withMessage("property_type Required..");
    req
      .checkBody("property_structure")
      .notEmpty()
      .withMessage("property_structure Required..");
    req
      .checkBody("no_of_units")
      .notEmpty()
      .withMessage("no_of_units Required..");
    req
      .checkBody("no_of_kitchen")
      .notEmpty()
      .withMessage("no_of_kitchen Required..");
    req
      .checkBody("no_of_bed")
      .notEmpty()
      .withMessage("no_of_bed Required..");
    req
      .checkBody("usage_remark")
      .notEmpty()
      .withMessage("usage_remark Required..");
    req
      .checkBody("maintenance_level")
      .notEmpty()
      .withMessage("maintenance_level Required..");
    req
      .checkBody("structure")
      .notEmpty()
      .withMessage("structure Required..");
    req
      .checkBody("interiors")
      .notEmpty()
      .withMessage("interiors Required..");
    req
      .checkBody("exteriors")
      .notEmpty()
      .withMessage("exteriors Required..");
    req
      .checkBody("age_of_property")
      .notEmpty()
      .withMessage("age_of_property Required..");
    req
      .checkBody("amenities")
      .notEmpty()
      .withMessage("amenities Required..");
    req
      .checkBody("land_as_plan_a")
      .notEmpty()
      .withMessage("land_as_plan_a Required..");
    req
      .checkBody("land_as_area_a")
      .notEmpty()
      .withMessage("land_as_area_a Required..");
    req
      .checkBody("ground_as_plan_a")
      .notEmpty()
      .withMessage("ground_as_plan_a Required..");
    req
      .checkBody("ground_as_area_a")
      .notEmpty()
      .withMessage("ground_as_area_a Required..");
    req
      .checkBody("first_as_plan_a")
      .notEmpty()
      .withMessage("first_as_plan_a Required..");
    req
      .checkBody("first_as_area_a")
      .notEmpty()
      .withMessage("first_as_area_a Required..");
    req
      .checkBody("second_as_plan_a")
      .notEmpty()
      .withMessage("second_as_plan_a Required..");
    req
      .checkBody("second_as_area_a")
      .notEmpty()
      .withMessage("second_as_area_a Required..");
    req
      .checkBody("rate_as_plan_a")
      .notEmpty()
      .withMessage("rate_as_plan_a Required..");
    req
      .checkBody("rate_as_area_a")
      .notEmpty()
      .withMessage("rate_as_area_a Required..");
    req
      .checkBody("rate_adopted_a")
      .notEmpty()
      .withMessage("rate_adopted_a Required..");
    req
      .checkBody("cost_construction_a")
      .notEmpty()
      .withMessage("cost_construction_a Required..");
    req
      .checkBody("land_value_a")
      .notEmpty()
      .withMessage("land_value_a Required..");
    req
      .checkBody("building_value_a")
      .notEmpty()
      .withMessage("building_value_a Required..");
    req
      .checkBody("depreciation_considered_a")
      .notEmpty()
      .withMessage("depreciation_considered_a Required..");
    req
      .checkBody("fair_market_value_a")
      .notEmpty()
      .withMessage("fair_market_value_a Required..");
    req
      .checkBody("land_as_plan_b")
      .notEmpty()
      .withMessage("land_as_plan_b Required..");
    req
      .checkBody("land_as_area_b")
      .notEmpty()
      .withMessage("land_as_area_b Required..");
    req
      .checkBody("carpet_as_plan_b")
      .notEmpty()
      .withMessage("carpet_as_plan_b Required..");
    req
      .checkBody("carpet_as_area_b")
      .notEmpty()
      .withMessage("carpet_as_area_b Required..");
    req
      .checkBody("area_detail_as_plan")
      .notEmpty()
      .withMessage("area_detail_as_plan Required..");
    req
      .checkBody("area_detail_as_area")
      .notEmpty()
      .withMessage("area_detail_as_area Required..");
    req
      .checkBody("prevailing_rate")
      .notEmpty()
      .withMessage("prevailing_rate Required..");
    req
      .checkBody("total_value")
      .notEmpty()
      .withMessage("total_value Required..");
    req
      .checkBody("amenities_cost")
      .notEmpty()
      .withMessage("amenities_cost Required..");
    req
      .checkBody("depreciation_building")
      .notEmpty()
      .withMessage("depreciation_building Required..");
    req
      .checkBody("total_fair_value")
      .notEmpty()
      .withMessage("total_fair_value Required..");
    req
      .checkBody("say")
      .notEmpty()
      .withMessage("say Required..");
    req
      .checkBody("remarks")
      .notEmpty()
      .withMessage("remarks Required..");
    req
      .checkBody("plot_area_plan")
      .notEmpty()
      .withMessage("plot_area_plan Required..");
    req
      .checkBody("no_of_flore_plan")
      .notEmpty()
      .withMessage("no_of_flore_plan Required..");
    req
      .checkBody("units_area_plan")
      .notEmpty()
      .withMessage("units_area_plan Required..");
    req
      .checkBody("built_area_plan")
      .notEmpty()
      .withMessage("built_area_plan Required..");
    req
      .checkBody("ground_area_plan")
      .notEmpty()
      .withMessage("ground_area_plan Required..");
    req
      .checkBody("first_area_plan")
      .notEmpty()
      .withMessage("first_area_plan Required..");
    req
      .checkBody("fsi_area_plan")
      .notEmpty()
      .withMessage("fsi_area_plan Required..");
    req
      .checkBody("coverage_area_plan")
      .notEmpty()
      .withMessage("coverage_area_plan Required..");
    req
      .checkBody("setbacks_area_plan")
      .notEmpty()
      .withMessage("setbacks_area_plan Required..");
    req
      .checkBody("front_area_plan")
      .notEmpty()
      .withMessage("front_area_plan Required..");
    req
      .checkBody("rear_area_plan")
      .notEmpty()
      .withMessage("rear_area_plan Required..");
    req
      .checkBody("left_area_plan")
      .notEmpty()
      .withMessage("left_area_plan Required..");
    req
      .checkBody("right_area_plan")
      .notEmpty()
      .withMessage("right_area_plan Required..");
    req
      .checkBody("plot_area_site")
      .notEmpty()
      .withMessage("plot_area_site Required..");
    req
      .checkBody("no_of_flore_site")
      .notEmpty()
      .withMessage("no_of_flore_site Required..");
    req
      .checkBody("units_area_site")
      .notEmpty()
      .withMessage("units_area_site Required..");
    req
      .checkBody("built_area_site")
      .notEmpty()
      .withMessage("built_area_site Required..");
    req
      .checkBody("ground_area_site")
      .notEmpty()
      .withMessage("ground_area_site Required..");
    req
      .checkBody("first_area_site")
      .notEmpty()
      .withMessage("first_area_site Required..");
    req
      .checkBody("fsi_area_site")
      .notEmpty()
      .withMessage("fsi_area_site Required..");
    req
      .checkBody("coverage_area_site")
      .notEmpty()
      .withMessage("coverage_area_site Required..");
    req
      .checkBody("setbacks_area_site")
      .notEmpty()
      .withMessage("setbacks_area_site Required..");
    req
      .checkBody("front_area_site")
      .notEmpty()
      .withMessage("front_area_site Required..");
    req
      .checkBody("rear_area_site")
      .notEmpty()
      .withMessage("rear_area_site Required..");
    req
      .checkBody("left_area_site")
      .notEmpty()
      .withMessage("left_area_site Required..");
    req
      .checkBody("right_area_site")
      .notEmpty()
      .withMessage("right_area_site Required..");
    req
      .checkBody("plot_area_deviation")
      .notEmpty()
      .withMessage("plot_area_deviation Required..");
    req
      .checkBody("no_of_flore_deviation")
      .notEmpty()
      .withMessage("no_of_flore_deviation Required..");
    req
      .checkBody("units_area_deviation")
      .notEmpty()
      .withMessage("units_area_deviation Required..");
    req
      .checkBody("built_area_deviation")
      .notEmpty()
      .withMessage("built_area_deviation Required..");
    req
      .checkBody("ground_area_deviation")
      .notEmpty()
      .withMessage("ground_area_deviation Required..");
    req
      .checkBody("first_area_deviation")
      .notEmpty()
      .withMessage("first_area_deviation Required..");
    req
      .checkBody("fsi_area_deviation")
      .notEmpty()
      .withMessage("fsi_area_deviation Required..");
    req
      .checkBody("coverage_area_deviation")
      .notEmpty()
      .withMessage("coverage_area_deviation Required..");
    req
      .checkBody("setbacks_area_deviation")
      .notEmpty()
      .withMessage("setbacks_area_deviation Required..");
    req
      .checkBody("front_area_deviation")
      .notEmpty()
      .withMessage("front_area_deviation Required..");
    req
      .checkBody("rear_area_deviation")
      .notEmpty()
      .withMessage("rear_area_deviation Required..");
    req
      .checkBody("left_area_deviation")
      .notEmpty()
      .withMessage("left_area_deviation Required..");
    req
      .checkBody("right_area_deviation")
      .notEmpty()
      .withMessage("right_area_deviation Required..");

    req.getValidationResult().then(result => {
      var errors = result.array();
      console.log(errors);
      if (result.array().length > 0) {
        res.render("valuation_report_a", {
          error: errors[0].msg,
          serial_no: req.body.serial_no,
          sBankName: req.body.sBankName,
          sBranchName: req.body.sBranchName,
          valuation_request: req.body.valuation_request,
          property_occupied_by: req.body.property_occupied_by,
          purpose_of_valuation: req.body.purpose_of_valuation,
          date_of_valuation: req.body.date_of_valuation,
          valuation_done_by: req.body.valuation_done_by,
          assessment_of_fmv: req.body.assessment_of_fmv,
          customer_name: req.body.customer_name,
          application_no: req.body.application_no,
          pro_purchaser_name: req.body.pro_purchaser_name,
          person_met: req.body.person_met,
          address_of_property: req.body.address_of_property,
          address_as_document: req.body.address_as_document,
          nearby_landmark: req.body.nearby_landmark,
          comment_on_property: req.body.comment_on_property,
          approved_layout_plan: req.body.approved_layout_plan,
          layout_date_num: req.body.layout_date_num,
          layout_authority: req.body.layout_authority,
          approved_building_plan: req.body.approved_building_plan,
          building_date_num: req.body.building_date_num,
          building_authority: req.body.building_authority,
          construction_per: req.body.construction_per,
          construction_date_num: req.body.construction_date_num,
          construction_authority: req.body.construction_authority,
          bu_permission: req.body.bu_permission,
          bu_date_num: req.body.bu_date_num,
          bu_authority: req.body.bu_authority,
          usage_as_plan: req.body.usage_as_plan,
          type_of_usage: req.body.type_of_usage,
          document_list: req.body.document_list,
          status_of_occupancy: req.body.status_of_occupancy,
          occupied_by: req.body.occupied_by,
          relationship_with_customer: req.body.relationship_with_customer,
          occupied_since: req.body.occupied_since,
          status_property: req.body.status_property,
          rental_value: req.body.rental_value,
          north_doc: req.body.north_doc,
          north_site: req.body.north_site,
          south_doc: req.body.south_doc,
          south_site: req.body.south_site,
          east_doc: req.body.east_doc,
          east_site: req.body.east_site,
          west_doc: req.body.west_doc,
          west_site: req.body.west_site,
          boundaries_match: req.body.boundaries_match,
          boundaries_remark: req.body.boundaries_remark,
          ploat_area: req.body.ploat_area,
          flat_demarcated_site: req.body.flat_demarcated_site,
          approved_use: req.body.approved_use,
          Infrastructure_area: req.body.Infrastructure_area,
          nature_of_locality: req.body.nature_of_locality,
          class_of_locality: req.body.class_of_locality,
          distance_from_office: req.body.distance_from_office,
          property_location: req.body.property_location,
          elec_service: req.body.elec_service,
          property_type: req.body.property_type,
          property_structure: req.body.property_structure,
          no_of_units: req.body.no_of_units,
          no_of_kitchen: req.body.no_of_kitchen,
          no_of_bed: req.body.no_of_bed,
          usage_remark: req.body.usage_remark,
          maintenance_level: req.body.maintenance_level,
          structure: req.body.structure,
          interiors: req.body.interiors,
          exteriors: req.body.exteriors,
          age_of_property: req.body.age_of_property,
          amenities: req.body.amenities,
          land_as_plan_a: req.body.land_as_plan_a,
          land_as_area_a: req.body.land_as_area_a,
          ground_as_plan_a: req.body.ground_as_plan_a,
          ground_as_area_a: req.body.ground_as_area_a,
          first_as_plan_a: req.body.first_as_plan_a,
          first_as_area_a: req.body.first_as_area_a,
          second_as_plan_a: req.body.second_as_plan_a,
          second_as_area_a: req.body.second_as_area_a,
          rate_as_plan_a: req.body.rate_as_plan_a,
          rate_as_area_a: req.body.rate_as_area_a,
          rate_adopted_a: req.body.rate_adopted_a,
          cost_construction_a: req.body.cost_construction_a,
          land_value_a: req.body.land_value_a,
          building_value_a: req.body.building_value_a,
          depreciation_considered_a: req.body.depreciation_considered_a,
          fair_market_value_a: req.body.fair_market_value_a,
          land_as_plan_b: req.body.land_as_plan_b,
          land_as_area_b: req.body.land_as_area_b,
          carpet_as_plan_b: req.body.carpet_as_plan_b,
          carpet_as_area_b: req.body.carpet_as_area_b,
          area_detail_as_plan: req.body.area_detail_as_plan,
          area_detail_as_area: req.body.area_detail_as_area,
          prevailing_rate: req.body.prevailing_rate,
          total_value: req.body.total_value,
          amenities_cost: req.body.amenities_cost,
          depreciation_building: req.body.depreciation_building,
          total_fair_value: req.body.total_fair_value,
          say: req.body.say,
          remarks: req.body.remarks,
          plot_area_plan: req.body.plot_area_plan,
          no_of_flore_plan: req.body.no_of_flore_plan,
          units_area_plan: req.body.units_area_plan,
          built_area_plan: req.body.built_area_plan,
          ground_area_plan: req.body.ground_area_plan,
          first_area_plan: req.body.first_area_plan,
          fsi_area_plan: req.body.fsi_area_plan,
          coverage_area_plan: req.body.coverage_area_plan,
          setbacks_area_plan: req.body.setbacks_area_plan,
          front_area_plan: req.body.front_area_plan,
          rear_area_plan: req.body.rear_area_plan,
          left_area_plan: req.body.left_area_plan,
          right_area_plan: req.body.right_area_plan,
          plot_area_site: req.body.plot_area_site,
          no_of_flore_site: req.body.no_of_flore_site,
          units_area_site: req.body.units_area_site,
          built_area_site: req.body.built_area_site,
          ground_area_site: req.body.ground_area_site,
          first_area_site: req.body.first_area_site,
          fsi_area_site: req.body.fsi_area_site,
          coverage_area_site: req.body.coverage_area_site,
          setbacks_area_site: req.body.setbacks_area_site,
          front_area_site: req.body.front_area_site,
          rear_area_site: req.body.rear_area_site,
          left_area_site: req.body.left_area_site,
          right_area_site: req.body.right_area_site,
          plot_area_deviation: req.body.plot_area_deviation,
          no_of_flore_deviation: req.body.no_of_flore_deviation,
          units_area_deviation: req.body.units_area_deviation,
          built_area_deviation: req.body.built_area_deviation,
          ground_area_deviation: req.body.ground_area_deviation,
          first_area_deviation: req.body.first_area_deviation,
          fsi_area_deviation: req.body.fsi_area_deviation,
          coverage_area_deviation: req.body.coverage_area_deviation,
          setbacks_area_deviation: req.body.setbacks_area_deviation,
          front_area_deviation: req.body.front_area_deviation,
          rear_area_deviation: req.body.rear_area_deviation,
          left_area_deviation: req.body.left_area_deviation,
          right_area_deviation: req.body.right_area_deviation
        });
      } else {
        let files;
        console.log(req.files);
        var keys = Object.keys(req.files);
        for (var elem in keys) {
          var fieldname = req.files[keys[elem]][0].fieldname;
          if (fieldname == "files") {
            req.body.files = req.files[keys[elem]][0].filename;
          }
        }
        const body = {
          images: req.body.files,
          serial_no: req.body.serial_no,
          bankInfo: {
            sBankName: req.body.sBankName,
            sBranchName: req.body.sBranchName
          },
          generalInfo: {
            valuation_request: req.body.valuation_request,
            property_occupied_by: req.body.property_occupied_by,
            purpose_of_valuation: req.body.purpose_of_valuation,
            date_of_valuation: req.body.date_of_valuation,
            valuation_done_by: req.body.valuation_done_by,
            assessment_of_fmv: req.body.assessment_of_fmv
          },
          customerDetails: {
            customer_name: req.body.customer_name,
            application_no: req.body.application_no,
            pro_purchaser_name: req.body.pro_purchaser_name,
            person_met: req.body.person_met
          },
          propertyDetails: {
            address_of_property: req.body.address_of_property,
            address_as_document: req.body.address_as_document,
            nearby_landmark: req.body.nearby_landmark,
            comment_on_property: req.body.comment_on_property
          },
          documentDetails: {
            approved_layout_plan: req.body.approved_layout_plan,
            layout_date_num: req.body.layout_date_num,
            layout_authority: req.body.layout_authority,
            approved_building_plan: req.body.approved_building_plan,
            building_date_num: req.body.building_date_num,
            building_authority: req.body.building_authority,
            construction_per: req.body.construction_per,
            construction_date_num: req.body.construction_date_num,
            construction_authority: req.body.construction_authority,
            bu_permission: req.body.bu_permission,
            bu_date_num: req.body.bu_date_num,
            bu_authority: req.body.bu_authority,
            usage_as_plan: req.body.usage_as_plan,
            type_of_usage: req.body.type_of_usage,
            document_list: req.body.document_list
          },
          occupancyDetail: {
            status_of_occupancy: req.body.status_of_occupancy,
            occupied_by: req.body.occupied_by,
            relationship_with_customer: req.body.relationship_with_customer,
            occupied_since: req.body.occupied_since,
            status_property: req.body.status_property,
            rental_value: req.body.rental_value
          },
          physicalDetail: {
            Boundaries: {
              north_doc: req.body.north_doc,
              north_site: req.body.north_site,
              south_doc: req.body.south_doc,
              south_site: req.body.south_site,
              east_doc: req.body.east_doc,
              east_site: req.body.east_site,
              west_doc: req.body.west_doc,
              west_site: req.body.west_site,
              boundaries_match: req.body.boundaries_match,
              boundaries_remark: req.body.boundaries_remark
            },
            Plot: {
              ploat_area: req.body.ploat_area,
              flat_demarcated_site: req.body.flat_demarcated_site,
              approved_use: req.body.approved_use,
              Infrastructure_area: req.body.Infrastructure_area,
              nature_of_locality: req.body.nature_of_locality,
              class_of_locality: req.body.class_of_locality,
              distance_from_office: req.body.distance_from_office,
              property_location: req.body.property_location,
              elec_service: req.body.elec_service,
              property_type: req.body.property_type
            },
            propDesc: {
              property_structure: req.body.property_structure,
              no_of_units: req.body.no_of_units,
              no_of_kitchen: req.body.no_of_kitchen,
              no_of_bed: req.body.no_of_bed,
              usage_remark: req.body.usage_remark,
              maintenance_level: req.body.maintenance_level,
              structure: req.body.structure,
              interiors: req.body.interiors,
              exteriors: req.body.exteriors,
              age_of_property: req.body.age_of_property,
              amenities: req.body.amenities
            }
          },
          Valuation: {
            areaDetailsA: {
              land_as_plan_a: req.body.land_as_plan_a,
              land_as_area_a: req.body.land_as_area_a,
              ground_as_plan_a: req.body.ground_as_plan_a,
              ground_as_area_a: req.body.ground_as_area_a,
              first_as_plan_a: req.body.first_as_plan_a,
              first_as_area_a: req.body.first_as_area_a,
              second_as_plan_a: req.body.second_as_plan_a,
              second_as_area_a: req.body.second_as_area_a,
              rate_as_plan_a: req.body.rate_as_plan_a,
              rate_as_area_a: req.body.rate_as_area_a,
              rate_adopted_a: req.body.rate_adopted_a,
              cost_construction_a: req.body.cost_construction_a,
              land_value_a: req.body.land_value_a,
              building_value_a: req.body.building_value_a,
              depreciation_considered_a: req.body.depreciation_considered_a,
              fair_market_value_a: req.body.fair_market_value_a
            },
            areaDetailsB: {
              land_as_plan_b: req.body.land_as_plan_b,
              land_as_area_b: req.body.land_as_area_b,
              carpet_as_plan_b: req.body.carpet_as_plan_b,
              carpet_as_area_b: req.body.carpet_as_area_b,
              area_detail_as_plan: req.body.area_detail_as_plan,
              area_detail_as_area: req.body.area_detail_as_area,
              prevailing_rate: req.body.prevailing_rate,
              total_value: req.body.total_value,
              amenities_cost: req.body.amenities_cost,
              depreciation_building: req.body.depreciation_building,
              total_fair_value: req.body.total_fair_value,
              say: req.body.say,
              remarks: req.body.remarks
            }
          },
          declaration: {
            plan: {
              plot_area_plan: req.body.plot_area_plan,
              no_of_flore_plan: req.body.no_of_flore_plan,
              units_area_plan: req.body.units_area_plan,
              built_area_plan: req.body.built_area_plan,
              ground_area_plan: req.body.ground_area_plan,
              first_area_plan: req.body.first_area_plan,
              fsi_area_plan: req.body.fsi_area_plan,
              coverage_area_plan: req.body.coverage_area_plan,
              setbacks_area_plan: req.body.setbacks_area_plan,
              front_area_plan: req.body.front_area_plan,
              rear_area_plan: req.body.rear_area_plan,
              left_area_plan: req.body.left_area_plan,
              right_area_plan: req.body.right_area_plan
            },
            site: {
              plot_area_site: req.body.plot_area_site,
              no_of_flore_site: req.body.no_of_flore_site,
              units_area_site: req.body.units_area_site,
              built_area_site: req.body.built_area_site,
              ground_area_site: req.body.ground_area_site,
              first_area_site: req.body.first_area_site,
              fsi_area_site: req.body.fsi_area_site,
              coverage_area_site: req.body.coverage_area_site,
              setbacks_area_site: req.body.setbacks_area_site,
              front_area_site: req.body.front_area_site,
              rear_area_site: req.body.rear_area_site,
              left_area_site: req.body.left_area_site,
              right_area_site: req.body.right_area_site
            },
            deviation: {
              plot_area_deviation: req.body.plot_area_deviation,
              no_of_flore_deviation: req.body.no_of_flore_deviation,
              units_area_deviation: req.body.units_area_deviation,
              built_area_deviation: req.body.built_area_deviation,
              ground_area_deviation: req.body.ground_area_deviation,
              first_area_deviation: req.body.first_area_deviation,
              fsi_area_deviation: req.body.fsi_area_deviation,
              coverage_area_deviation: req.body.coverage_area_deviation,
              setbacks_area_deviation: req.body.setbacks_area_deviation,
              front_area_deviation: req.body.front_area_deviation,
              rear_area_deviation: req.body.rear_area_deviation,
              left_area_deviation: req.body.left_area_deviation,
              right_area_deviation: req.body.right_area_deviation
            }
          }
        };
        const newData = new RepoartLap(body);
        newData.save((error, success) => {
          if (error) {
            return res.status(500).jsonp(error);
          } else {
            req.flash("success_msg", "Valuation Report Lap added");
            res.redirect("/");
          }
        });
      }
    });
  });
};

const DeleteLap = (req, res, next) => {
  RepoartLap.remove({
    _id: req.params.id
  }).then(() => {
    req.flash("success_msg", "Valuation Report Lap removed");
    res.redirect("/valuation_report_lap_view");
  });
};

const editLap = (req, res, next) => {
  req
    .checkBody("sBankName")
    .notEmpty()
    .withMessage("sBankName Required..");
  req
    .checkBody("sBranchName")
    .notEmpty()
    .withMessage("sBranchName Required..");
  req
    .checkBody("valuation_request")
    .notEmpty()
    .withMessage("valuation_request Required..");
  req
    .checkBody("property_occupied_by")
    .notEmpty()
    .withMessage("property_occupied_by Required..");
  req
    .checkBody("purpose_of_valuation")
    .notEmpty()
    .withMessage("purpose_of_valuation Required..");
  req
    .checkBody("date_of_valuation")
    .notEmpty()
    .withMessage("date_of_valuation Required..");
  req
    .checkBody("valuation_done_by")
    .notEmpty()
    .withMessage("valuation_done_by Required..");
  req
    .checkBody("assessment_of_fmv")
    .notEmpty()
    .withMessage("assessment_of_fmv Required..");
  req
    .checkBody("customer_name")
    .notEmpty()
    .withMessage("customer_name Required..");
  req
    .checkBody("application_no")
    .notEmpty()
    .withMessage("application_no Required..");
  req
    .checkBody("pro_purchaser_name")
    .notEmpty()
    .withMessage("pro_purchaser_name Required..");
  req
    .checkBody("person_met")
    .notEmpty()
    .withMessage("person_met Required..");
  req
    .checkBody("address_of_property")
    .notEmpty()
    .withMessage("address_of_property Required..");
  req
    .checkBody("address_as_document")
    .notEmpty()
    .withMessage("address_as_document Required..");
  req
    .checkBody("nearby_landmark")
    .notEmpty()
    .withMessage("nearby_landmark Required..");
  req
    .checkBody("comment_on_property")
    .notEmpty()
    .withMessage("comment_on_property Required..");
  req
    .checkBody("approved_layout_plan")
    .notEmpty()
    .withMessage("approved_layout_plan Required..");

  req
    .checkBody("approved_building_plan")
    .notEmpty()
    .withMessage("approved_building_plan Required..");

  req
    .checkBody("construction_per")
    .notEmpty()
    .withMessage("construction_per Required..");

  req
    .checkBody("bu_permission")
    .notEmpty()
    .withMessage("bu_permission Required..");

  req
    .checkBody("usage_as_plan")
    .notEmpty()
    .withMessage("usage_as_plan Required..");
  req
    .checkBody("type_of_usage")
    .notEmpty()
    .withMessage("type_of_usage Required..");
  req
    .checkBody("document_list")
    .notEmpty()
    .withMessage("document_list Required..");
  req
    .checkBody("status_of_occupancy")
    .notEmpty()
    .withMessage("status_of_occupancy Required..");
  req
    .checkBody("occupied_by")
    .notEmpty()
    .withMessage("occupied_by Required..");
  req
    .checkBody("relationship_with_customer")
    .notEmpty()
    .withMessage("relationship_with_customer Required..");
  req
    .checkBody("occupied_since")
    .notEmpty()
    .withMessage("occupied_since Required..");
  req
    .checkBody("status_property")
    .notEmpty()
    .withMessage("status_property Required..");
  req
    .checkBody("rental_value")
    .notEmpty()
    .withMessage("rental_value Required..");
  req
    .checkBody("north_doc")
    .notEmpty()
    .withMessage("north_doc Required..");
  req
    .checkBody("north_site")
    .notEmpty()
    .withMessage("north_site Required..");
  req
    .checkBody("south_doc")
    .notEmpty()
    .withMessage("south_doc Required..");
  req
    .checkBody("south_site")
    .notEmpty()
    .withMessage("south_site Required..");
  req
    .checkBody("east_doc")
    .notEmpty()
    .withMessage("east_doc Required..");
  req
    .checkBody("east_site")
    .notEmpty()
    .withMessage("east_site Required..");
  req
    .checkBody("west_doc")
    .notEmpty()
    .withMessage("west_doc Required..");
  req
    .checkBody("west_site")
    .notEmpty()
    .withMessage("west_site Required..");
  req
    .checkBody("boundaries_match")
    .notEmpty()
    .withMessage("boundaries_match Required..");
  req
    .checkBody("boundaries_remark")
    .notEmpty()
    .withMessage("boundaries_remark Required..");
  req
    .checkBody("ploat_area")
    .notEmpty()
    .withMessage("ploat_area Required..");
  req
    .checkBody("flat_demarcated_site")
    .notEmpty()
    .withMessage("flat_demarcated_site Required..");
  req
    .checkBody("approved_use")
    .notEmpty()
    .withMessage("approved_use Required..");
  req
    .checkBody("Infrastructure_area")
    .notEmpty()
    .withMessage("Infrastructure_area Required..");
  req
    .checkBody("nature_of_locality")
    .notEmpty()
    .withMessage("nature_of_locality Required..");
  req
    .checkBody("class_of_locality")
    .notEmpty()
    .withMessage("class_of_locality Required..");
  req
    .checkBody("distance_from_office")
    .notEmpty()
    .withMessage("distance_from_office Required..");
  req
    .checkBody("property_location")
    .notEmpty()
    .withMessage("property_location Required..");
  req
    .checkBody("elec_service")
    .notEmpty()
    .withMessage("elec_service Required..");
  req
    .checkBody("property_type")
    .notEmpty()
    .withMessage("property_type Required..");
  req
    .checkBody("property_structure")
    .notEmpty()
    .withMessage("property_structure Required..");
  req
    .checkBody("no_of_units")
    .notEmpty()
    .withMessage("no_of_units Required..");
  req
    .checkBody("no_of_kitchen")
    .notEmpty()
    .withMessage("no_of_kitchen Required..");
  req
    .checkBody("no_of_bed")
    .notEmpty()
    .withMessage("no_of_bed Required..");
  req
    .checkBody("usage_remark")
    .notEmpty()
    .withMessage("usage_remark Required..");
  req
    .checkBody("maintenance_level")
    .notEmpty()
    .withMessage("maintenance_level Required..");
  req
    .checkBody("structure")
    .notEmpty()
    .withMessage("structure Required..");
  req
    .checkBody("interiors")
    .notEmpty()
    .withMessage("interiors Required..");
  req
    .checkBody("exteriors")
    .notEmpty()
    .withMessage("exteriors Required..");
  req
    .checkBody("age_of_property")
    .notEmpty()
    .withMessage("age_of_property Required..");
  req
    .checkBody("amenities")
    .notEmpty()
    .withMessage("amenities Required..");
  req
    .checkBody("land_as_plan_a")
    .notEmpty()
    .withMessage("land_as_plan_a Required..");
  req
    .checkBody("land_as_area_a")
    .notEmpty()
    .withMessage("land_as_area_a Required..");
  req
    .checkBody("ground_as_plan_a")
    .notEmpty()
    .withMessage("ground_as_plan_a Required..");
  req
    .checkBody("ground_as_area_a")
    .notEmpty()
    .withMessage("ground_as_area_a Required..");
  req
    .checkBody("first_as_plan_a")
    .notEmpty()
    .withMessage("first_as_plan_a Required..");
  req
    .checkBody("first_as_area_a")
    .notEmpty()
    .withMessage("first_as_area_a Required..");
  req
    .checkBody("second_as_plan_a")
    .notEmpty()
    .withMessage("second_as_plan_a Required..");
  req
    .checkBody("second_as_area_a")
    .notEmpty()
    .withMessage("second_as_area_a Required..");
  req
    .checkBody("rate_as_plan_a")
    .notEmpty()
    .withMessage("rate_as_plan_a Required..");
  req
    .checkBody("rate_as_area_a")
    .notEmpty()
    .withMessage("rate_as_area_a Required..");
  req
    .checkBody("rate_adopted_a")
    .notEmpty()
    .withMessage("rate_adopted_a Required..");
  req
    .checkBody("cost_construction_a")
    .notEmpty()
    .withMessage("cost_construction_a Required..");
  req
    .checkBody("land_value_a")
    .notEmpty()
    .withMessage("land_value_a Required..");
  req
    .checkBody("building_value_a")
    .notEmpty()
    .withMessage("building_value_a Required..");
  req
    .checkBody("depreciation_considered_a")
    .notEmpty()
    .withMessage("depreciation_considered_a Required..");
  req
    .checkBody("fair_market_value_a")
    .notEmpty()
    .withMessage("fair_market_value_a Required..");
  req
    .checkBody("land_as_plan_b")
    .notEmpty()
    .withMessage("land_as_plan_b Required..");
  req
    .checkBody("land_as_area_b")
    .notEmpty()
    .withMessage("land_as_area_b Required..");
  req
    .checkBody("carpet_as_plan_b")
    .notEmpty()
    .withMessage("carpet_as_plan_b Required..");
  req
    .checkBody("carpet_as_area_b")
    .notEmpty()
    .withMessage("carpet_as_area_b Required..");
  req
    .checkBody("area_detail_as_plan")
    .notEmpty()
    .withMessage("area_detail_as_plan Required..");
  req
    .checkBody("area_detail_as_area")
    .notEmpty()
    .withMessage("area_detail_as_area Required..");
  req
    .checkBody("prevailing_rate")
    .notEmpty()
    .withMessage("prevailing_rate Required..");
  req
    .checkBody("total_value")
    .notEmpty()
    .withMessage("total_value Required..");
  req
    .checkBody("amenities_cost")
    .notEmpty()
    .withMessage("amenities_cost Required..");
  req
    .checkBody("depreciation_building")
    .notEmpty()
    .withMessage("depreciation_building Required..");
  req
    .checkBody("total_fair_value")
    .notEmpty()
    .withMessage("total_fair_value Required..");
  req
    .checkBody("say")
    .notEmpty()
    .withMessage("say Required..");
  req
    .checkBody("remarks")
    .notEmpty()
    .withMessage("remarks Required..");
  req
    .checkBody("plot_area_plan")
    .notEmpty()
    .withMessage("plot_area_plan Required..");
  req
    .checkBody("no_of_flore_plan")
    .notEmpty()
    .withMessage("no_of_flore_plan Required..");
  req
    .checkBody("units_area_plan")
    .notEmpty()
    .withMessage("units_area_plan Required..");
  req
    .checkBody("built_area_plan")
    .notEmpty()
    .withMessage("built_area_plan Required..");
  req
    .checkBody("ground_area_plan")
    .notEmpty()
    .withMessage("ground_area_plan Required..");
  req
    .checkBody("first_area_plan")
    .notEmpty()
    .withMessage("first_area_plan Required..");
  req
    .checkBody("fsi_area_plan")
    .notEmpty()
    .withMessage("fsi_area_plan Required..");
  req
    .checkBody("coverage_area_plan")
    .notEmpty()
    .withMessage("coverage_area_plan Required..");
  req
    .checkBody("setbacks_area_plan")
    .notEmpty()
    .withMessage("setbacks_area_plan Required..");
  req
    .checkBody("front_area_plan")
    .notEmpty()
    .withMessage("front_area_plan Required..");
  req
    .checkBody("rear_area_plan")
    .notEmpty()
    .withMessage("rear_area_plan Required..");
  req
    .checkBody("left_area_plan")
    .notEmpty()
    .withMessage("left_area_plan Required..");
  req
    .checkBody("right_area_plan")
    .notEmpty()
    .withMessage("right_area_plan Required..");
  req
    .checkBody("plot_area_site")
    .notEmpty()
    .withMessage("plot_area_site Required..");
  req
    .checkBody("no_of_flore_site")
    .notEmpty()
    .withMessage("no_of_flore_site Required..");
  req
    .checkBody("units_area_site")
    .notEmpty()
    .withMessage("units_area_site Required..");
  req
    .checkBody("built_area_site")
    .notEmpty()
    .withMessage("built_area_site Required..");
  req
    .checkBody("ground_area_site")
    .notEmpty()
    .withMessage("ground_area_site Required..");
  req
    .checkBody("first_area_site")
    .notEmpty()
    .withMessage("first_area_site Required..");
  req
    .checkBody("fsi_area_site")
    .notEmpty()
    .withMessage("fsi_area_site Required..");
  req
    .checkBody("coverage_area_site")
    .notEmpty()
    .withMessage("coverage_area_site Required..");
  req
    .checkBody("setbacks_area_site")
    .notEmpty()
    .withMessage("setbacks_area_site Required..");
  req
    .checkBody("front_area_site")
    .notEmpty()
    .withMessage("front_area_site Required..");
  req
    .checkBody("rear_area_site")
    .notEmpty()
    .withMessage("rear_area_site Required..");
  req
    .checkBody("left_area_site")
    .notEmpty()
    .withMessage("left_area_site Required..");
  req
    .checkBody("right_area_site")
    .notEmpty()
    .withMessage("right_area_site Required..");
  req
    .checkBody("plot_area_deviation")
    .notEmpty()
    .withMessage("plot_area_deviation Required..");
  req
    .checkBody("no_of_flore_deviation")
    .notEmpty()
    .withMessage("no_of_flore_deviation Required..");
  req
    .checkBody("units_area_deviation")
    .notEmpty()
    .withMessage("units_area_deviation Required..");
  req
    .checkBody("built_area_deviation")
    .notEmpty()
    .withMessage("built_area_deviation Required..");
  req
    .checkBody("ground_area_deviation")
    .notEmpty()
    .withMessage("ground_area_deviation Required..");
  req
    .checkBody("first_area_deviation")
    .notEmpty()
    .withMessage("first_area_deviation Required..");
  req
    .checkBody("fsi_area_deviation")
    .notEmpty()
    .withMessage("fsi_area_deviation Required..");
  req
    .checkBody("coverage_area_deviation")
    .notEmpty()
    .withMessage("coverage_area_deviation Required..");
  req
    .checkBody("setbacks_area_deviation")
    .notEmpty()
    .withMessage("setbacks_area_deviation Required..");
  req
    .checkBody("front_area_deviation")
    .notEmpty()
    .withMessage("front_area_deviation Required..");
  req
    .checkBody("rear_area_deviation")
    .notEmpty()
    .withMessage("rear_area_deviation Required..");
  req
    .checkBody("left_area_deviation")
    .notEmpty()
    .withMessage("left_area_deviation Required..");
  req
    .checkBody("right_area_deviation")
    .notEmpty()
    .withMessage("right_area_deviation Required..");

  req.getValidationResult().then(result => {
    var errors = result.array();
    console.log(errors);
    if (result.array().length > 0) {
      res.render("valuation_report_lap", {
        error: errors[0].msg,
        serial_no: req.body.serial_no,
        sBankName: req.body.sBankName,
        sBranchName: req.body.sBranchName,
        valuation_request: req.body.valuation_request,
        property_occupied_by: req.body.property_occupied_by,
        purpose_of_valuation: req.body.purpose_of_valuation,
        date_of_valuation: req.body.date_of_valuation,
        valuation_done_by: req.body.valuation_done_by,
        assessment_of_fmv: req.body.assessment_of_fmv,
        customer_name: req.body.customer_name,
        application_no: req.body.application_no,
        pro_purchaser_name: req.body.pro_purchaser_name,
        person_met: req.body.person_met,
        address_of_property: req.body.address_of_property,
        address_as_document: req.body.address_as_document,
        nearby_landmark: req.body.nearby_landmark,
        comment_on_property: req.body.comment_on_property,
        approved_layout_plan: req.body.approved_layout_plan,
        layout_date_num: req.body.layout_date_num,
        layout_authority: req.body.layout_authority,
        approved_building_plan: req.body.approved_building_plan,
        building_date_num: req.body.building_date_num,
        building_authority: req.body.building_authority,
        construction_per: req.body.construction_per,
        construction_date_num: req.body.construction_date_num,
        construction_authority: req.body.construction_authority,
        bu_permission: req.body.bu_permission,
        bu_date_num: req.body.bu_date_num,
        bu_authority: req.body.bu_authority,
        usage_as_plan: req.body.usage_as_plan,
        type_of_usage: req.body.type_of_usage,
        document_list: req.body.document_list,
        status_of_occupancy: req.body.status_of_occupancy,
        occupied_by: req.body.occupied_by,
        relationship_with_customer: req.body.relationship_with_customer,
        occupied_since: req.body.occupied_since,
        status_property: req.body.status_property,
        rental_value: req.body.rental_value,
        north_doc: req.body.north_doc,
        north_site: req.body.north_site,
        south_doc: req.body.south_doc,
        south_site: req.body.south_site,
        east_doc: req.body.east_doc,
        east_site: req.body.east_site,
        west_doc: req.body.west_doc,
        west_site: req.body.west_site,
        boundaries_match: req.body.boundaries_match,
        boundaries_remark: req.body.boundaries_remark,
        ploat_area: req.body.ploat_area,
        flat_demarcated_site: req.body.flat_demarcated_site,
        approved_use: req.body.approved_use,
        Infrastructure_area: req.body.Infrastructure_area,
        nature_of_locality: req.body.nature_of_locality,
        class_of_locality: req.body.class_of_locality,
        distance_from_office: req.body.distance_from_office,
        property_location: req.body.property_location,
        elec_service: req.body.elec_service,
        property_type: req.body.property_type,
        property_structure: req.body.property_structure,
        no_of_units: req.body.no_of_units,
        no_of_kitchen: req.body.no_of_kitchen,
        no_of_bed: req.body.no_of_bed,
        usage_remark: req.body.usage_remark,
        maintenance_level: req.body.maintenance_level,
        structure: req.body.structure,
        interiors: req.body.interiors,
        exteriors: req.body.exteriors,
        age_of_property: req.body.age_of_property,
        amenities: req.body.amenities,
        land_as_plan_a: req.body.land_as_plan_a,
        land_as_area_a: req.body.land_as_area_a,
        ground_as_plan_a: req.body.ground_as_plan_a,
        ground_as_area_a: req.body.ground_as_area_a,
        first_as_plan_a: req.body.first_as_plan_a,
        first_as_area_a: req.body.first_as_area_a,
        second_as_plan_a: req.body.second_as_plan_a,
        second_as_area_a: req.body.second_as_area_a,
        rate_as_plan_a: req.body.rate_as_plan_a,
        rate_as_area_a: req.body.rate_as_area_a,
        rate_adopted_a: req.body.rate_adopted_a,
        cost_construction_a: req.body.cost_construction_a,
        land_value_a: req.body.land_value_a,
        building_value_a: req.body.building_value_a,
        depreciation_considered_a: req.body.depreciation_considered_a,
        fair_market_value_a: req.body.fair_market_value_a,
        land_as_plan_b: req.body.land_as_plan_b,
        land_as_area_b: req.body.land_as_area_b,
        carpet_as_plan_b: req.body.carpet_as_plan_b,
        carpet_as_area_b: req.body.carpet_as_area_b,
        area_detail_as_plan: req.body.area_detail_as_plan,
        area_detail_as_area: req.body.area_detail_as_area,
        prevailing_rate: req.body.prevailing_rate,
        total_value: req.body.total_value,
        amenities_cost: req.body.amenities_cost,
        depreciation_building: req.body.depreciation_building,
        total_fair_value: req.body.total_fair_value,
        say: req.body.say,
        remarks: req.body.remarks,
        plot_area_plan: req.body.plot_area_plan,
        no_of_flore_plan: req.body.no_of_flore_plan,
        units_area_plan: req.body.units_area_plan,
        built_area_plan: req.body.built_area_plan,
        ground_area_plan: req.body.ground_area_plan,
        first_area_plan: req.body.first_area_plan,
        fsi_area_plan: req.body.fsi_area_plan,
        coverage_area_plan: req.body.coverage_area_plan,
        setbacks_area_plan: req.body.setbacks_area_plan,
        front_area_plan: req.body.front_area_plan,
        rear_area_plan: req.body.rear_area_plan,
        left_area_plan: req.body.left_area_plan,
        right_area_plan: req.body.right_area_plan,
        plot_area_site: req.body.plot_area_site,
        no_of_flore_site: req.body.no_of_flore_site,
        units_area_site: req.body.units_area_site,
        built_area_site: req.body.built_area_site,
        ground_area_site: req.body.ground_area_site,
        first_area_site: req.body.first_area_site,
        fsi_area_site: req.body.fsi_area_site,
        coverage_area_site: req.body.coverage_area_site,
        setbacks_area_site: req.body.setbacks_area_site,
        front_area_site: req.body.front_area_site,
        rear_area_site: req.body.rear_area_site,
        left_area_site: req.body.left_area_site,
        right_area_site: req.body.right_area_site,
        plot_area_deviation: req.body.plot_area_deviation,
        no_of_flore_deviation: req.body.no_of_flore_deviation,
        units_area_deviation: req.body.units_area_deviation,
        built_area_deviation: req.body.built_area_deviation,
        ground_area_deviation: req.body.ground_area_deviation,
        first_area_deviation: req.body.first_area_deviation,
        fsi_area_deviation: req.body.fsi_area_deviation,
        coverage_area_deviation: req.body.coverage_area_deviation,
        setbacks_area_deviation: req.body.setbacks_area_deviation,
        front_area_deviation: req.body.front_area_deviation,
        rear_area_deviation: req.body.rear_area_deviation,
        left_area_deviation: req.body.left_area_deviation,
        right_area_deviation: req.body.right_area_deviation
      });
    } else {
      RepoartLap.findOne({
        _id: req.params.id
      }).then(function(reportLap) {
        console.log(reportLap);
        reportLap.serial_no = req.body.serial_no;
        reportLap.bankInfo.sBankName = req.body.sBankName;
        reportLap.bankInfo.sBranchName = req.body.sBranchName;
        reportLap.generalInfo.valuation_request = req.body.valuation_request;
        reportLap.generalInfo.property_occupied_by =
          req.body.property_occupied_by;
        reportLap.generalInfo.purpose_of_valuation =
          req.body.purpose_of_valuation;
        reportLap.generalInfo.date_of_valuation = req.body.date_of_valuation;
        reportLap.generalInfo.valuation_done_by = req.body.valuation_done_by;
        reportLap.generalInfo.assessment_of_fmv = req.body.assessment_of_fmv;
        reportLap.customerDetails.customer_name = req.body.customer_name;
        reportLap.customerDetails.application_no = req.body.application_no;
        reportLap.customerDetails.pro_purchaser_name =
          req.body.pro_purchaser_name;
        reportLap.customerDetails.person_met = req.body.person_met;
        reportLap.propertyDetails.address_of_property =
          req.body.address_of_property;
        reportLap.propertyDetails.address_as_document =
          req.body.address_as_document;
        reportLap.propertyDetails.nearby_landmark = req.body.nearby_landmark;
        reportLap.propertyDetails.comment_on_property =
          req.body.comment_on_property;
        reportLap.documentDetails.approved_layout_plan =
          req.body.approved_layout_plan;
        reportLap.documentDetails.layout_date_num = req.body.layout_date_num;
        reportLap.documentDetails.layout_authority = req.body.layout_authority;
        reportLap.documentDetails.approved_building_plan =
          req.body.approved_building_plan;
        reportLap.documentDetails.building_date_num =
          req.body.building_date_num;
        reportLap.documentDetails.building_authority =
          req.body.building_authority;
        reportLap.documentDetails.construction_per = req.body.construction_per;
        reportLap.documentDetails.construction_date_num =
          req.body.construction_date_num;
        reportLap.documentDetails.construction_authority =
          req.body.construction_authority;
        reportLap.documentDetails.bu_permission = req.body.bu_permission;
        reportLap.documentDetails.bu_date_num = req.body.bu_date_num;
        reportLap.documentDetails.bu_authority = req.body.bu_authority;
        reportLap.documentDetails.usage_as_plan = req.body.usage_as_plan;
        reportLap.documentDetails.type_of_usage = req.body.type_of_usage;
        reportLap.documentDetails.document_list = req.body.document_list;
        reportLap.occupancyDetail.status_of_occupancy =
          req.body.status_of_occupancy;
        reportLap.occupancyDetail.occupied_by = req.body.occupied_by;
        reportLap.occupancyDetail.relationship_with_customer =
          req.body.relationship_with_customer;
        reportLap.occupancyDetail.occupied_since = req.body.occupied_since;
        reportLap.occupancyDetail.status_property = req.body.status_property;
        reportLap.occupancyDetail.rental_value = req.body.rental_value;
        reportLap.physicalDetail.Boundaries.north_doc = req.body.north_doc;
        reportLap.physicalDetail.Boundaries.north_site = req.body.north_site;
        reportLap.physicalDetail.Boundaries.south_doc = req.body.south_doc;
        reportLap.physicalDetail.Boundaries.south_site = req.body.south_site;
        reportLap.physicalDetail.Boundaries.east_doc = req.body.east_doc;
        reportLap.physicalDetail.Boundaries.east_site = req.body.east_site;
        reportLap.physicalDetail.Boundaries.west_doc = req.body.west_doc;
        reportLap.physicalDetail.Boundaries.west_site = req.body.west_site;
        reportLap.physicalDetail.Boundaries.boundaries_match =
          req.body.boundaries_match;
        reportLap.physicalDetail.Boundaries.boundaries_remark =
          req.body.boundaries_remark;
        reportLap.physicalDetail.Plot.ploat_area = req.body.ploat_area;
        reportLap.physicalDetail.Plot.flat_demarcated_site =
          req.body.flat_demarcated_site;
        reportLap.physicalDetail.Plot.approved_use = req.body.approved_use;
        reportLap.physicalDetail.Plot.Infrastructure_area =
          req.body.Infrastructure_area;
        reportLap.physicalDetail.Plot.nature_of_locality =
          req.body.nature_of_locality;
        reportLap.physicalDetail.Plot.class_of_locality =
          req.body.class_of_locality;
        reportLap.physicalDetail.Plot.distance_from_office =
          req.body.distance_from_office;
        reportLap.physicalDetail.Plot.property_location =
          req.body.property_location;
        reportLap.physicalDetail.Plot.elec_service = req.body.elec_service;
        reportLap.physicalDetail.Plot.property_type = req.body.property_type;
        reportLap.physicalDetail.propDesc.property_structure =
          req.body.property_structure;
        reportLap.physicalDetail.propDesc.no_of_units = req.body.no_of_units;
        reportLap.physicalDetail.propDesc.no_of_kitchen =
          req.body.no_of_kitchen;
        reportLap.physicalDetail.propDesc.no_of_bed = req.body.no_of_bed;
        reportLap.physicalDetail.propDesc.usage_remark = req.body.usage_remark;
        reportLap.physicalDetail.propDesc.maintenance_level =
          req.body.maintenance_level;
        reportLap.physicalDetail.propDesc.structure = req.body.structure;
        reportLap.physicalDetail.propDesc.interiors = req.body.interiors;
        reportLap.physicalDetail.propDesc.exteriors = req.body.exteriors;
        reportLap.physicalDetail.propDesc.age_of_property =
          req.body.age_of_property;
        reportLap.physicalDetail.propDesc.amenities = req.body.amenities;
        reportLap.Valuation.areaDetailsA.land_as_plan_a =
          req.body.land_as_plan_a;
        reportLap.Valuation.areaDetailsA.land_as_area_a =
          req.body.land_as_area_a;
        reportLap.Valuation.areaDetailsA.ground_as_plan_a =
          req.body.ground_as_plan_a;
        reportLap.Valuation.areaDetailsA.ground_as_area_a =
          req.body.ground_as_area_a;
        reportLap.Valuation.areaDetailsA.first_as_plan_a =
          req.body.first_as_plan_a;
        reportLap.Valuation.areaDetailsA.first_as_area_a =
          req.body.first_as_area_a;
        reportLap.Valuation.areaDetailsA.second_as_plan_a =
          req.body.second_as_plan_a;
        reportLap.Valuation.areaDetailsA.second_as_area_a =
          req.body.second_as_area_a;
        reportLap.Valuation.areaDetailsA.rate_as_plan_a =
          req.body.rate_as_plan_a;
        reportLap.Valuation.areaDetailsA.rate_as_area_a =
          req.body.rate_as_area_a;
        reportLap.Valuation.areaDetailsA.rate_adopted_a =
          req.body.rate_adopted_a;
        reportLap.Valuation.areaDetailsA.cost_construction_a =
          req.body.cost_construction_a;
        reportLap.Valuation.areaDetailsA.land_value_a = req.body.land_value_a;
        reportLap.Valuation.areaDetailsA.building_value_a =
          req.body.building_value_a;
        reportLap.Valuation.areaDetailsA.depreciation_considered_a =
          req.body.depreciation_considered_a;
        reportLap.Valuation.areaDetailsA.fair_market_value_a =
          req.body.fair_market_value_a;
        reportLap.Valuation.areaDetailsB.land_as_plan_b =
          req.body.land_as_plan_b;
        reportLap.Valuation.areaDetailsB.land_as_area_b =
          req.body.land_as_area_b;
        reportLap.Valuation.areaDetailsB.carpet_as_plan_b =
          req.body.carpet_as_plan_b;
        reportLap.Valuation.areaDetailsB.carpet_as_area_b =
          req.body.carpet_as_area_b;
        reportLap.Valuation.areaDetailsB.area_detail_as_plan =
          req.body.area_detail_as_plan;
        reportLap.Valuation.areaDetailsB.area_detail_as_area =
          req.body.area_detail_as_area;
        reportLap.Valuation.areaDetailsB.prevailing_rate =
          req.body.prevailing_rate;
        reportLap.Valuation.areaDetailsB.total_value = req.body.total_value;
        reportLap.Valuation.areaDetailsB.amenities_cost =
          req.body.amenities_cost;
        reportLap.Valuation.areaDetailsB.depreciation_building =
          req.body.depreciation_building;
        reportLap.Valuation.areaDetailsB.total_fair_value =
          req.body.total_fair_value;
        reportLap.Valuation.areaDetailsB.say = req.body.say;
        reportLap.Valuation.areaDetailsB.remarks = req.body.remarks;
        reportLap.declaration.plan.plot_area_plan = req.body.plot_area_plan;
        reportLap.declaration.plan.no_of_flore_plan = req.body.no_of_flore_plan;
        reportLap.declaration.plan.units_area_plan = req.body.units_area_plan;
        reportLap.declaration.plan.built_area_plan = req.body.built_area_plan;
        reportLap.declaration.plan.ground_area_plan = req.body.ground_area_plan;
        reportLap.declaration.plan.first_area_plan = req.body.first_area_plan;
        reportLap.declaration.plan.fsi_area_plan = req.body.fsi_area_plan;
        reportLap.declaration.plan.coverage_area_plan =
          req.body.coverage_area_plan;
        reportLap.declaration.plan.setbacks_area_plan =
          req.body.setbacks_area_plan;
        reportLap.declaration.plan.front_area_plan = req.body.front_area_plan;
        reportLap.declaration.plan.rear_area_plan = req.body.rear_area_plan;
        reportLap.declaration.plan.left_area_plan = req.body.left_area_plan;
        reportLap.declaration.plan.right_area_plan = req.body.right_area_plan;
        reportLap.declaration.site.plot_area_site = req.body.plot_area_site;
        reportLap.declaration.site.no_of_flore_site = req.body.no_of_flore_site;
        reportLap.declaration.site.units_area_site = req.body.units_area_site;
        reportLap.declaration.site.built_area_site = req.body.built_area_site;
        reportLap.declaration.site.ground_area_site = req.body.ground_area_site;
        reportLap.declaration.site.first_area_site = req.body.first_area_site;
        reportLap.declaration.site.fsi_area_site = req.body.fsi_area_site;
        reportLap.declaration.site.coverage_area_site =
          req.body.coverage_area_site;
        reportLap.declaration.site.setbacks_area_site =
          req.body.setbacks_area_site;
        reportLap.declaration.site.front_area_site = req.body.front_area_site;
        reportLap.declaration.site.rear_area_site = req.body.rear_area_site;
        reportLap.declaration.site.left_area_site = req.body.left_area_site;
        reportLap.declaration.site.right_area_site = req.body.right_area_site;
        reportLap.declaration.deviation.plot_area_deviation =
          req.body.plot_area_deviation;
        reportLap.declaration.deviation.no_of_flore_deviation =
          req.body.no_of_flore_deviation;
        reportLap.declaration.deviation.units_area_deviation =
          req.body.units_area_deviation;
        reportLap.declaration.deviation.built_area_deviation =
          req.body.built_area_deviation;
        reportLap.declaration.deviation.ground_area_deviation =
          req.body.ground_area_deviation;
        reportLap.declaration.deviation.first_area_deviation =
          req.body.first_area_deviation;
        reportLap.declaration.deviation.fsi_area_deviation =
          req.body.fsi_area_deviation;
        reportLap.declaration.deviation.coverage_area_deviation =
          req.body.coverage_area_deviation;
        reportLap.declaration.deviation.setbacks_area_deviation =
          req.body.setbacks_area_deviation;
        reportLap.declaration.deviation.front_area_deviation =
          req.body.front_area_deviation;
        reportLap.declaration.deviation.rear_area_deviation =
          req.body.rear_area_deviation;
        reportLap.declaration.deviation.left_area_deviation =
          req.body.left_area_deviation;
        reportLap.declaration.deviation.right_area_deviation =
          req.body.right_area_deviation;

        reportLap.save((error, success) => {
          if (error) {
            return res.status(500).jsonp(error);
          } else {
            req.flash("success_msg", "Valuation Report Lap Updated");
            res.redirect("/valuation_report_lap_view");
          }
        });
      });
    }
  });
};
module.exports = {
  valuation_report_lap,
  DeleteLap,
  editLap
};
