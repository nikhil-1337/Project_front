const User = require("../models/user");
const mongoose = require("mongoose");
const Loan = require("../models/loan_model");
const cli = require("../config/cli").console;
var bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator/check");
const debug = require("debug")("http");


mongoose.Promise = Promise;

const addLoan = (req, res, next) => {
  req
    .checkBody("sName")
    .notEmpty()
    .withMessage("Enter Applicant Name");
  req
    .checkBody("sPurpose")
    .notEmpty()
    .withMessage("Enter sPurpose ");
  req
    .checkBody("dRecivedDate")
    .notEmpty()
    .withMessage("EnterdRecivedDate ");
  req
    .checkBody("dSentDate")
    .notEmpty()
    .withMessage("Enter dSentDate ");
  req
    .checkBody("sBankName")
    .notEmpty()
    .withMessage("Enter Bank Name ");
  req
    .checkBody("sBranchName")
    .notEmpty()
    .withMessage("Enter BranchName ");
  req
    .checkBody("sVisitDoneBy")
    .notEmpty()
    .withMessage("Enter sVisitDoneBy ");
  req
    .checkBody("nFees")
    .notEmpty()
    .withMessage("Enter nFees ");
  req
    .checkBody("nTax")
    .notEmpty()
    .withMessage("Enter nTax ");
  req
    .checkBody("nTotalFees")
    .notEmpty()
    .withMessage("Enter nTotalFees ");

  req.getValidationResult().then(result => {
    var errors = result.array();
    console.log(errors);
    if (result.array().length > 0) {
      res.render("loan_form", {
        error: errors[0].msg,
        sName: req.body.sName,
        sPurpose: req.body.sPurpose,
        dRecivedDate: req.body.dRecivedDate,
        dSentDate: req.body.dSentDate,
        sVisitDoneBy: req.body.sVisitDoneBy,
        nFees: req.body.nFees,
        nTax: req.body.nTax,
        nTotalFees: req.body.nTotalFees,
        sRemarks: req.body.sRemarks
      });
    } else {
      var newLoan = new Loan({
        sName: req.body.sName,
        sPurpose: req.body.sPurpose,
        dRecivedDate: new Date(req.body.dRecivedDate).toDateString(),
        dSentDate: req.body.dSentDate,
        sBankName: req.body.sBankName,
        sBranchName: req.body.sBranchName,
        sVisitDoneBy: req.body.sVisitDoneBy,
        nFees: req.body.nFees,
        nTax: req.body.nTax,
        nTotalFees: req.body.nTotalFees,
        sRemarks: req.body.sRemarks,
        ePaidunpaid: req.body.ePaidunpaid,
        sCashOrCheque: req.body.sCashOrCheque
      });
      Loan.create(newLoan, function(err, Loan) {
        if (err) throw err;
        console.log(err);
      });
      req.flash("success_msg", "Loan Data added");
      res.redirect("/");
    }
  });
};

const editLoan = (req, res, next) => {
  req
    .checkBody("sName")
    .notEmpty()
    .withMessage("Enter Applicant Name");
  req
    .checkBody("sPurpose")
    .notEmpty()
    .withMessage("Enter sPurpose ");
  req
    .checkBody("dRecivedDate")
    .notEmpty()
    .withMessage("EnterdRecivedDate ");
  req
    .checkBody("dSentDate")
    .notEmpty()
    .withMessage("Enter dSentDate ");
  req
    .checkBody("sBankName")
    .notEmpty()
    .withMessage("Enter Bank Name ");
  req
    .checkBody("sBranchName")
    .notEmpty()
    .withMessage("Enter BranchName ");
  req
    .checkBody("sVisitDoneBy")
    .notEmpty()
    .withMessage("Enter sVisitDoneBy ");
  req
    .checkBody("nFees")
    .notEmpty()
    .withMessage("Enter nFees ");

  req
    .checkBody("nTotalFees")
    .notEmpty()
    .withMessage("Enter nTotalFees ");

  req.getValidationResult().then(result => {
    var errors = result.array();
    if (result.array().length > 0) {
      res.render("loan_client_edit", {
        error: errors[0].msg,
        sName: req.body.sName,
        sPurpose: req.body.sPurpose,
        dRecivedDate: req.body.dRecivedDate,
        dSentDate: req.body.dSentDate,
        sVisitDoneBy: req.body.sVisitDoneBy,
        nFees: req.body.nFees,
        nTax: req.body.nTax,
        nTotalFees: req.body.nTotalFees,
        sRemarks: req.body.sRemarks
      });
    } else {
      Loan.findOne({
        _id: req.params.id
      }).then(function(loan) {
        console.log(loan);
        loan.sName = req.body.sName;
        loan.sPurpose = req.body.sPurpose;
        loan.dRecivedDate = req.body.dRecivedDate;
        loan.dSentDate = req.body.dSentDate;
        loan.sBankName = req.body.sBankName;
        loan.sBranchName = req.body.sBranchName;
        loan.sVisitDoneBy = req.body.sVisitDoneBy;
        loan.nFees = req.body.nFees;
        loan.nTax = req.body.nTax;
        loan.nTotalFees = req.body.nTotalFees;
        loan.sRemarks = req.body.sRemarks;
        loan.ePaidunpaid = req.body.ePaidunpaid;
        loan.sCashOrCheque = req.body.sCashOrCheque;

        loan.save(function(error, data) {
          if (error) {
            console.log(error);
            res.redirect("back", {
              error: error,
              sName: req.body.sName,
              sPurpose: req.body.sPurpose,
              dRecivedDate: req.body.dRecivedDate,
              dSentDate: req.body.dSentDate,
              sVisitDoneBy: req.body.sVisitDoneBy,
              nFees: req.body.nFees,
              nTax: req.body.nTax,
              nTotalFees: req.body.nTotalFees,
              sRemarks: req.body.sRemarks
            });
          } else {
            req.flash("success_msg", "Data Updated successfully.");
            res.redirect("/");
          }
        });
      });
    }
  });
};

const deleteLoan = (req, res, next) => {
  Loan.remove({
    _id: req.params.id
  }).then(() => {
    req.flash("success_msg", "Data deleted Successfully");
    res.redirect("/loan_client_data");
  });
};

module.exports = {
  addLoan,
  deleteLoan,
  editLoan
};
