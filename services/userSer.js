const User = require("../models/user");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const async = require("async");
const mail = require("../services/mail");
const request = require("request");
const cli = require("../config/cli").console;
const debug = require("debug")("http");


mongoose.Promise = Promise;

const createUser = (req, res) => {
  let errors = [];
  let cPwd = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
  let cName = /^(([a-zA-Z]{2,20})+(\s{0,2}[a-zA-Z,' ']{2,20}))*$/;
  let cEmail = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/;

  if (cEmail.test(req.body.sEmail) === false) {
    errors.push({
      text: "Enter Valid Email"
    });
  }
  if (cName.test(req.body.sFullname) === false) {
    errors.push({
      text: "Enter Valid Name"
    });
  }
  if (cPwd.test(req.body.sPassword) === false) {
    errors.push({
      text:
        "Password (UpperCase, LowerCase, Number/SpecialChar and min 8 Chars)"
    });
  }
  if (req.body.sPassword != req.body.cpassword) {
    errors.push({
      text: "Passwords do not match"
    });
  }

  if (errors.length > 0) {
    res.render("users/register", {
      errors: errors,
      sFullname: req.body.sFullname,
      sEmail: req.body.sEmail,
      sPassword: req.body.sPassword,
      cpassword: req.body.cpassword
    });
  } else {
    User.findOne({
      sEmail: req.body.sEmail
    }).then(user => {
      if (user) {
        console.log("email exists");
        req.flash("error", "Email Already Registered!");
        cli.red("Email Already Registered!");
        res.redirect("register");
      } else {
        let sDeviceToken = bcrypt.hashSync(
          req.body.sEmail + Math.random() + Date.now()
        );
        let sVerificationToken = jwt.sign(
          {
            email: req.body.email
          },
          "qwertgfdsa",
          {
            expiresIn: "1h"
          }
        );
        const newUser = new User({
          sFullname: req.body.sFullname,
          sEmail: req.body.sEmail,
          sPassword: req.body.sPassword,
          eUserType: false,
          aJwtToken: {
            sDeviceToken: sDeviceToken
          },
          resetPasswordToken: "",
          sVerificationToken: sVerificationToken
        });
        var secretToken = jwt.sign(
          {
            email: req.body.email
          },
          "qwertgfdsa",
          {
            expiresIn: "1h"
          }
        );
        newUser.secretToken = secretToken;
        // if (req.body["g-recaptcha-response"] === undefined || req.body["g-recaptcha-response"] === "" || req.body["g-recaptcha-response"] === null) {
        //   req.flash("error_msg", "Please select captcha");
        //   res.redirect("register");
        //   return;
        // }
        // var secretKey = "6LciSUMUAAAAAHhOPz--EbBkG9StviBXELfEuRZ1";
        // var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body["g-recaptcha-response"] + "&remoteip=" + req.connection.remoteAddress;
        // request(verificationUrl, function(error, response, body) {
        //   body = JSON.parse(body);
        //   if (body.success !== undefined && !body.success) {
        //     return res.redirect("register");
        //   }
        // });
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.sPassword, salt, (err, hash) => {
            if (err) throw err;
            newUser.sPassword = hash;
            newUser.save();
          });
        });
        const url = "http://localhost:4000/verification/" + sVerificationToken;
        try {
          mail.sendMail(
            {
              emailReciver: req.body.sEmail,
              subject: "Please verify your email!",
              text: "Activation link " + url
            },
            function(err, info) {
              if (err) {
                console.log(err);
              } else {
                console.log(info);
              }
            }
          );
        } catch (error) {
          cli.red("Unsucessfull: created user email notification.");
          throw error;
        }
        req.flash("success_msg", "Please Check Your Mail");
        cli.green("Please Check Your Mail");
        res.redirect("/login");
      }
    });
  }
};

const updateProfile = (req, res, next) => {
  let errors = [];
  let cPwd = /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/;
  let cName = /^(([a-zA-Z]{2,20})+(\s{0,2}[a-zA-Z,' ']{2,20}))*$/;

  if (cName.test(req.body.sFullname) === false) {
    errors.push({
      text: "Enter Valid Name"
    });
  }
  if (cPwd.test(req.body.sPassword) === false) {
    errors.push({
      text:
        "Password (UpperCase, LowerCase, Number/SpecialChar and min 8 Chars)"
    });
  }

  if (errors.length > 0) {
    res.render("profile", {
      errors: errors,
      sFullname: req.body.sFullname,
      sPassword: req.body.sPassword
    });
  } else {
    let whereQuery = {
      _id: req.user.id
    };
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(req.body.sPassword, salt, (err, hash) => {
        if (err) throw err;
        req.body.sPassword = hash;
        let updateValue = {
          $set: {
            sFullname: req.body.sFullname,
            sPassword: hash
          }
        };
        User.updateOne(whereQuery, updateValue, (err, result) => {
          if (err) {
            throw err;
          } else {
            req.flash("success_msg", "Pprofile updated");
            cli.green("Pprofile updated");
            res.redirect("/profile");
          }
        });
      });
    });
  }
};

const deleteUser = (req, res) => {
  User.remove({
    _id: req.params.id
  }).then(() => {
    req.flash("success_msg", "User removed");
    res.redirect("/user_data");
  });
};

const makeAdmin = (req, res) => {
  User.findOne({
    _id: req.params.id
  }).then(function(data) {
    data.eUserType = true;
    data.save(function(err, data) {
      if (err) {
        req.flash("error_msg", "internal error");
        res.redirect("/user_data");
      } else {
        req.flash("success_msg", "Admin Created Successfully");
        res.redirect("/user_data");
      }
    });
  });
};
module.exports = {
  createUser,
  updateProfile,
  deleteUser,
  makeAdmin
};
