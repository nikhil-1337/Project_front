const User = require("../models/user");
const mongoose = require("mongoose");
const cli = require("../config/cli").console;
var bodyParser = require("body-parser");
const debug = require("debug")("http");
const Loan = require("../models/loan_model")
const ReportA = require("../models/valuation_report_model")
const ReportB = require("../models/valuation_report_pro_model");
const ReportLap = require("../models/valuation_report_lap_model")
const PDFDocument = require("pdfkit");

mongoose.Promise = Promise;

const loanPdf =(req,res) => {
  const doc = new PDFDocument();
  Loan.find({
    _id: req.params.id
  }).then(function (loan) {
    var title = loan[0].sName;
    var Purpose = loan[0].sPurpose;
    var date = new Date();
    var Dated = date.toDateString();
    var filename = encodeURIComponent(Math.random()) + ".pdf";
    res.setHeader(
      "Content-disposition",
      'attachment; filename="' + filename + '"'
    );
    res.setHeader("Content-type", "application/pdf");
    doc
      .font("Times-Roman", 18)
      .fontSize(25)
      .text("Client: " + title, 100, 50);
    doc
      .fontSize(15)
      .fillColor("black")
      .moveDown()
      .text("Purpose:" + Purpose);
    doc.moveDown().text("Bank: " + loan[0].sBankName);
    doc.moveDown().text("Branch: " + loan[0].sBranchName);
    doc
      .moveDown()
      .text(
        "Date of Recived: " + new Date(loan[0].dRecivedDate).toDateString()
      );
    doc
      .moveDown()
      .text("Date of Sent: " + new Date(loan[0].dSentDate).toDateString());
    doc.moveDown().text("Visit Done By: " + loan[0].sVisitDoneBy);
    doc.moveDown().text("Fees: " + loan[0].nFees);
    doc.moveDown().text("Paid/Unpaid: " + loan[0].ePaidunpaid);
    doc.moveDown().text("Cash/Cheque: " + loan[0].sCashOrCheque);
    doc.moveDown().text("Remarks: " + loan[0].sRemarks);
    doc.moveDown().text("Date: " + Dated, {
      align: "right"
    });
    doc
      .moveTo(98, 78)
      .lineTo(550, 78)
      .fillColor("grey")
      .stroke();
    doc
      .moveTo(98, 460)
      .lineTo(550, 460)
      .fillColor("grey")
      .stroke();
    doc.pipe(res);
    doc.end();
  });
}

const reportApdf = (req,res) => {
  const urlA = "./public/uploads/a";
  const doc = new PDFDocument();
  ReportA.find({ _id: req.params.id }).then(function (reportA) {
    var image = "./public/uploads/a/" + reportA[0].images;
    var title = reportA[0].generalInfo.name_of_customer;
    var date = new Date();
    var Dated = date.toDateString();
    var filename = encodeURIComponent(Math.random()) + ".pdf";
    res.setHeader("Content-disposition", 'attachment; filename="' + filename + '"');
    res.setHeader("Content-type", "application/pdf");
    doc
      .font("Times-Roman", 18)
      .fontSize(22).fillColor("brown")

      .text("VALUATION REPORT A", 200, 50);
    // Fit the image within the dimensions
    doc.moveDown(0.5).image(image, {
      fit: [250, 300],
      align: "center",
    })


    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("Bank Information", 75);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Bank: " + reportA[0].bankInfo.sBankName, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Branch: " + reportA[0].bankInfo.sBranchName, 100);
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("General Information", 75);

    doc
      .fontSize(13)
      .fillColor("black")
      .moveDown(0.5)
      .text("Purpose of Valuation: " + reportA[0].generalInfo.purpose_of_valuation, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Customer Name: " + reportA[0].generalInfo.name_of_customer, 100);
    doc.moveDown(0.5).fontSize(13).text("Location: " + reportA[0].generalInfo.location, 100);
    doc.moveDown(0.5).fontSize(13).text("Date of Valuation: " + new Date(reportA[0].generalInfo.date_of_valuation, 100).toDateString());
    doc.moveDown(0.5).fontSize(13).text("Estimated Marcket Value: " + reportA[0].generalInfo.estimated_market_value, 100);
    doc.moveDown(0.5).fontSize(13).text("Document Provided: " + reportA[0].generalInfo.documents_provided, 100);


    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("Property Details", 75);

    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Revenue Survey No: " + reportA[0].propertyDetails.revenue_survey_no, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("F. P. No.: " + reportA[0].propertyDetails.fp_no, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("T.P.S. No.:  " + reportA[0].propertyDetails.tps_no, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("O.P. No.:  " + reportA[0].propertyDetails.op_no, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Flat No.:  " + reportA[0].propertyDetails.flat_no, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Mouje:  " + reportA[0].propertyDetails.mouje, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Society Name:  " + reportA[0].propertyDetails.society_name, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Landmark:  " + reportA[0].propertyDetails.landmark, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Taluka:  " + reportA[0].propertyDetails.taluka, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("District:  " + reportA[0].propertyDetails.district, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Availability of Local Transport:  " + reportA[0].propertyDetails.local_transport, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Distance from City Center: " + reportA[0].propertyDetails.distance_to_city_center, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Class of Locality: " + reportA[0].propertyDetails.class_of_locality, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Quality of Infrastructure in the Vicinity: " + reportA[0].propertyDetails.quality_of_infrastructure, 100);
    doc.moveDown(0.5).fontSize(13).fillColor('black').text("Boundaries(As per site verification): " + reportA[0].propertyDetails.quality_of_infrastructure, 100);
    doc.moveDown(0.5).fontSize(12).text("North: " + reportA[0].propertyDetails.Boundry.north_boundarie, 100);
    doc.moveDown(0.5).fontSize(12).text("South: " + reportA[0].propertyDetails.Boundry.south_boundarie, 100);
    doc.moveDown(0.5).fontSize(12).text("East: " + reportA[0].propertyDetails.Boundry.east_boundarie, 100);
    doc.moveDown(0.5).fontSize(12).text("West: " + reportA[0].propertyDetails.Boundry.west_boundarie, 100);
    doc .moveDown(0.5) .fontSize(13).fillColor('black') .text("Do the Boundaries at Site match,as mentioned in the documentation: " + reportA[0].propertyDetails.boundaries_in_doc, 100);
    doc .moveDown(0.5) .fontSize(13).fillColor('black') .text("Level of land with topographical conditions. " + reportA[0].propertyDetails.topographical_condition, 100);
    doc .moveDown(0.5) .fontSize(13).fillColor('black') .text("Status of the land / Flat: " + reportA[0].propertyDetails.status_of_land, 100);
    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Type of Property: " + reportA[0].propertyDetails.type_of_property, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Approved usage of property: " + reportA[0].propertyDetails.approved_usage, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Actual usage of the property: " + reportA[0].propertyDetails.actual_usage, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Type of Structure: " + reportA[0].propertyDetails.type_of_structure, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("No. of Floors: " + reportA[0].propertyDetails.no_of_floors, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Occupancy Details: " + reportA[0].propertyDetails.occupancy_details, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Does the property have Electricity / Water / Drainage connection: " + reportA[0].propertyDetails.property_connection, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Proximity to civic amenities like school, hospital, market, etc. " + reportA[0].propertyDetails.proximity, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Development of surrounding area: " + reportA[0].propertyDetails.development_area, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Approval Details: " + reportA[0].propertyDetails.approval_details, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Construction Details: " + reportA[0].propertyDetails.construction_details, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Usage : " + reportA[0].propertyDetails.usages_of_pro, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Whether the construction is as per approved building plan? " + reportA[0].propertyDetails.building_plan, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Recommended / Available Side Margin : " + reportA[0].propertyDetails.side_margin, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Front Margin: " + reportA[0].propertyDetails.front_margin, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Quality of construction: " + reportA[0].propertyDetails.quality_of_construction, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Maintenance of the property " + reportA[0].propertyDetails.maintenance_of_property, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Current life of the structure: " + reportA[0].propertyDetails.current_life_structure, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Estimated future life of the structure: " + reportA[0].propertyDetails.estimated_life_structure, 100);
    //left to add recommended valuation of property and values
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("Recommand Valuation", 75);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Recommended Rate of the Flat " + reportA[0].RecomValuation.recommended_rate_flat, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Value of Flat : " + reportA[0].RecomValuation.values.value_of_flat, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Value of Terrace : " + reportA[0].RecomValuation.values.value_of_terrace, 100);

    doc .moveDown(0.5) .fontSize(13).fillColor('black') .text("Total Value : " + reportA[0].RecomValuation.values.total_value, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Say : " + reportA[0].RecomValuation.values.say, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Stage of Construction: " + reportA[0].RecomValuation.stage_of_const, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("(%) Work Completed: " + reportA[0].RecomValuation.work_complited, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("(%) Disbursement Recommended: " + reportA[0].RecomValuation.disbursement_recommended, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Date of Property Visit " + reportA[0].RecomValuation.date_of_pro_visit, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Valuation as per Government reckoner rates: " + reportA[0].RecomValuation.government_rates, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Distress valuation of the property: " + reportA[0].RecomValuation.valuation_property, 100);

    doc
      .moveDown(0.5)
      .fontSize(13).fillColor('black')
      .text("Rental value per month: " + reportA[0].RecomValuation.rent_value, 100);
    doc.pipe(res);
    doc.end();
  })
}

const reportBpdf = (req,res) => {
  const urlA = "./public/uploads/b";

  const doc = new PDFDocument();

  ReportB.find({ _id: req.params.id }).then(function(reportB) {
    var image = "./public/uploads/b/" + reportB[0].images;
    var title = reportB[0].generalDetals.name_of_applicant;
    var date = new Date();
    var Dated = date.toDateString();
    var filename = encodeURIComponent(Math.random()) + ".pdf";
    res.setHeader("Content-disposition", 'attachment; filename="' + filename + '"');
    res.setHeader("Content-type", "application/pdf");
    doc
      .font("Times-Roman", 18)
      .fontSize(22)
      .fillColor("brown")
      .text("VALUATION REPORT B", 200, 50);

    // Fit the image within the dimensions
    doc.moveDown(0.5).image(image, { fit: [250, 300], align: "center" });
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("BANK INFORMATION", 75);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Bank: " + reportB[0].bankInfo.sBankName, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Branch: " + reportB[0].bankInfo.sBranchName, 100);

    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("GENERAL DETAILS", 75);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Name of Applicant: " + reportB[0].generalDetals.name_of_applicant, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Property Address: " + reportB[0].generalDetals.property_address, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Area details: " + reportB[0].generalDetals.area_details, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Name of Proposed Buyer: " + reportB[0].generalDetals.proposed_buyer, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Loan application number: " + reportB[0].generalDetals.loan_application_number, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Legal address of property: " + reportB[0].generalDetals.legal_address_of_property, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Date of Inspection: " + reportB[0].generalDetals.date_of_inspection, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Purpose of Inspection: " + reportB[0].generalDetals.purpose_of_inspection, 100);    
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("SURROUNDING LOCALITY DETAILS", 75);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Ward no./ Municipal land No: " + reportB[0].surrLocDetails.ward_no, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Vicinity: " + reportB[0].surrLocDetails.vicinity, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Plot Demarcated at the site: " + reportB[0].surrLocDetails.plot_site, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Type: " + reportB[0].surrLocDetails.pro_type, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Conditions of Approach Road: " + reportB[0].surrLocDetails.conditions_of_road, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Nearest landmark: " + reportB[0].surrLocDetails.nearest_landmark, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Proximity to Civic Amenities: " + reportB[0].surrLocDetails.local_transport, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Other: " + reportB[0].surrLocDetails.local_transport_other, 100);    
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Property Identified through: " + reportB[0].surrLocDetails.pro_identified_through, 100);    
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("PROPERTY DETAILS", 75);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Type of Usage of entire property: " + reportB[0].propDetails.usage_entire_pro, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Additional Amenities: " + reportB[0].propDetails.additional_amenities, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Type of Premises: " + reportB[0].propDetails.type_of_premises, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Occupied by: " + reportB[0].propDetails.occupied_by, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Name of occupant: " + reportB[0].propDetails.name_of_occupant, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Is property rented?:" + reportB[0].propDetails.property_rented, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("If relative, state relationship with the applicant:" + reportB[0].propDetails.if_relative, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("If rented list of occupants:" + reportB[0].propDetails.if_rented, 100); 
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("PROPERTY DETAILS - BOUNDRY ARE - - ", 75);
    doc
      .moveDown(0.5)
      .fontSize(12)
      .fillColor("black")
      .text("North: " + reportB[0].propDetails.Boundry.north_boundarie, 100); 
    doc
      .moveDown(0.5)
      .fontSize(12)
      .fillColor("black")
      .text("South: " + reportB[0].propDetails.Boundry.south_boundarie, 100); 
    doc
      .moveDown(0.5)
      .fontSize(12)
      .fillColor("black")
      .text("East: " + reportB[0].propDetails.Boundry.east_boundarie, 100); 
    doc
      .moveDown(0.5)
      .fontSize(12)
      .fillColor("black")
      .text("West: " + reportB[0].propDetails.Boundry.west_boundarie, 100);     
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("STRUCTURE DETAILS", 75);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Type of Structure: " + reportB[0].structDetail.type_of_structure, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("No. of Floors: " + reportB[0].structDetail.no_of_floors, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("No. of dwelling units: " + reportB[0].structDetail.no_of_dwelling, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("No. of Wings: " + reportB[0].structDetail.no_of_wings, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("No. of Lifts in each wing: " + reportB[0].structDetail.no_of_lifts, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Construction Year: " + reportB[0].structDetail.construction_year, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Estimated Future Life: " + reportB[0].structDetail.future_life, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Age of property: " + reportB[0].structDetail.age_of_property, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Situated on Floor No.: " + reportB[0].structDetail.situated_floor_no, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Quality of Fittings: " + reportB[0].structDetail.quality_fittings, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Internal Composition:  " + reportB[0].structDetail.Internal_composition, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Flooring: " + reportB[0].structDetail.flooring, 100);
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(14)
      .fillColor("brown")
      .text("QUALITY OF CONSTRUCTION", 75);
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("A. Exteriors", 85);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Beam & Column Structure: " + reportB[0].QualityConst.AExteriors.beam_column, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Appearance & Maintenance of Building: " + reportB[0].QualityConst.AExteriors.appearance_maintenance, 100); 
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Common Area Remarks: " + reportB[0].QualityConst.AExteriors.common_remarks, 100); 
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("B. Exteriors", 85);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Finishing: " + reportB[0].QualityConst.BExteriors.finishing, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Roofing and Terracing: " + reportB[0].QualityConst.BExteriors.roofing_terracing, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("No. of Lifts: " + reportB[0].QualityConst.BExteriors.no_of_lift, 100); 
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(14)
      .fillColor("brown")
      .text("PLAN APPROVALS", 75);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Construction as per approved: " + reportB[0].planApproval.constr_per_approved, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Details of approved plan & plans: " + reportB[0].planApproval.details_approved_plans, 100);

    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Approved Date: " + reportB[0].planApproval.approval_date, 100);

    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Bldg. use Permission Number and Date: " + reportB[0].planApproval.use_permission, 100);

    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Violations Observed if Any: " + reportB[0].planApproval.violations_observed, 100);

    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("If plans not available then is the structure confirming to the local byelaws : " + reportB[0].planApproval.local_byelaws, 100);
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(14)
      .fillColor("brown")
      .text("QUALITY OF CONSTRUCTION", 75);
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(12)
      .fillColor("brown")
      .text("INDIVIDUAL APARTMENT / FLAT / SHOP / OFFICE", 85);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Built up area (Sq. Ft " + reportB[0].valuation.built_up_area, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Loading for SBUA " + reportB[0].valuation.loading_sbua, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Current Govt. Approved Rates: " + reportB[0].valuation.approved_rates, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Final Value: " + reportB[0].valuation.final_value, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Recommended Rate and basis for recommendation: " + reportB[0].valuation.recommended_rate, 100);
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(12)
      .fillColor("brown")
      .text("FOR VALUATION LAND & BUILDING", 85);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Land Area: " + reportB[0].valuation.land_area, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Current Government Approved Rates for Land (Jantry Rate): " + reportB[0].valuation.current_rates_for_land, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Recommended Rate and Basis For recommendation: " + reportB[0].valuation.land_recommended_rate, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Land Value: " + reportB[0].valuation.land_value, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Actual BUA of the premises: " + reportB[0].valuation.bua_premises, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Construction cost as per the Amenities provided per Sq. Ft.: " + reportB[0].valuation.construction_cost, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Total Value of Construction: " + reportB[0].valuation.total_value_const, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Depreciate value of Construction (98.25%): " + reportB[0].valuation.depreciate_value_const, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Total valuation: " + reportB[0].valuation.total_valuation, 100);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Say: " + reportB[0].valuation.say, 100);

    doc.pipe(res);
    doc.end();
  });

}



 const reportLapPdf = (req,res) => {
   const urlA = "./public/uploads/c";
   const doc = new PDFDocument();
   ReportLap.find({ _id: req.params.id }).then(function (reportLap) {
    var image = "./public/uploads/c/" + reportLap[0].images;
    var date = new Date();
    var Dated = date.toDateString();
    var filename = encodeURIComponent(Math.random()) + ".pdf";
    res.setHeader("Content-disposition", 'attachment; filename="' + filename + '"');
    res.setHeader("Content-type", "application/pdf");
    doc
      .font("Times-Roman", 18)
      .fontSize(22)
      .fillColor("brown")
      .text("VALUATION REPORT LAP", 200, 50);

    // Fit the image within the dimensions
    doc.moveDown(0.5).image(image, { fit: [250, 300], align: "center" });
    doc
      .moveDown(0.5)
      .font("Times-Roman", 16)
      .fontSize(15)
      .fillColor("brown")
      .text("BANK INFORMATION", 75);
    doc
      .moveDown(0.5)
      .fontSize(13)
      .fillColor("black")
      .text("Bank: " + reportLap[0].bankInfo.sBankName, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Branch: " + reportLap[0].bankInfo.sBranchName, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(15)
       .fillColor("brown")
       .text("GENERAL DETAILS", 75);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Valuation request given by: " + reportLap[0].generalInfo.valuation_request, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("The Property is occupied by: " + reportLap[0].generalInfo.property_occupied_by, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Purpose of Valuation: " + reportLap[0].generalInfo.purpose_of_valuation, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Date of Valuation Report " + reportLap[0].generalInfo.date_of_valuation, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Valuation done by (Name of Field Engineer) and date of Site Visit: " + reportLap[0].generalInfo.valuation_done_by, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Name of the person who has done assessment of FMV & vetted field investigation report & signed the report: " + reportLap[0].generalInfo.assessment_of_fmv, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(15)
       .fillColor("brown")
       .text("CUSTOMER DETAILS", 75);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Customer Name: " + reportLap[0].customerDetails.customer_name, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Application Number (Of file) : " + reportLap[0].customerDetails.application_no, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Prospective Purchaser Name (as reported): " + reportLap[0].customerDetails.pro_purchaser_name, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Name of the person met (with phone No.): " + reportLap[0].customerDetails.person_met, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(15)
       .fillColor("brown")
       .text("PROPERTY DETAILS", 75);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Address of Property(at site): " + reportLap[0].propertyDetails.address_of_property, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Address as per Legal Document: " + reportLap[0].propertyDetails.address_as_document, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Nearby Landmark: " + reportLap[0].propertyDetails.nearby_landmark, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Specific Comments On Property: " + reportLap[0].propertyDetails.comment_on_property, 100);
     doc
       .moveDown(1)
       .font("Times-Roman", 16)
       .fontSize(15)
       .fillColor("brown")
       .text("DOCUMENT DETAILS", 75);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approved Layout Plan: " + reportLap[0].documentDetails.approved_layout_plan, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Date & Number: " + reportLap[0].documentDetails.layout_date_num, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Authority: " + reportLap[0].documentDetails.layout_authority, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approved Building Plan " + reportLap[0].documentDetails.approved_building_plan, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Date & Number: " + reportLap[0].documentDetails.building_date_num, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Authority:< " + reportLap[0].documentDetails.building_authority, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Construction Permission " + reportLap[0].documentDetails.construction_per, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Date & Number: " + reportLap[0].documentDetails.construction_date_num, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Authority: " + reportLap[0].documentDetails.construction_authority, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("B. U. Permission " + reportLap[0].documentDetails.bu_permission, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Date & Number: " + reportLap[0].documentDetails.bu_date_num, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approving Authority: " + reportLap[0].documentDetails.bu_authority, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Type of Usage as per Plan: " + reportLap[0].documentDetails.usage_as_plan, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Type of Usage at Site: " + reportLap[0].documentDetails.type_of_usage, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("List of Property Document Produced for perusal: " + reportLap[0].documentDetails.document_list, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(15)
       .fillColor("brown")
       .text("OCCUPANCY DETAILS", 75);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Status of Occupancy : " + reportLap[0].occupancyDetail.status_of_occupancy, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Occupied by: " + reportLap[0].occupancyDetail.occupied_by, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Relationship of Occupant with customer: " + reportLap[0].occupancyDetail.relationship_with_customer, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Occupied Since: " + reportLap[0].occupancyDetail.occupied_since, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Completion Status of building/property: " + reportLap[0].occupancyDetail.status_property, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Rental Value: " + reportLap[0].occupancyDetail.rental_value, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(15)
       .fillColor("brown")
       .text("PHYSICAL DETAILS", 75);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(14)
       .fillColor("brown")
       .text("Boundaries-", 85);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("North:: As per Sale Documents: " + reportLap[0].physicalDetail.Boundaries.north_doc, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("North:: Actual at Site: " + reportLap[0].physicalDetail.Boundaries.north_site, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("South:: As per Sale Documents: " + reportLap[0].physicalDetail.Boundaries.south_doc, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("South:: Actual at Site: " + reportLap[0].physicalDetail.Boundaries.south_site, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("East:: As per Sale Documents:" + reportLap[0].physicalDetail.Boundaries.east_doc, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("East:: Actual at Site: " + reportLap[0].physicalDetail.Boundaries.east_site, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("West:: As per Sale Documents: " + reportLap[0].physicalDetail.Boundaries.west_doc, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("West:: Actual at Site: " + reportLap[0].physicalDetail.Boundaries.west_site, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Boundaries Matching: " + reportLap[0].physicalDetail.Boundaries.boundaries_match, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Remarks: " + reportLap[0].physicalDetail.Boundaries.boundaries_remark, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(14)
       .fillColor("brown")
       .text("Plot Details-", 85);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Plot Area: " + reportLap[0].physicalDetail.Plot.ploat_area, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Flat demarcated at site: " + reportLap[0].physicalDetail.Plot.flat_demarcated_site, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approved Land / Building Use: " + reportLap[0].physicalDetail.Plot.approved_use, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Infrastructure of the surrounding area: " + reportLap[0].physicalDetail.Plot.Infrastructure_area, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Nature of Locality: " + reportLap[0].physicalDetail.Plot.nature_of_locality, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Class of Locality: " + reportLap[0].physicalDetail.Plot.class_of_locality, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Distance from Indusind office (km): " + reportLap[0].physicalDetail.Plot.distance_from_office, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Property Location: " + reportLap[0].physicalDetail.Plot.property_location, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Electricity Service Connection: " + reportLap[0].physicalDetail.Plot.elec_service, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Type of Property: " + reportLap[0].physicalDetail.Plot.property_type, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(14)
       .fillColor("brown")
       .text("Property Description (Floor-wise) -", 85);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Property Structure: " + reportLap[0].physicalDetail.propDesc.property_structure, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("No. of Units: " + reportLap[0].physicalDetail.propDesc.no_of_units, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text(" No. of Kitchen: " + reportLap[0].physicalDetail.propDesc.no_of_kitchen, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("No. of Bed Rooms: " + reportLap[0].physicalDetail.propDesc.no_of_bed, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text(" Usage & Remarks if any: " + reportLap[0].physicalDetail.propDesc.usage_remark, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Maintenance level: " + reportLap[0].physicalDetail.propDesc.maintenance_level, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Structure: " + reportLap[0].physicalDetail.propDesc.structure, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Interiors: " + reportLap[0].physicalDetail.propDesc.interiors, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Exteriors: " + reportLap[0].physicalDetail.propDesc.exteriors, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Approx Age of Property: " + reportLap[0].physicalDetail.propDesc.age_of_property, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Amenities: " + reportLap[0].physicalDetail.propDesc.amenities, 100);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(15)
       .fillColor("brown")
       .text("VALUATION", 75);
     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(14)
       .fillColor("brown")
       .text("A. AREA DETAILS & DEVIATION (RAW HOUSE / BUNGALOW / KOTHI):-", 85);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Land Area:: As per approved plan: " + reportLap[0].Valuation.areaDetailsA.land_as_plan_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Land Area:: Actual area(As observed & Measured): " + reportLap[0].Valuation.areaDetailsA.land_as_area_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Ground Floor:: As per approved plan: " + reportLap[0].Valuation.areaDetailsA.ground_as_plan_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Ground Floor:: Actual area(As observed & Measured): " + reportLap[0].Valuation.areaDetailsA.ground_as_area_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("First Floor:: As per approved plan: " + reportLap[0].Valuation.areaDetailsA.first_as_plan_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("First Floor:: Actual area(As observed & Measured): " + reportLap[0].Valuation.areaDetailsA.first_as_area_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Second Floor:: As per approved plan: " + reportLap[0].Valuation.areaDetailsA.second_as_plan_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Second Floor:: Actual area(As observed & Measured): " + reportLap[0].Valuation.areaDetailsA.second_as_area_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Prevailing Market Rate:: As per approved plan: " + reportLap[0].Valuation.areaDetailsA.rate_as_plan_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Prevailing Market Rate:: Actual area(As observed & Measured): " + reportLap[0].Valuation.areaDetailsA.rate_as_area_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Land Rate adopted (in Sq. Ft.):  " + reportLap[0].Valuation.areaDetailsA.rate_adopted_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Cost of Construction (in Sq. Ft.): " + reportLap[0].Valuation.areaDetailsA.cost_construction_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Land Value: " + reportLap[0].Valuation.areaDetailsA.land_value_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Building Value(of total approved area): " + reportLap[0].Valuation.areaDetailsA.building_value_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Depreciation considered(on building value): " + reportLap[0].Valuation.areaDetailsA.depreciation_considered_a, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Fair Market Value of the property: " + reportLap[0].Valuation.areaDetailsA.fair_market_value_a, 100);

     doc
       .moveDown(0.5)
       .font("Times-Roman", 16)
       .fontSize(14)
       .fillColor("brown")
       .text("B. AREA DETAILS & DEVIATIONS (FLAT / OFFICE / SHOP):-", 85);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Land Area: As per approved plan: " + reportLap[0].Valuation.areaDetailsB.land_as_plan_b, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Land Area: Actual area(As observd & Measured): " + reportLap[0].Valuation.areaDetailsB.land_as_area_b, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Carpet Area: As per approved plan:" + reportLap[0].Valuation.areaDetailsB.carpet_as_plan_b, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Carpet Area: Actual area(As observd & Measured):" + reportLap[0].Valuation.areaDetailsB.carpet_as_area_b, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Area Details: As per approved plan: " + reportLap[0].Valuation.areaDetailsB.area_detail_as_plan, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Area Details: Actual area(As observed & Measured): " + reportLap[0].Valuation.areaDetailsB.area_detail_as_area, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Prevailing Market Rate: " + reportLap[0].Valuation.areaDetailsB.prevailing_rate, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Total Value: " + reportLap[0].Valuation.areaDetailsB.total_value, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Amenities cost (fix furniture & interior work): " + reportLap[0].Valuation.areaDetailsB.amenities_cost, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Depreciation considered (on building value): " + reportLap[0].Valuation.areaDetailsB.depreciation_building, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Total fair market value of the property: " + reportLap[0].Valuation.areaDetailsB.total_fair_value, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Say: " + reportLap[0].Valuation.areaDetailsB.say, 100);
     doc
       .moveDown(0.5)
       .fontSize(13)
       .fillColor("black")
       .text("Remarks: " + reportLap[0].Valuation.areaDetailsB.remarks, 100);

     doc.pipe(res);
     doc.end();
   })
}


module.exports = {
  loanPdf,
  reportApdf,
  reportBpdf,
  reportLapPdf
};