const User = require("../models/user");
const mongoose = require("mongoose");
const Bank = require("../models/bank_model");
const cli = require("../config/cli").console;
var bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator/check");
const ReportB = require("../models/valuation_report_pro_model");
const debug = require("debug")("http");


mongoose.Promise = Promise;
const multer = require("multer");
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/uploads/b");
  },
  filename: function(req, file, cb) {
    var datetimestamp = Date.now() + Math.random();
    cb(
      null,
      file.fieldname +
        "-" +
        datetimestamp +
        "." +
        file.originalname.split(".")[file.originalname.split(".").length - 1]
    );
  }
});

var upload = multer({
  storage: storage
}).fields([
  {
    name: "files",
    maxCount: 1
  }
]);
const valuation_report_b = (req, res, next) => {
  upload(req, res, function(err) {
    req
      .checkBody("sBankName")
      .notEmpty()
      .withMessage("sBankName Required!");
    req
      .checkBody("sBranchName")
      .notEmpty()
      .withMessage("sBranchName Required!");
    req
      .checkBody("name_of_applicant")
      .notEmpty()
      .withMessage("name_of_applicant Required!");
    req
      .checkBody("property_address")
      .notEmpty()
      .withMessage("property_address Required!");
    req
      .checkBody("area_details")
      .notEmpty()
      .withMessage("area_details Required!");
    req
      .checkBody("proposed_buyer")
      .notEmpty()
      .withMessage("proposed_buyer Required!");
    req
      .checkBody("loan_application_number")
      .notEmpty()
      .withMessage("loan_application_number Required!");
    req
      .checkBody("legal_address_of_property")
      .notEmpty()
      .withMessage("legal_address_of_property Required!");
    req
      .checkBody("date_of_inspection")
      .notEmpty()
      .withMessage("date_of_inspection Required!");
    req
      .checkBody("purpose_of_inspection")
      .notEmpty()
      .withMessage("purpose_of_inspection Required!");
    req
      .checkBody("ward_no")
      .notEmpty()
      .withMessage("ward_no Required!");
    req
      .checkBody("vicinity")
      .notEmpty()
      .withMessage("vicinity Required!");
    req
      .checkBody("plot_site")
      .notEmpty()
      .withMessage("plot_site Required!");
    req
      .checkBody("pro_type")
      .notEmpty()
      .withMessage("pro_type Required!");
    req
      .checkBody("conditions_of_road")
      .notEmpty()
      .withMessage("conditions_of_road Required!");
    req
      .checkBody("nearest_landmark")
      .notEmpty()
      .withMessage("nearest_landmark Required!");
    req
      .checkBody("local_transport")
      .notEmpty()
      .withMessage("local_transport Required!");
    req
      .checkBody("pro_identified_through")
      .notEmpty()
      .withMessage("pro_identified_through Required!");
    req
      .checkBody("usage_entire_pro")
      .notEmpty()
      .withMessage("usage_entire_pro Required!");
    req
      .checkBody("additional_amenities")
      .notEmpty()
      .withMessage("additional_amenities Required!");
    req
      .checkBody("type_of_premises")
      .notEmpty()
      .withMessage("type_of_premises Required!");
    req
      .checkBody("occupied_by")
      .notEmpty()
      .withMessage("occupied_by Required!");
    req
      .checkBody("name_of_occupant")
      .notEmpty()
      .withMessage("name_of_occupant Required!");
    req
      .checkBody("property_rented")
      .notEmpty()
      .withMessage("property_rented Required!");
    req
      .checkBody("if_relative")
      .notEmpty()
      .withMessage("if_relative Required!");
    req
      .checkBody("if_rented")
      .notEmpty()
      .withMessage("if_rented Required!");
    req
      .checkBody("north_boundarie")
      .notEmpty()
      .withMessage("north_boundarie Required!");
    req
      .checkBody("south_boundarie")
      .notEmpty()
      .withMessage("south_boundarie Required!");
    req
      .checkBody("east_boundarie")
      .notEmpty()
      .withMessage("east_boundarie Required!");
    req
      .checkBody("west_boundarie")
      .notEmpty()
      .withMessage("west_boundarie Required!");
    req
      .checkBody("type_of_structure")
      .notEmpty()
      .withMessage("type_of_structure Required!");
    req
      .checkBody("no_of_floors")
      .notEmpty()
      .withMessage("no_of_floors Required!");
    req
      .checkBody("no_of_dwelling")
      .notEmpty()
      .withMessage("no_of_dwelling Required!");
    req
      .checkBody("no_of_wings")
      .notEmpty()
      .withMessage("no_of_wings Required!");
    req
      .checkBody("no_of_lifts")
      .notEmpty()
      .withMessage("no_of_lifts Required!");
    req
      .checkBody("construction_year")
      .notEmpty()
      .withMessage("construction_year Required!");
    req
      .checkBody("future_life")
      .notEmpty()
      .withMessage("future_life Required!");
    req
      .checkBody("age_of_property")
      .notEmpty()
      .withMessage("age_of_property Required!");
    req
      .checkBody("beam_column")
      .notEmpty()
      .withMessage("beam_column Required!");
    req
      .checkBody("appearance_maintenance")
      .notEmpty()
      .withMessage("appearance_maintenance Required!");
    req
      .checkBody("common_remarks")
      .notEmpty()
      .withMessage("common_remarks Required!");
    req
      .checkBody("finishing")
      .notEmpty()
      .withMessage("finishing Required!");
    req
      .checkBody("roofing_terracing")
      .notEmpty()
      .withMessage("roofing_terracing Required!");
    req
      .checkBody("no_of_lift")
      .notEmpty()
      .withMessage("no_of_lift Required!");
    req
      .checkBody("constr_per_approved")
      .notEmpty()
      .withMessage("constr_per_approved Required!");
    req
      .checkBody("details_approved_plans")
      .notEmpty()
      .withMessage("details_approved_plans Required!");
    req
      .checkBody("approval_date")
      .notEmpty()
      .withMessage("approval_date Required!");
    req
      .checkBody("use_permission")
      .notEmpty()
      .withMessage("use_permission Required!");
    req
      .checkBody("violations_observed")
      .notEmpty()
      .withMessage("violations_observed Required!");
    req
      .checkBody("local_byelaws")
      .notEmpty()
      .withMessage("local_byelaws Required!");
    req
      .checkBody("situated_floor_no")
      .notEmpty()
      .withMessage("situated_floor_no Required!");
    req
      .checkBody("quality_fittings")
      .notEmpty()
      .withMessage("quality_fittings Required!");
    req
      .checkBody("Internal_composition")
      .notEmpty()
      .withMessage("Internal_composition Required!");
    req
      .checkBody("flooring")
      .notEmpty()
      .withMessage("flooring Required!");
    req
      .checkBody("built_up_area")
      .notEmpty()
      .withMessage("built_up_area Required!");
    req
      .checkBody("loading_sbua")
      .notEmpty()
      .withMessage("loading_sbua Required!");
    req
      .checkBody("approved_rates")
      .notEmpty()
      .withMessage("approved_rates Required!");
    req
      .checkBody("final_value")
      .notEmpty()
      .withMessage("final_value Required!");
    req
      .checkBody("recommended_rate")
      .notEmpty()
      .withMessage("recommended_rate Required!");
    req
      .checkBody("land_area")
      .notEmpty()
      .withMessage("land_area Required!");
    req
      .checkBody("current_rates_for_land")
      .notEmpty()
      .withMessage("current_rates_for_land Required!");
    req
      .checkBody("land_recommended_rate")
      .notEmpty()
      .withMessage("land_recommended_rate Required!");
    req
      .checkBody("land_value")
      .notEmpty()
      .withMessage("land_value Required!");
    req
      .checkBody("bua_premises")
      .notEmpty()
      .withMessage("bua_premises Required!");
    req
      .checkBody("construction_cost")
      .notEmpty()
      .withMessage("construction_cost Required!");
    req
      .checkBody("total_value_const")
      .notEmpty()
      .withMessage("total_value_const Required!");
    req
      .checkBody("depreciate_value_const")
      .notEmpty()
      .withMessage("depreciate_value_const Required!");
    req
      .checkBody("total_valuation")
      .notEmpty()
      .withMessage("total_valuation Required!");
    req
      .checkBody("say")
      .notEmpty()
      .withMessage("say Required!");

    req.getValidationResult().then(result => {
      var errors = result.array();
      console.log(errors);
      if (result.array().length > 0) {
        res.render("valuation_report_b", {
          error: errors[0].msg,
          sBankName: req.body.sBankName,
          sBranchName: req.body.sBranchName,
          serial_no: req.body.serial_no,
          name_of_applicant: req.body.name_of_applicant,
          property_address: req.body.property_address,
          area_details: req.body.area_details,
          proposed_buyer: req.body.proposed_buyer,
          loan_application_number: req.body.loan_application_number,
          legal_address_of_property: req.body.legal_address_of_property,
          date_of_inspection: req.body.date_of_inspection,
          purpose_of_inspection: req.body.purpose_of_inspection,
          ward_no: req.body.ward_no,
          vicinity: req.body.vicinity,
          plot_site: req.body.plot_site,
          pro_type: req.body.pro_type,
          conditions_of_road: req.body.conditions_of_road,
          nearest_landmark: req.body.nearest_landmark,
          local_transport: req.body.local_transport,
          local_transport_other: req.body.local_transport_other,
          pro_identified_through: req.body.pro_identified_through,
          usage_entire_pro: req.body.usage_entire_pro,
          additional_amenities: req.body.additional_amenities,
          type_of_premises: req.body.type_of_premises,
          occupied_by: req.body.occupied_by,
          name_of_occupant: req.body.name_of_occupant,
          property_rented: req.body.property_rented,
          if_relative: req.body.if_relative,
          if_rented: req.body.if_rented,
          north_boundarie: req.body.north_boundarie,
          south_boundarie: req.body.south_boundarie,
          east_boundarie: req.body.east_boundarie,
          west_boundarie: req.body.west_boundarie,
          type_of_structure: req.body.type_of_structure,
          no_of_floors: req.body.no_of_floors,
          no_of_dwelling: req.body.no_of_dwelling,
          no_of_wings: req.body.no_of_wings,
          no_of_lifts: req.body.no_of_lifts,
          construction_year: req.body.construction_year,
          future_life: req.body.future_life,
          age_of_property: req.body.age_of_property,
          beam_column: req.body.beam_column,
          appearance_maintenance: req.body.appearance_maintenance,
          common_remarks: req.body.common_remarks,
          finishing: req.body.finishing,
          roofing_terracing: req.body.roofing_terracing,
          no_of_lift: req.body.no_of_lift,
          constr_per_approved: req.body.constr_per_approved,
          details_approved_plans: req.body.details_approved_plans,
          approval_date: req.body.approval_date,
          use_permission: req.body.use_permission,
          violations_observed: req.body.violations_observed,
          local_byelaws: req.body.local_byelaws,
          situated_floor_no: req.body.situated_floor_no,
          quality_fittings: req.body.quality_fittings,
          Internal_composition: req.body.Internal_composition,
          flooring: req.body.flooring,
          built_up_area: req.body.built_up_area,
          loading_sbua: req.body.loading_sbua,
          approved_rates: req.body.approved_rates,
          final_value: req.body.final_value,
          recommended_rate: req.body.recommended_rate,
          land_area: req.body.land_area,
          current_rates_for_land: req.body.current_rates_for_land,
          land_recommended_rate: req.body.land_recommended_rate,
          land_value: req.body.land_value,
          bua_premises: req.body.bua_premises,
          construction_cost: req.body.construction_cost,
          total_value_const: req.body.total_value_const,
          depreciate_value_const: req.body.depreciate_value_const,
          total_valuation: req.body.total_valuation,
          say: req.body.say
        });
      } else {
        // documents_provided = req.body.documents_provided.split(',');
        let files;
        console.log(req.files);
        var keys = Object.keys(req.files);
        for (var elem in keys) {
          var fieldname = req.files[keys[elem]][0].fieldname;
          if (fieldname == "files") {
            req.body.files = req.files[keys[elem]][0].filename;
          }
        }

        const body = {
          images: req.body.files,
          serial_no: req.body.serial_no,
          bankInfo: {
            sBankName: req.body.sBankName,
            sBranchName: req.body.sBranchName
          },
          generalDetals: {
            name_of_applicant: req.body.name_of_applicant,
            property_address: req.body.property_address,
            area_details: req.body.area_details,
            proposed_buyer: req.body.proposed_buyer,
            loan_application_number: req.body.loan_application_number,
            legal_address_of_property: req.body.legal_address_of_property,
            date_of_inspection: req.body.date_of_inspection,
            purpose_of_inspection: req.body.purpose_of_inspection
          },
          surrLocDetails: {
            ward_no: req.body.ward_no,
            vicinity: req.body.vicinity,
            plot_site: req.body.plot_site,
            pro_type: req.body.pro_type,
            conditions_of_road: req.body.conditions_of_road,
            nearest_landmark: req.body.nearest_landmark,
            local_transport: req.body.local_transport,
            local_transport_other: req.body.local_transport_other,
            pro_identified_through: req.body.pro_identified_through
          },
          propDetails: {
            usage_entire_pro: req.body.usage_entire_pro,
            additional_amenities: req.body.additional_amenities,
            type_of_premises: req.body.type_of_premises,
            occupied_by: req.body.occupied_by,
            name_of_occupant: req.body.name_of_occupant,
            property_rented: req.body.property_rented,
            if_relative: req.body.if_relative,
            if_rented: req.body.if_rented,
            Boundry: {
              north_boundarie: req.body.north_boundarie,
              south_boundarie: req.body.south_boundarie,
              east_boundarie: req.body.east_boundarie,
              west_boundarie: req.body.west_boundarie
            }
          },
          structDetail: {
            type_of_structure: req.body.type_of_structure,
            no_of_floors: req.body.no_of_floors,
            no_of_dwelling: req.body.no_of_dwelling,
            no_of_wings: req.body.no_of_wings,
            no_of_lifts: req.body.no_of_lifts,
            construction_year: req.body.construction_year,
            future_life: req.body.future_life,
            age_of_property: req.body.age_of_property,
            situated_floor_no: req.body.situated_floor_no,
            quality_fittings: req.body.quality_fittings,
            Internal_composition: req.body.Internal_composition,
            flooring: req.body.flooring
          },
          QualityConst: {
            AExteriors: {
              beam_column: req.body.beam_column,
              appearance_maintenance: req.body.appearance_maintenance,
              common_remarks: req.body.common_remarks
            },
            BExteriors: {
              finishing: req.body.finishing,
              roofing_terracing: req.body.roofing_terracing,
              no_of_lift: req.body.no_of_lift
            }
          },
          planApproval: {
            constr_per_approved: req.body.constr_per_approved,
            details_approved_plans: req.body.details_approved_plans,
            approval_date: req.body.approval_date,
            use_permission: req.body.use_permission,
            violations_observed: req.body.violations_observed,
            local_byelaws: req.body.local_byelaws
          },
          valuation: {
            built_up_area: req.body.built_up_area,
            loading_sbua: req.body.loading_sbua,
            approved_rates: req.body.approved_rates,
            final_value: req.body.final_value,
            recommended_rate: req.body.recommended_rate,
            land_area: req.body.land_area,
            current_rates_for_land: req.body.current_rates_for_land,
            land_recommended_rate: req.body.land_recommended_rate,
            land_value: req.body.land_value,
            bua_premises: req.body.bua_premises,
            construction_cost: req.body.construction_cost,
            total_value_const: req.body.total_value_const,
            depreciate_value_const: req.body.depreciate_value_const,
            total_valuation: req.body.total_valuation,
            say: req.body.say
          },
          remarks: req.body.remarks
        };
        const newData = new ReportB(body);
        newData.save((error, success) => {
          if (error) {
            return res.status(500).jsonp(error);
          } else {
            req.flash("success_msg", "Valuation Report B added");
            res.redirect("/");
          }
        });
      }
    });
  });
};

const DeleteB = (req, res, next) => {
  ReportB.remove({
    _id: req.params.id
  }).then(() => {
    req.flash("success_msg", "Valuation Report B removed");
    res.redirect("/valuation_report_b_view");
  });
};

const editB = (req, res, next) => {
  req
    .checkBody("sBankName")
    .notEmpty()
    .withMessage("sBankName Required!");
  req
    .checkBody("sBranchName")
    .notEmpty()
    .withMessage("sBranchName Required!");
  req
    .checkBody("name_of_applicant")
    .notEmpty()
    .withMessage("name_of_applicant Required!");
  req
    .checkBody("property_address")
    .notEmpty()
    .withMessage("property_address Required!");
  req
    .checkBody("area_details")
    .notEmpty()
    .withMessage("area_details Required!");
  req
    .checkBody("proposed_buyer")
    .notEmpty()
    .withMessage("proposed_buyer Required!");
  req
    .checkBody("loan_application_number")
    .notEmpty()
    .withMessage("loan_application_number Required!");
  req
    .checkBody("legal_address_of_property")
    .notEmpty()
    .withMessage("legal_address_of_property Required!");
  req
    .checkBody("date_of_inspection")
    .notEmpty()
    .withMessage("date_of_inspection Required!");
  req
    .checkBody("purpose_of_inspection")
    .notEmpty()
    .withMessage("purpose_of_inspection Required!");
  req
    .checkBody("ward_no")
    .notEmpty()
    .withMessage("ward_no Required!");
  req
    .checkBody("vicinity")
    .notEmpty()
    .withMessage("vicinity Required!");
  req
    .checkBody("plot_site")
    .notEmpty()
    .withMessage("plot_site Required!");
  req
    .checkBody("pro_type")
    .notEmpty()
    .withMessage("pro_type Required!");
  req
    .checkBody("conditions_of_road")
    .notEmpty()
    .withMessage("conditions_of_road Required!");
  req
    .checkBody("nearest_landmark")
    .notEmpty()
    .withMessage("nearest_landmark Required!");
  req
    .checkBody("local_transport")
    .notEmpty()
    .withMessage("local_transport Required!");
  req
    .checkBody("pro_identified_through")
    .notEmpty()
    .withMessage("pro_identified_through Required!");
  req
    .checkBody("usage_entire_pro")
    .notEmpty()
    .withMessage("usage_entire_pro Required!");
  req
    .checkBody("additional_amenities")
    .notEmpty()
    .withMessage("additional_amenities Required!");
  req
    .checkBody("type_of_premises")
    .notEmpty()
    .withMessage("type_of_premises Required!");
  req
    .checkBody("occupied_by")
    .notEmpty()
    .withMessage("occupied_by Required!");
  req
    .checkBody("name_of_occupant")
    .notEmpty()
    .withMessage("name_of_occupant Required!");
  req
    .checkBody("property_rented")
    .notEmpty()
    .withMessage("property_rented Required!");
  req
    .checkBody("if_relative")
    .notEmpty()
    .withMessage("if_relative Required!");
  req
    .checkBody("if_rented")
    .notEmpty()
    .withMessage("if_rented Required!");
  req
    .checkBody("north_boundarie")
    .notEmpty()
    .withMessage("north_boundarie Required!");
  req
    .checkBody("south_boundarie")
    .notEmpty()
    .withMessage("south_boundarie Required!");
  req
    .checkBody("east_boundarie")
    .notEmpty()
    .withMessage("east_boundarie Required!");
  req
    .checkBody("west_boundarie")
    .notEmpty()
    .withMessage("west_boundarie Required!");
  req
    .checkBody("type_of_structure")
    .notEmpty()
    .withMessage("type_of_structure Required!");
  req
    .checkBody("no_of_floors")
    .notEmpty()
    .withMessage("no_of_floors Required!");
  req
    .checkBody("no_of_dwelling")
    .notEmpty()
    .withMessage("no_of_dwelling Required!");
  req
    .checkBody("no_of_wings")
    .notEmpty()
    .withMessage("no_of_wings Required!");
  req
    .checkBody("no_of_lifts")
    .notEmpty()
    .withMessage("no_of_lifts Required!");
  req
    .checkBody("construction_year")
    .notEmpty()
    .withMessage("construction_year Required!");
  req
    .checkBody("future_life")
    .notEmpty()
    .withMessage("future_life Required!");
  req
    .checkBody("age_of_property")
    .notEmpty()
    .withMessage("age_of_property Required!");
  req
    .checkBody("beam_column")
    .notEmpty()
    .withMessage("beam_column Required!");
  req
    .checkBody("appearance_maintenance")
    .notEmpty()
    .withMessage("appearance_maintenance Required!");
  req
    .checkBody("common_remarks")
    .notEmpty()
    .withMessage("common_remarks Required!");
  req
    .checkBody("finishing")
    .notEmpty()
    .withMessage("finishing Required!");
  req
    .checkBody("roofing_terracing")
    .notEmpty()
    .withMessage("roofing_terracing Required!");
  req
    .checkBody("no_of_lift")
    .notEmpty()
    .withMessage("no_of_lift Required!");
  req
    .checkBody("constr_per_approved")
    .notEmpty()
    .withMessage("constr_per_approved Required!");
  req
    .checkBody("details_approved_plans")
    .notEmpty()
    .withMessage("details_approved_plans Required!");
  req
    .checkBody("approval_date")
    .notEmpty()
    .withMessage("approval_date Required!");
  req
    .checkBody("use_permission")
    .notEmpty()
    .withMessage("use_permission Required!");
  req
    .checkBody("violations_observed")
    .notEmpty()
    .withMessage("violations_observed Required!");
  req
    .checkBody("local_byelaws")
    .notEmpty()
    .withMessage("local_byelaws Required!");
  req
    .checkBody("situated_floor_no")
    .notEmpty()
    .withMessage("situated_floor_no Required!");
  req
    .checkBody("quality_fittings")
    .notEmpty()
    .withMessage("quality_fittings Required!");
  req
    .checkBody("Internal_composition")
    .notEmpty()
    .withMessage("Internal_composition Required!");
  req
    .checkBody("flooring")
    .notEmpty()
    .withMessage("flooring Required!");
  req
    .checkBody("built_up_area")
    .notEmpty()
    .withMessage("built_up_area Required!");
  req
    .checkBody("loading_sbua")
    .notEmpty()
    .withMessage("loading_sbua Required!");
  req
    .checkBody("approved_rates")
    .notEmpty()
    .withMessage("approved_rates Required!");
  req
    .checkBody("final_value")
    .notEmpty()
    .withMessage("final_value Required!");
  req
    .checkBody("recommended_rate")
    .notEmpty()
    .withMessage("recommended_rate Required!");
  req
    .checkBody("land_area")
    .notEmpty()
    .withMessage("land_area Required!");
  req
    .checkBody("current_rates_for_land")
    .notEmpty()
    .withMessage("current_rates_for_land Required!");
  req
    .checkBody("land_recommended_rate")
    .notEmpty()
    .withMessage("land_recommended_rate Required!");
  req
    .checkBody("land_value")
    .notEmpty()
    .withMessage("land_value Required!");
  req
    .checkBody("bua_premises")
    .notEmpty()
    .withMessage("bua_premises Required!");
  req
    .checkBody("construction_cost")
    .notEmpty()
    .withMessage("construction_cost Required!");
  req
    .checkBody("total_value_const")
    .notEmpty()
    .withMessage("total_value_const Required!");
  req
    .checkBody("depreciate_value_const")
    .notEmpty()
    .withMessage("depreciate_value_const Required!");
  req
    .checkBody("total_valuation")
    .notEmpty()
    .withMessage("total_valuation Required!");
  req
    .checkBody("say")
    .notEmpty()
    .withMessage("say Required!");
  req.getValidationResult().then(result => {
    var errors = result.array();
    console.log(errors);
    if (result.array().length > 0) {
      res.render("valuation_report_b", {
        error: errors[0].msg,
        serial_no: req.body.serial_no,
        sBankName: req.body.sBankName,
        sBranchName: req.body.sBranchName,
        name_of_applicant: req.body.name_of_applicant,
        property_address: req.body.property_address,
        area_details: req.body.area_details,
        proposed_buyer: req.body.proposed_buyer,
        loan_application_number: req.body.loan_application_number,
        legal_address_of_property: req.body.legal_address_of_property,
        date_of_inspection: req.body.date_of_inspection,
        purpose_of_inspection: req.body.purpose_of_inspection,
        ward_no: req.body.ward_no,
        vicinity: req.body.vicinity,
        plot_site: req.body.plot_site,
        pro_type: req.body.pro_type,
        conditions_of_road: req.body.conditions_of_road,
        nearest_landmark: req.body.nearest_landmark,
        local_transport: req.body.local_transport,
        local_transport_other: req.body.local_transport_other,
        pro_identified_through: req.body.pro_identified_through,
        usage_entire_pro: req.body.usage_entire_pro,
        additional_amenities: req.body.additional_amenities,
        type_of_premises: req.body.type_of_premises,
        occupied_by: req.body.occupied_by,
        name_of_occupant: req.body.name_of_occupant,
        property_rented: req.body.property_rented,
        if_relative: req.body.if_relative,
        if_rented: req.body.if_rented,
        north_boundarie: req.body.north_boundarie,
        south_boundarie: req.body.south_boundarie,
        east_boundarie: req.body.east_boundarie,
        west_boundarie: req.body.west_boundarie,
        type_of_structure: req.body.type_of_structure,
        no_of_floors: req.body.no_of_floors,
        no_of_dwelling: req.body.no_of_dwelling,
        no_of_wings: req.body.no_of_wings,
        no_of_lifts: req.body.no_of_lifts,
        construction_year: req.body.construction_year,
        future_life: req.body.future_life,
        age_of_property: req.body.age_of_property,
        situated_floor_no: req.body.situated_floor_no,
        quality_fittings: req.body.quality_fittings,
        Internal_composition: req.body.Internal_composition,
        flooring: req.body.flooring,
        beam_column: req.body.beam_column,
        appearance_maintenance: req.body.appearance_maintenance,
        common_remarks: req.body.common_remarks,
        finishing: req.body.finishing,
        roofing_terracing: req.body.roofing_terracing,
        no_of_lift: req.body.no_of_lift,
        constr_per_approved: req.body.constr_per_approved,
        details_approved_plans: req.body.details_approved_plans,
        approval_date: req.body.approval_date,
        use_permission: req.body.use_permission,
        violations_observed: req.body.violations_observed,
        local_byelaws: req.body.local_byelaws,
        built_up_area: req.body.built_up_area,
        loading_sbua: req.body.loading_sbua,
        approved_rates: req.body.approved_rates,
        final_value: req.body.final_value,
        recommended_rate: req.body.recommended_rate,
        land_area: req.body.land_area,
        current_rates_for_land: req.body.current_rates_for_land,
        land_recommended_rate: req.body.land_recommended_rate,
        land_value: req.body.land_value,
        bua_premises: req.body.bua_premises,
        construction_cost: req.body.construction_cost,
        total_value_const: req.body.total_value_const,
        depreciate_value_const: req.body.depreciate_value_const,
        total_valuation: req.body.total_valuation,
        say: req.body.say,
        remarks: req.body.remarks
      });
    } else {
      ReportB.findOne({
        _id: req.params.id
      }).then(function(reportB) {
        console.log(reportB);
        reportB.serial_no = req.body.serial_no;
        reportB.bankInfo.sBankName = req.body.sBankName;
        reportB.bankInfo.sBranchName = req.body.sBranchName;
        reportB.generalDetals.name_of_applicant = req.body.name_of_applicant;
        reportB.generalDetals.property_address = req.body.property_address;
        reportB.generalDetals.area_details = req.body.area_details;
        reportB.generalDetals.proposed_buyer = req.body.proposed_buyer;
        reportB.generalDetals.loan_application_number =
          req.body.loan_application_number;
        reportB.generalDetals.legal_address_of_property =
          req.body.legal_address_of_property;
        reportB.generalDetals.date_of_inspection = req.body.date_of_inspection;
        reportB.generalDetals.purpose_of_inspection =
          req.body.purpose_of_inspection;
        reportB.surrLocDetails.ward_no = req.body.ward_no;
        reportB.surrLocDetails.vicinity = req.body.vicinity;
        reportB.surrLocDetails.plot_site = req.body.plot_site;
        reportB.surrLocDetails.pro_type = req.body.pro_type;
        reportB.surrLocDetails.conditions_of_road = req.body.conditions_of_road;
        reportB.surrLocDetails.nearest_landmark = req.body.nearest_landmark;
        reportB.surrLocDetails.local_transport = req.body.local_transport;
        reportB.surrLocDetails.local_transport_other =
          req.body.local_transport_other;
        reportB.surrLocDetails.pro_identified_through =
          req.body.pro_identified_through;
        reportB.propDetails.usage_entire_pro = req.body.usage_entire_pro;
        reportB.propDetails.additional_amenities =
          req.body.additional_amenities;
        reportB.propDetails.type_of_premises = req.body.type_of_premises;
        reportB.propDetails.occupied_by = req.body.occupied_by;
        reportB.propDetails.name_of_occupant = req.body.name_of_occupant;
        reportB.propDetails.property_rented = req.body.property_rented;
        reportB.propDetails.if_relative = req.body.if_relative;
        reportB.propDetails.if_rented = req.body.if_rented;
        reportB.propDetails.Boundry.north_boundarie = req.body.north_boundarie;
        reportB.propDetails.Boundry.south_boundarie = req.body.south_boundarie;
        reportB.propDetails.Boundry.east_boundarie = req.body.east_boundarie;
        reportB.propDetails.Boundry.west_boundarie = req.body.west_boundarie;
        reportB.structDetail.type_of_structure = req.body.type_of_structure;
        reportB.structDetail.no_of_floors = req.body.no_of_floors;
        reportB.structDetail.no_of_dwelling = req.body.no_of_dwelling;
        reportB.structDetail.no_of_wings = req.body.no_of_wings;
        reportB.structDetail.no_of_lifts = req.body.no_of_lifts;
        reportB.structDetail.construction_year = req.body.construction_year;
        reportB.structDetail.future_life = req.body.future_life;
        reportB.structDetail.age_of_property = req.body.age_of_property;
        reportB.structDetail.situated_floor_no = req.body.situated_floor_no;
        reportB.structDetail.quality_fittings = req.body.quality_fittings;
        reportB.structDetail.Internal_composition =
          req.body.Internal_composition;
        reportB.structDetail.flooring = req.body.flooring;
        reportB.QualityConst.AExteriors.beam_column = req.body.beam_column;
        reportB.QualityConst.AExteriors.appearance_maintenance =
          req.body.appearance_maintenance;
        reportB.QualityConst.AExteriors.common_remarks =
          req.body.common_remarks;
        reportB.QualityConst.BExteriors.finishing = req.body.finishing;
        reportB.QualityConst.BExteriors.roofing_terracing =
          req.body.roofing_terracing;
        reportB.QualityConst.BExteriors.no_of_lift = req.body.no_of_lift;
        reportB.planApproval.constr_per_approved = req.body.constr_per_approved;
        reportB.planApproval.details_approved_plans =
          req.body.details_approved_plans;
        reportB.planApproval.approval_date = req.body.approval_date;
        reportB.planApproval.use_permission = req.body.use_permission;
        reportB.planApproval.violations_observed = req.body.violations_observed;
        reportB.planApproval.local_byelaws = req.body.local_byelaws;
        reportB.valuation.built_up_area = req.body.built_up_area;
        reportB.valuation.loading_sbua = req.body.loading_sbua;
        reportB.valuation.approved_rates = req.body.approved_rates;
        reportB.valuation.final_value = req.body.final_value;
        reportB.valuation.recommended_rate = req.body.recommended_rate;
        reportB.valuation.land_area = req.body.land_area;
        reportB.valuation.current_rates_for_land =
          req.body.current_rates_for_land;
        reportB.valuation.land_recommended_rate =
          req.body.land_recommended_rate;
        reportB.valuation.land_value = req.body.land_value;
        reportB.valuation.bua_premises = req.body.bua_premises;
        reportB.valuation.construction_cost = req.body.construction_cost;
        reportB.valuation.total_value_const = req.body.total_value_const;
        reportB.valuation.depreciate_value_const =
          req.body.depreciate_value_const;
        reportB.valuation.total_valuation = req.body.total_valuation;
        reportB.valuation.say = req.body.say;
        reportB.remarks = req.body.remarks;

        reportB.save((error, success) => {
          if (error) {
            return res.status(500).jsonp(error);
          } else {
            req.flash("success_msg", "Valuation Report B Updated");
            res.redirect("/valuation_report_b_view");
          }
        });
      });
      // documents_provided = req.body.documents_provided.split(',');
    }
  });
};

module.exports = {
  valuation_report_b,
  DeleteB,
  editB
};
