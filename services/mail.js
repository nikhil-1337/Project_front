const nodemailer = require("nodemailer");
const constant = require("../config/constant");
const debug = require("debug")("http");


let transpoter = nodemailer.createTransport({
  host: constant.emailHost,
  port: constant.emailPort,
  secure: constant.emailSecure,
  auth: {
    user: constant.emailUserHost,
    pass: constant.emailUserPassword
  }
});

const sendMail = (body, cb) => {
  let mailBody = {
    from: constant.emailSender,
    to: body.emailReciver,
    subject: body.subject,
    text: body.text
    // html    : ''
  };
  transpoter.sendMail(mailBody, cb);
};

module.exports = {
  sendMail
};
