const User = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const constant = require("../config/constant");
const mail = require("./mail");
const cli = require("../config/cli").console;
const LocalStrategy = require("passport-local").Strategy;
const crypto = require("crypto");
const async = require("async");
const debug = require("debug")("http");


let adminLogin = (req, res) => {
  let body = {
    sEmail: req.body.sEmail,
    eStatus: {
      $ne: "n"
    },
    eUserType: "admin"
  };
  req.checkBody("sEmail", "Please enter valid email.").notEmpty();
  req.checkBody("sEmail", "Please enter valid email.").isEmail();
  req.checkBody("sPassword", "Please enter valid password.").notEmpty();
  req.getValidationResult().then(function(result) {
    if (!result.isEmpty()) {
      res.status(400).json({
        message: "Please enter valid value.",
        data: result.mapped()
      });
    } else {
      UserModel.findOne(body, (err, user) => {
        if (err) return res.status(501).send(err);
        if (user) {
          if (bcrypt.compareSync(req.body.sPassword, user.sPassword)) {
            if (user.eStatus === "y") {
              let sDeviceToken = bcrypt.hashSync(
                req.body.sEmail + Math.random() + Date.now()
              );
              let payload = {
                token: sDeviceToken
              };
              let token = jwt.sign(payload, constant.jwtSecretKey);
              user.aJwtToken.pop();
              user.aJwtToken.push({
                sDeviceToken: sDeviceToken,
                sPushToken: "",
                sDeviceType: req.body.sDeviceType,
                eLogin: "y"
              });
              user.save(function(err) {
                if (err) {
                  res.status(500).send(err);
                } else {
                  res.status(200).json({
                    token: token
                  });
                }
              });
            } else {
              res.status(400).json({
                message: "User deactivated by admin, Contact to admin."
              });
            }
          } else {
            res.status(401).json({
              message: "Unauthorized"
            });
          }
        } else {
          res.status(401).json({
            message: "Unauthorized"
          });
        }
      });
    }
  });
};

// Testing purpose only
let CreateAdmin = (req, res) => {
  req.checkBody("sEmail", "Please enter valid email.").notEmpty();
  req.checkBody("sEmail", "Please enter valid email.").isEmail();
  req.checkBody("sPassword", "Please enter valid password").notEmpty();
  req.getValidationResult().then(function(result) {
    if (!result.isEmpty()) {
      res.status(403).json({
        field: result.mapped(),
        message: "Please enter valid value."
      });
    } else {
      let sDeviceToken = bcrypt.hashSync(
        req.body.sEmail + Math.random() + Date.now()
      );
      let sVerificationToken = bcrypt.hashSync(
        req.body.sEmail + req.body.sPassword + Math.random() + Date.now()
      );
      let query = {
        sEmail: req.body.sEmail
      };
      let option = {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
      };
      let body = {
        sFirstName: req.body.sFirstName,
        sLastName: req.body.sLastName,
        sEmail: req.body.sEmail,
        sPassword: bcrypt.hashSync(req.body.sPassword),
        eUserType: "admin",
        aJwtToken: {
          sDeviceToken: sDeviceToken,
          sPushToken: "",
          sDeviceType: req.body.sDeviceType,
          eLogin: "y"
        },
        sVerificationToken: sVerificationToken
      };
      UserModel.findOneAndUpdate(query, body, option, (error, adminDetail) => {
        if (error) {
          throw error;
        } else {
          if (error) {
            res.status(400).json({
              message: "Please enter proper value."
            });
          } else {
            res.status(200).json({
              message: "Admin created successfully."
            });
          }
        }
      });
    }
  });
};

const emailVerification = (req, res) => {
  console.log(req.params.token);
  req.getValidationResult().then(function(result) {
    if (!result.isEmpty()) {
      res.status(400).json({
        message: "Please enter valid value.",
        data: result.mapped()
      });
    } else {
      let body = {
        sVerificationToken: req.params.token
      };
      User.findOne(body, (err, user) => {
        if (err) return res.status(501).send(err);
        if (user) {
          user.eStatus = "y";
          user.save(err => {
            if (err) {
              res.status(500).json({
                message: "Internal server err"
              });
            } else {
              req.flash("success_msg", "Thank you! Now you may login.");
              cli.green("Thank you! Now you may login.");
              res.redirect("/login");
            }
          });
        } else {
          req.flash("error_msg", "User Not Found");
          cli.red("User Not Found");
          res.redirect("/login");
        }
      });
    }
  });
};

let forgotPass = (req, res) => {
  async.waterfall(
    [
      function(done) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString("hex");
          done(err, token);
        });
      },
      function(token, done) {
        User.findOne(
          {
            sEmail: req.body.sEmail
          },
          function(err, user) {
            if (!user) {
              req.flash(
                "error_msg",
                "No account with that email address exists."
              );
              return res.redirect("forgot");
            }
            user.resetPasswordToken = token;
            user.save(function(err) {
              done(err, token, user);
            });
          }
        );
      },
      function(token, user, done) {
        try {
          mail.sendMail(
            {
              emailReciver: req.body.sEmail,
              subject: "Password Reset",
              text:
                "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
                "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
                "http://" +
                req.headers.host +
                "/reset/" +
                token +
                "\n\n" +
                "If you did not request this, please ignore this email and your password will remain unchanged.\n"
            },
            function(err, info) {
              if (err) {
                console.log(err);
              } else {
                console.log(info);
              }
            }
          );
        } catch (error) {
          cli.red("Unsucessfull: created user email notification.");
          throw error;
        }
        req.flash(
          "success_msg",
          "An e-mail has been sent to " +
            user.sEmail +
            " with further instructions."
        );
        res.redirect("login");
        cli.green("Please Check Your Mail");
      }
    ],
    function(err) {
      if (err) return next(err);
      res.redirect("forgot");
    }
  );
};

let resetTokenVerification = (req, res) => {
  User.findOne(
    {
      resetPasswordToken: req.params.token
    },
    function(err, user) {
      if (!user) {
        req.flash("error", "Password reset token is invalid.");
        return res.redirect("forgot");
      }
      res.render("users/reset", {
        user: req.user
      });
    }
  );
};

let resetPass = (req, res) => {
  async.waterfall(
    [
      function(done) {
        User.findOne(
          {
            resetPasswordToken: req.params.token
          },
          function(err, user) {
            if (!user) {
              req.flash(
                "error_msg",
                "Password reset token is invalid or has expired."
              );
              cli.green("Success! Your password has been changed.");
              return res.redirect("back");
            }
            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(req.body.sNewPassword, salt, (err, hash) => {
                if (err) throw err;
                req.body.sNewPassword = hash;
                user.sPassword = hash;
                user.save(function(err) {
                  console.log("Saved info");
                  //res.redirect('login')
                });
              });
            });
            try {
              mail.sendMail(
                {
                  emailReciver: user.sEmail,
                  subject: "Your password has been changed",
                  text:
                    "Hello,\n\n" +
                    "This is a confirmation that the password for your account " +
                    user.email +
                    " has just been changed.\n"
                },
                function(err, info) {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log(info);
                  }
                }
              );
            } catch (error) {
              cli.red("Unsucessfull: created user email notification.");
              throw error;
            }
            req.flash(
              "success_msg",
              "Success! Your password has been changed."
            );
            cli.green("Success! Your password has been changed.");
            res.redirect("/login");
          }
        );
      }
    ],
    function(err) {
      res.redirect("login");
    }
  );
};

let logout = (req, res) => {
  req.logout();
  req.flash("success_msg", "You are logged out");
  res.redirect("login");
};

module.exports = {
  adminLogin,
  CreateAdmin,
  emailVerification,
  forgotPass,
  resetPass,
  resetTokenVerification,
  logout
};
