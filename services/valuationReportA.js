const User = require("../models/user");
const mongoose = require("mongoose");
const Bank = require("../models/bank_model");
const cli = require("../config/cli").console;
var bodyParser = require("body-parser");
const { check, validationResult } = require("express-validator/check");
const RepoartA = require("../models/valuation_report_model");
const debug = require("debug")("http");


mongoose.Promise = Promise;
const multer = require("multer");
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/uploads/a");
  },
  filename: function(req, file, cb) {
    var datetimestamp = Date.now() + Math.random();
    cb(
      null,
      file.fieldname +
        "-" +
        datetimestamp +
        "." +
        file.originalname.split(".")[file.originalname.split(".").length - 1]
    );
  }
});

var upload = multer({
  storage: storage
}).fields([
  {
    name: "files",
    maxCount: 1
  },
  {
    name: "google_map2",
    maxCount: 1
  },
  {
    name: "google_map",
    maxCount: 1
  }
]);

const valuation_report_a = (req, res, next) => {

  upload(req, res, function(err) {
    req
      .checkBody("sBankName")
      .notEmpty()
      .withMessage("Please Enter Bank Name");
    req
      .checkBody("sBranchName")
      .notEmpty()
      .withMessage("Branch name required.");

    req
      .checkBody("name_of_customer")
      .notEmpty()
      .withMessage("Customer name required");
    req
      .checkBody("purpose_of_valuation")
      .notEmpty()
      .withMessage("Purpose of valuation is required");
    req
      .checkBody("location")
      .notEmpty()
      .withMessage("Location is required");
    req
      .checkBody("date_of_valuation")
      .notEmpty()
      .withMessage("Please enter date of valuation");
    req
      .checkBody("estimated_market_value")
      .notEmpty()
      .withMessage("Enter estimated marcket value");
    req
      .checkBody("documents_provided")
      .notEmpty()
      .withMessage("Chooose provided Document.");
    req
      .checkBody("revenue_survey_no")
      .notEmpty()
      .withMessage("Revenur survey no. is required");
    req
      .checkBody("fp_no")
      .notEmpty()
      .withMessage("Fp. no. is required");
    req
      .checkBody("tps_no")
      .notEmpty()
      .withMessage("tps_no is required");
    req
      .checkBody("op_no")
      .notEmpty()
      .withMessage("op_no is required");
    req
      .checkBody("flat_no")
      .notEmpty()
      .withMessage("flat_no is required");
    req
      .checkBody("mouje")
      .notEmpty()
      .withMessage("mouje is required");
    req
      .checkBody("society_name")
      .notEmpty()
      .withMessage("society name is required");
    req
      .checkBody("landmark")
      .notEmpty()
      .withMessage("landmark is required");
    req
      .checkBody("taluka")
      .notEmpty()
      .withMessage("taluka is required");
    req
      .checkBody("district")
      .notEmpty()
      .withMessage("District is required");
    req
      .checkBody("local_transport")
      .notEmpty()
      .withMessage("Local transport is required");
    req
      .checkBody("distance_to_city_center")
      .notEmpty()
      .withMessage("Distance from City Center is required");
    req
      .checkBody("class_of_locality")
      .notEmpty()
      .withMessage("Class of locality is required");
    req
      .checkBody("quality_of_infrastructure")
      .notEmpty()
      .withMessage("Quality of infrastructure is required");
    req
      .checkBody("north_boundarie")
      .notEmpty()
      .withMessage("North Boundry is required");
    req
      .checkBody("south_boundarie")
      .notEmpty()
      .withMessage("south Boundry is required");
    req
      .checkBody("east_boundarie")
      .notEmpty()
      .withMessage("east Boundry is required");
    req
      .checkBody("west_boundarie")
      .notEmpty()
      .withMessage("west Boundry is required");
    req
      .checkBody("boundaries_in_doc")
      .notEmpty()
      .withMessage("Boundaries at Site match required");
    req
      .checkBody("topographical_condition")
      .notEmpty()
      .withMessage("Level of land with topographical conditions is required");
    req
      .checkBody("status_of_land")
      .notEmpty()
      .withMessage("Status of land is required");
    req
      .checkBody("type_of_property")
      .notEmpty()
      .withMessage("Type of property is required");
    req
      .checkBody("approved_usage")
      .notEmpty()
      .withMessage("Approved usage of property is required");
    req
      .checkBody("actual_usage")
      .notEmpty()
      .withMessage("Actual usage of the property is required");
    req
      .checkBody("type_of_structure")
      .notEmpty()
      .withMessage("Type of Structure is required");
    req
      .checkBody("no_of_floors")
      .notEmpty()
      .withMessage("No. of Floors is required");
    req
      .checkBody("occupancy_details")
      .notEmpty()
      .withMessage("Occupancy Details is required");
    req
      .checkBody("property_connection")
      .notEmpty()
      .withMessage("Property Connection is required");
    req
      .checkBody("proximity")
      .notEmpty()
      .withMessage("Proximity is required");
    req
      .checkBody("development_area")
      .notEmpty()
      .withMessage("Development of surrounding area is required");
    req
      .checkBody("approval_details")
      .notEmpty()
      .withMessage("Approval Details is required");
    req
      .checkBody("construction_details")
      .notEmpty()
      .withMessage("Construction Details is required");
    req
      .checkBody("usages_of_pro")
      .notEmpty()
      .withMessage("Usage of property is required");
    req
      .checkBody("building_plan")
      .notEmpty()
      .withMessage("Building plan is required");
    req
      .checkBody("side_margin")
      .notEmpty()
      .withMessage("Available Side Margin is required");
    req
      .checkBody("front_margin")
      .notEmpty()
      .withMessage("Front Margin is required");
    req
      .checkBody("quality_of_construction")
      .notEmpty()
      .withMessage("Quality of construction is required");
    req
      .checkBody("maintenance_of_property")
      .notEmpty()
      .withMessage("Maintenance of the property is required");
    req
      .checkBody("current_life_structure")
      .notEmpty()
      .withMessage("Current life of the structure is required");
    req
      .checkBody("estimated_life_structure")
      .notEmpty()
      .withMessage("Estimated future life of the structure is required");
    req
      .checkBody("stage_of_const")
      .notEmpty()
      .withMessage("Stage of Construction is required");
    req
      .checkBody("work_complited")
      .notEmpty()
      .withMessage("Work Completed is required");
    req
      .checkBody("disbursement_recommended")
      .notEmpty()
      .withMessage("Disbursement Recommended is required");
    req
      .checkBody("date_of_pro_visit")
      .notEmpty()
      .withMessage("Date of Property Visit is required");
    req
      .checkBody("government_rates")
      .notEmpty()
      .withMessage("Valuation as per Government reckoner rate is required");
    req
      .checkBody("valuation_property")
      .notEmpty()
      .withMessage("Distress valuation of the property is required");
    req
      .checkBody("rent_value")
      .notEmpty()
      .withMessage("Rental value per month is required");
    req
      .checkBody("recommended_rate_flat")
      .notEmpty()
      .withMessage("Recommended Rate of the Flat is required");
    req
      .checkBody("value_of_flat")
      .notEmpty()
      .withMessage("Value of Flat is required");
    req
      .checkBody("value_of_terrace")
      .notEmpty()
      .withMessage("Value of Terrace is required");
    req
      .checkBody("total_value")
      .notEmpty()
      .withMessage("Total Value is required");
    req
      .checkBody("say")
      .notEmpty()
      .withMessage("Say is required");
    req.getValidationResult().then(result => {
      var errors = result.array();
      console.log(errors);
      if (result.array().length > 0) {
        res.render("valuation_report_a", {
          error: errors[0].msg,
          serial_no: req.body.serial_no,
          sBankName: req.body.sBankName,
          sBranchName: req.body.sBranchName,
          customer_id: req.body.customer_id,
          name_of_customer: req.body.name_of_customer,
          purpose_of_valuation: req.body.purpose_of_valuation,
          location: req.body.location,
          date_of_valuation: req.body.date_of_valuation,
          estimated_market_value: req.body.estimated_market_value,
          documents_provided: req.body.documents_provided,
          documents_provided_other: req.body.documents_provided_other,
          revenue_survey_no: req.body.revenue_survey_no,
          fp_no: req.body.fp_no,
          tps_no: req.body.tps_no,
          op_no: req.body.op_no,
          flat_no: req.body.flat_no,
          mouje: req.body.mouje,
          society_name: req.body.society_name,
          landmark: req.body.landmark,
          taluka: req.body.taluka,
          district: req.body.district,
          local_transport: req.body.local_transport,
          local_transport_other: req.body.local_transport_other,
          distance_to_city_center: req.body.distance_to_city_center,
          class_of_locality: req.body.class_of_locality,
          quality_of_infrastructure: req.body.quality_of_infrastructure,
          north_boundarie: req.body.north_boundarie,
          south_boundarie: req.body.south_boundarie,
          east_boundarie: req.body.east_boundarie,
          west_boundarie: req.body.west_boundarie,
          boundaries_in_doc: req.body.boundaries_in_doc,
          topographical_condition: req.body.topographical_condition,
          status_of_land: req.body.status_of_land,
          type_of_property: req.body.type_of_property,
          approved_usage: req.body.approved_usage,
          actual_usage: req.body.actual_usage,
          type_of_structure: req.body.type_of_structure,
          no_of_floors: req.body.no_of_floors,
          occupancy_details: req.body.occupancy_details,
          property_connection: req.body.property_connection,
          proximity: req.body.proximity,
          development_area: req.body.development_area,
          approval_details: req.body.approval_details,
          construction_details: req.body.construction_details,
          usages_of_pro: req.body.usages_of_pro,
          building_plan: req.body.building_plan,
          side_margin: req.body.side_margin,
          front_margin: req.body.front_margin,
          quality_of_construction: req.body.quality_of_construction,
          maintenance_of_property: req.body.maintenance_of_property,
          current_life_structure: req.body.current_life_structure,
          estimated_life_structure: req.body.estimated_life_structure,
          stage_of_const: req.body.stage_of_const,
          work_complited: req.body.work_complited,
          disbursement_recommended: req.body.disbursement_recommended,
          date_of_pro_visit: req.body.date_of_pro_visit,
          government_rates: req.body.government_rates,
          valuation_property: req.body.valuation_property,
          rent_value: req.body.rent_value,
          recommended_rate_flat: req.body.recommended_rate_flat,
          value_of_flat: req.body.value_of_flat,
          value_of_terrace: req.body.value_of_terrace,
          total_value: req.body.total_value,
          say: req.body.say
        });
      } else {
        // documents_provided = req.body.documents_provided.split(',');
        let files;
        console.log(req.files);
        var keys = Object.keys(req.files);
        for (var elem in keys) {
          var fieldname = req.files[keys[elem]][0].fieldname;
          if (fieldname == "files") {
            req.body.files = req.files[keys[elem]][0].filename;
          } else if (fieldname == "google_map") {
            req.body.google_map = req.files[keys[elem]][0].filename;
          } else if (fieldname == 'google_map2') {
            req.body.google_map2 = req.files[keys[elem]][0].filename;
          }
        }

        const body = {
          images:req.body.files,
          google_map:req.body.google_map,
          google_map2:req.body.google_map2,
          serial_no: req.body.serial_no,
          bankInfo: {
            sBankName: req.body.sBankName,
            sBranchName: req.body.sBranchName
          },
          generalInfo: {
            customer_id: req.body.customer_id,
            name_of_customer: req.body.name_of_customer,
            purpose_of_valuation: req.body.purpose_of_valuation,
            location: req.body.location,
            date_of_valuation: req.body.date_of_valuation,
            estimated_market_value: req.body.estimated_market_value,
            documents_provided: req.body.documents_provided,
            documents_provided_other: req.body.documents_provided_other
          },
          propertyDetails: {
            revenue_survey_no: req.body.revenue_survey_no,
            fp_no: req.body.fp_no,
            tps_no: req.body.tps_no,
            op_no: req.body.op_no,
            flat_no: req.body.flat_no,
            mouje: req.body.mouje,
            society_name: req.body.society_name,
            landmark: req.body.landmark,
            taluka: req.body.taluka,
            district: req.body.district,
            local_transport: req.body.local_transport,
            local_transport_other: req.body.local_transport_other,
            distance_to_city_center: req.body.distance_to_city_center,
            class_of_locality: req.body.class_of_locality,
            quality_of_infrastructure: req.body.quality_of_infrastructure,
            Boundry: {
              north_boundarie: req.body.north_boundarie,
              south_boundarie: req.body.south_boundarie,
              east_boundarie: req.body.east_boundarie,
              west_boundarie: req.body.west_boundarie
            },
            boundaries_in_doc: req.body.boundaries_in_doc,
            topographical_condition: req.body.topographical_condition,
            status_of_land: req.body.status_of_land,
            type_of_property: req.body.type_of_property,
            approved_usage: req.body.approved_usage,
            actual_usage: req.body.actual_usage,
            type_of_structure: req.body.type_of_structure,
            no_of_floors: req.body.no_of_floors,
            occupancy_details: req.body.occupancy_details,
            property_connection: req.body.property_connection,
            proximity: req.body.proximity,
            development_area: req.body.development_area,
            approval_details: req.body.approval_details,
            construction_details: req.body.construction_details,
            usages_of_pro: req.body.usages_of_pro,
            building_plan: req.body.building_plan,
            side_margin: req.body.side_margin,
            front_margin: req.body.front_margin,
            quality_of_construction: req.body.quality_of_construction,
            maintenance_of_property: req.body.maintenance_of_property,
            current_life_structure: req.body.current_life_structure,
            estimated_life_structure: req.body.estimated_life_structure
          },
          RecomValuation: {
            stage_of_const: req.body.stage_of_const,
            work_complited: req.body.work_complited,
            disbursement_recommended: req.body.disbursement_recommended,
            date_of_pro_visit: req.body.date_of_pro_visit,
            government_rates: req.body.government_rates,
            valuation_property: req.body.valuation_property,
            rent_value: req.body.rent_value,
            recommended_rate_flat: req.body.recommended_rate_flat,

            values: {
              value_of_flat: req.body.value_of_flat,
              value_of_terrace: req.body.value_of_terrace,
              total_value: req.body.total_value,
              say: req.body.say
            }
          }
        };


        const newData = new RepoartA(body);
        newData.save((error, success) => {
          if (error) {
            return res.status(500).jsonp(error);
          } else {
            req.flash("success_msg", "Valuation Report A added");
            res.redirect("/");
          }
        });
      }
    });
  });
};

const editA = (req, res, next) => {
  upload(req, res, function(err) {
 req
   .checkBody("sBankName")
   .notEmpty()
   .withMessage("Please Enter Bank Name");
 req
   .checkBody("sBranchName")
   .notEmpty()
   .withMessage("Branch name required.");

 req
   .checkBody("name_of_customer")
   .notEmpty()
   .withMessage("Customer name required");
 req
   .checkBody("purpose_of_valuation")
   .notEmpty()
   .withMessage("Purpose of valuation is required");
 req
   .checkBody("location")
   .notEmpty()
   .withMessage("Location is required");
 req
   .checkBody("date_of_valuation")
   .notEmpty()
   .withMessage("Please enter date of valuation");
 req
   .checkBody("estimated_market_value")
   .notEmpty()
   .withMessage("Enter estimated marcket value");
 req
   .checkBody("documents_provided")
   .notEmpty()
   .withMessage("Chooose provided Document.");
 req
   .checkBody("revenue_survey_no")
   .notEmpty()
   .withMessage("Revenur survey no. is required");
 req
   .checkBody("fp_no")
   .notEmpty()
   .withMessage("Fp. no. is required");
 req
   .checkBody("tps_no")
   .notEmpty()
   .withMessage("tps_no is required");
 req
   .checkBody("op_no")
   .notEmpty()
   .withMessage("op_no is required");
 req
   .checkBody("flat_no")
   .notEmpty()
   .withMessage("flat_no is required");
 req
   .checkBody("mouje")
   .notEmpty()
   .withMessage("mouje is required");
 req
   .checkBody("society_name")
   .notEmpty()
   .withMessage("society name is required");
 req
   .checkBody("landmark")
   .notEmpty()
   .withMessage("landmark is required");
 req
   .checkBody("taluka")
   .notEmpty()
   .withMessage("taluka is required");
 req
   .checkBody("district")
   .notEmpty()
   .withMessage("District is required");
 req
   .checkBody("local_transport")
   .notEmpty()
   .withMessage("Local transport is required");
 req
   .checkBody("distance_to_city_center")
   .notEmpty()
   .withMessage("Distance from City Center is required");
 req
   .checkBody("class_of_locality")
   .notEmpty()
   .withMessage("Class of locality is required");
 req
   .checkBody("quality_of_infrastructure")
   .notEmpty()
   .withMessage("Quality of infrastructure is required");
 req
   .checkBody("north_boundarie")
   .notEmpty()
   .withMessage("North Boundry is required");
 req
   .checkBody("south_boundarie")
   .notEmpty()
   .withMessage("south Boundry is required");
 req
   .checkBody("east_boundarie")
   .notEmpty()
   .withMessage("east Boundry is required");
 req
   .checkBody("west_boundarie")
   .notEmpty()
   .withMessage("west Boundry is required");
 req
   .checkBody("boundaries_in_doc")
   .notEmpty()
   .withMessage("Boundaries at Site match required");
 req
   .checkBody("topographical_condition")
   .notEmpty()
   .withMessage("Level of land with topographical conditions is required");
 req
   .checkBody("status_of_land")
   .notEmpty()
   .withMessage("Status of land is required");
 req
   .checkBody("type_of_property")
   .notEmpty()
   .withMessage("Type of property is required");
 req
   .checkBody("approved_usage")
   .notEmpty()
   .withMessage("Approved usage of property is required");
 req
   .checkBody("actual_usage")
   .notEmpty()
   .withMessage("Actual usage of the property is required");
 req
   .checkBody("type_of_structure")
   .notEmpty()
   .withMessage("Type of Structure is required");
 req
   .checkBody("no_of_floors")
   .notEmpty()
   .withMessage("No. of Floors is required");
 req
   .checkBody("occupancy_details")
   .notEmpty()
   .withMessage("Occupancy Details is required");
 req
   .checkBody("property_connection")
   .notEmpty()
   .withMessage("Property Connection is required");
 req
   .checkBody("proximity")
   .notEmpty()
   .withMessage("Proximity is required");
 req
   .checkBody("development_area")
   .notEmpty()
   .withMessage("Development of surrounding area is required");
 req
   .checkBody("approval_details")
   .notEmpty()
   .withMessage("Approval Details is required");
 req
   .checkBody("construction_details")
   .notEmpty()
   .withMessage("Construction Details is required");
 req
   .checkBody("usages_of_pro")
   .notEmpty()
   .withMessage("Usage of property is required");
 req
   .checkBody("building_plan")
   .notEmpty()
   .withMessage("Building plan is required");
 req
   .checkBody("side_margin")
   .notEmpty()
   .withMessage("Available Side Margin is required");
 req
   .checkBody("front_margin")
   .notEmpty()
   .withMessage("Front Margin is required");
 req
   .checkBody("quality_of_construction")
   .notEmpty()
   .withMessage("Quality of construction is required");
 req
   .checkBody("maintenance_of_property")
   .notEmpty()
   .withMessage("Maintenance of the property is required");
 req
   .checkBody("current_life_structure")
   .notEmpty()
   .withMessage("Current life of the structure is required");
 req
   .checkBody("estimated_life_structure")
   .notEmpty()
   .withMessage("Estimated future life of the structure is required");
 req
   .checkBody("stage_of_const")
   .notEmpty()
   .withMessage("Stage of Construction is required");
 req
   .checkBody("work_complited")
   .notEmpty()
   .withMessage("Work Completed is required");
 req
   .checkBody("disbursement_recommended")
   .notEmpty()
   .withMessage("Disbursement Recommended is required");
 req
   .checkBody("date_of_pro_visit")
   .notEmpty()
   .withMessage("Date of Property Visit is required");
 req
   .checkBody("government_rates")
   .notEmpty()
   .withMessage("Valuation as per Government reckoner rate is required");
 req
   .checkBody("valuation_property")
   .notEmpty()
   .withMessage("Distress valuation of the property is required");
 req
   .checkBody("rent_value")
   .notEmpty()
   .withMessage("Rental value per month is required");
 req
   .checkBody("recommended_rate_flat")
   .notEmpty()
   .withMessage("Recommended Rate of the Flat is required");
 req
   .checkBody("value_of_flat")
   .notEmpty()
   .withMessage("Value of Flat is required");
 req
   .checkBody("value_of_terrace")
   .notEmpty()
   .withMessage("Value of Terrace is required");
 req
   .checkBody("total_value")
   .notEmpty()
   .withMessage("Total Value is required");
 req
   .checkBody("say")
   .notEmpty()
   .withMessage("Say is required");
 req.getValidationResult().then(result => {
   var errors = result.array();
   console.log(errors);
   if (result.array().length > 0) {
     res.render("valuation_report_a", {
       error: errors[0].msg,
       sBankName: req.body.sBankName,
       sBranchName: req.body.sBranchName,
       serial_no: req.body.serial_no,
       customer_id: req.body.customer_id,
       name_of_customer: req.body.name_of_customer,
       purpose_of_valuation: req.body.purpose_of_valuation,
       location: req.body.location,
       date_of_valuation: req.body.date_of_valuation,
       estimated_market_value: req.body.estimated_market_value,
       documents_provided: req.body.documents_provided,
       documents_provided_other: req.body.documents_provided_other,
       revenue_survey_no: req.body.revenue_survey_no,
       fp_no: req.body.fp_no,
       tps_no: req.body.tps_no,
       op_no: req.body.op_no,
       flat_no: req.body.flat_no,
       mouje: req.body.mouje,
       society_name: req.body.society_name,
       landmark: req.body.landmark,
       taluka: req.body.taluka,
       district: req.body.district,
       local_transport: req.body.local_transport,
       local_transport_other: req.body.local_transport_other,
       distance_to_city_center: req.body.distance_to_city_center,
       class_of_locality: req.body.class_of_locality,
       quality_of_infrastructure: req.body.quality_of_infrastructure,
       north_boundarie: req.body.north_boundarie,
       south_boundarie: req.body.south_boundarie,
       east_boundarie: req.body.east_boundarie,
       west_boundarie: req.body.west_boundarie,
       boundaries_in_doc: req.body.boundaries_in_doc,
       topographical_condition: req.body.topographical_condition,
       status_of_land: req.body.status_of_land,
       type_of_property: req.body.type_of_property,
       approved_usage: req.body.approved_usage,
       actual_usage: req.body.actual_usage,
       type_of_structure: req.body.type_of_structure,
       no_of_floors: req.body.no_of_floors,
       occupancy_details: req.body.occupancy_details,
       property_connection: req.body.property_connection,
       proximity: req.body.proximity,
       development_area: req.body.development_area,
       approval_details: req.body.approval_details,
       construction_details: req.body.construction_details,
       usages_of_pro: req.body.usages_of_pro,
       building_plan: req.body.building_plan,
       side_margin: req.body.side_margin,
       front_margin: req.body.front_margin,
       quality_of_construction: req.body.quality_of_construction,
       maintenance_of_property: req.body.maintenance_of_property,
       current_life_structure: req.body.current_life_structure,
       estimated_life_structure: req.body.estimated_life_structure,
       stage_of_const: req.body.stage_of_const,
       work_complited: req.body.work_complited,
       disbursement_recommended: req.body.disbursement_recommended,
       date_of_pro_visit: req.body.date_of_pro_visit,
       government_rates: req.body.government_rates,
       valuation_property: req.body.valuation_property,
       rent_value: req.body.rent_value,
       recommended_rate_flat: req.body.recommended_rate_flat,
       value_of_flat: req.body.value_of_flat,
       value_of_terrace: req.body.value_of_terrace,
       total_value: req.body.total_value,
       say: req.body.say
     });
   } else {
             let files;
        console.log(req.files);

     RepoartA.findOne({ _id: req.params.id }).then(function(reportA) {
        var keys = Object.keys(req.files);
        for (var elem in keys) {
          var fieldname = req.files[keys[elem]][0].fieldname;
          if (fieldname == "files") {
            req.body.files = req.files[keys[elem]][0].filename;
          } else if (fieldname == "google_map") {
            req.body.google_map = req.files[keys[elem]][0].filename;
          } else if (fieldname == "google_map2") {
            req.body.google_map2 = req.files[keys[elem]][0].filename;
          }
        }
       console.log(reportA);
       reportA.google_map = req.body.google_map
       reportA.google_map2 = req.body.google_map2
       reportA.images= req.body.files
       reportA.bankInfo.sBankName = req.body.sBankName;
       reportA.bankInfo.sBranchName = req.body.sBranchName;
       reportA.generalInfo.customer_id = req.body.customer_id;
       reportA.generalInfo.name_of_customer = req.body.name_of_customer;
       reportA.generalInfo.purpose_of_valuation = req.body.purpose_of_valuation;
       reportA.generalInfo.location = req.body.location;
       reportA.generalInfo.date_of_valuation = req.body.date_of_valuation;
       reportA.generalInfo.estimated_market_value = req.body.estimated_market_value;
       reportA.generalInfo.documents_provided = req.body.documents_provided;
       reportA.generalInfo.documents_provided_other = req.body.documents_provided_other;
       reportA.propertyDetails.revenue_survey_no = req.body.revenue_survey_no;
       reportA.propertyDetails.fp_no = req.body.fp_no;
       reportA.propertyDetails.tps_no = req.body.tps_no;
       reportA.propertyDetails.op_no = req.body.op_no;
       reportA.propertyDetails.flat_no = req.body.flat_no;
       reportA.propertyDetails.mouje = req.body.mouje;
       reportA.propertyDetails.society_name = req.body.society_name;
       reportA.propertyDetails.landmark = req.body.landmark;
       reportA.propertyDetails.taluka = req.body.taluka;
       reportA.propertyDetails.district = req.body.district;
       reportA.propertyDetails.local_transport = req.body.local_transport;
       reportA.propertyDetails.local_transport_other = req.body.local_transport_other;
       reportA.propertyDetails.distance_to_city_center = req.body.distance_to_city_center;
       reportA.propertyDetails.class_of_locality = req.body.class_of_locality;
       reportA.propertyDetails.quality_of_infrastructure = req.body.quality_of_infrastructure;
       reportA.propertyDetails.Boundry.north_boundarie = req.body.north_boundarie;
       reportA.propertyDetails.Boundry.south_boundarie = req.body.south_boundarie;
       reportA.propertyDetails.Boundry.east_boundarie = req.body.east_boundarie;
       reportA.propertyDetails.Boundry.west_boundarie = req.body.west_boundarie;
       reportA.propertyDetails.boundaries_in_doc = req.body.boundaries_in_doc;
       reportA.propertyDetails.topographical_condition = req.body.topographical_condition;
       reportA.propertyDetails.status_of_land = req.body.status_of_land;
       reportA.propertyDetails.type_of_property = req.body.type_of_property;
       reportA.propertyDetails.approved_usage = req.body.approved_usage;
       reportA.propertyDetails.actual_usage = req.body.actual_usage;
       reportA.propertyDetails.type_of_structure = req.body.type_of_structure;
       reportA.propertyDetails.no_of_floors = req.body.no_of_floors;
       reportA.propertyDetails.occupancy_details = req.body.occupancy_details;
       reportA.propertyDetails.property_connection = req.body.property_connection;
       reportA.propertyDetails.proximity = req.body.proximity;
       reportA.propertyDetails.development_area = req.body.development_area;
       reportA.propertyDetails.approval_details = req.body.approval_details;
       reportA.propertyDetails.construction_details = req.body.construction_details;
       reportA.propertyDetails.usages_of_pro = req.body.usages_of_pro;
       reportA.propertyDetails.building_plan = req.body.building_plan;
       reportA.propertyDetails.side_margin = req.body.side_margin;
       reportA.propertyDetails.front_margin = req.body.front_margin;
       reportA.propertyDetails.quality_of_construction = req.body.quality_of_construction;
       reportA.propertyDetails.maintenance_of_property = req.body.maintenance_of_property;
       reportA.propertyDetails.current_life_structure = req.body.current_life_structure;
       reportA.propertyDetails.estimated_life_structure = req.body.estimated_life_structure;
       reportA.RecomValuation.stage_of_const = req.body.stage_of_const;
       reportA.RecomValuation.work_complited = req.body.work_complited;
       reportA.RecomValuation.disbursement_recommended = req.body.disbursement_recommended;
       reportA.RecomValuation.date_of_pro_visit = req.body.date_of_pro_visit;
       reportA.RecomValuation.government_rates = req.body.government_rates;
       reportA.RecomValuation.valuation_property = req.body.valuation_property;
       reportA.RecomValuation.rent_value = req.body.rent_value;
       reportA.RecomValuation.recommended_rate_flat = req.body.recommended_rate_flat;
       reportA.RecomValuation.values.value_of_flat = req.body.value_of_flat;
       reportA.RecomValuation.values.value_of_terrace = req.body.value_of_terrace;
       reportA.RecomValuation.values.total_value = req.body.total_value;
       reportA.RecomValuation.values.say = req.body.say;

       reportA.save((error, success) => {
         if (error) {
           return res.status(500).jsonp(error);
         } else {
           req.flash("success_msg", "Valuation Report A Updated");
           res.redirect("/valuation_report_a_view");
         }
       });
     });
     // documents_provided = req.body.documents_provided.split(',');
   }
 });
  })
 
};

const DeleteA = (req, res, next) => {
  console.log("Bank_data:id_delete", req.params.id);
  RepoartA.remove({
    _id: req.params.id
  }).then(() => {
    req.flash("success_msg", "Valuation Report A removed");
    res.redirect("/valuation_report_a_view");
  });
};
module.exports = {
  valuation_report_a,
  DeleteA,
  editA
};
