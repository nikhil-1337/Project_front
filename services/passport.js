const LocalStrategy = require("passport-local").Strategy;
const mongoose = require("mongoose");
const cli = require("../config/cli").console;
const User = require("../models/user");
const bcrypt = require("bcryptjs");
const debug = require("debug")("http");


module.exports = function(passport) {
  passport.use(
    new LocalStrategy(
      {
        usernameField: "email"
      },
      (email, password, done) => {
        //Match user
        cli.blue(email);
        cli.blue(password);
        User.findOne({
          sEmail: email
        }).then(user => {
          if (!user) {
            cli.red("No user Found");
            return done(null, false, {
              message: "No User Found"
            });
          }
          if (user.eStatus === "n") {
            cli.red("sorry You must verify your email");
            return done(null, false, {
              message: "Sorry, you must Verify Your Email first"
            });
          }
          cli.green("isMatch");
          bcrypt.compare(password, user.sPassword, (err, isMatch) => {
            if (err) cli.red("Err", err);
            if (isMatch) {
              cli.blue(isMatch);
              return done(null, user);
            } else {
              cli.red("Enter Correct password");
              return done(null, false, {
                message: "Enter Correct Password!"
              });
            }
          });
        });
      }
    )
  );

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};
