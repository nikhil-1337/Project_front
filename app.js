const express = require('express');
const path = require('path');
const logger = require("morgan")
const cookieParser = require("cookie-parser");
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const constant = require("./config/constant");
const session = require("express-session");
const flash = require("connect-flash");
const expressValidator = require("express-validator");
const cli = require("./config/cli").console;

const app = express();

var methodOverride = require("method-override");
//Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//load routers
const index = require('./routes/index');
require("./services/passport")(passport);
const db = require("./config/db");
mongoose.set("debug", true);

app.use(cors());
//Handlebard Engine
app.set('view engine', 'handlebars');
app.engine('handlebars', exphbs({
  defaultLayout: 'main'
}));
app.use(express.static(__dirname + "/public"));
app.use("/public", express.static(__dirname + "/public"));


app.use(logger("dev"));
app.use(cookieParser());
app.use(expressValidator());
app.use(methodOverride("_method"));

app.use(
  session({
    secret: "qaswedfrtghyu",
    resave: true,
    saveUninitialized: true
  })
);

//Passport Session and Init
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  next();
});

app.use('/', index)
  //  app.use("/api/admin", admin);
// override with POST having ?_method=PUT


const port = constant.PORT || process.env.PORT;
app.listen(port, () => {
  console.log(`Magic Started on port:${port}`);
});
module.exports = app;
