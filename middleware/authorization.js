
const isAdminOnly = (req, res, next) => {

  if (req.isAuthenticated() && req.user.eUserType) {
    return next();
  } else {
    req.flash("error_msg", "Permission Denied");
    res.redirect("back");
  }
};
 const ensureAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash("error_msg", "You need to be Login First!");
    res.redirect("/login");
  }
  const isNotAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
      res.redirect("/");
    } else {
      return next();
    }
  };

module.exports = {
  isAdminOnly,
  ensureAuthenticated,
  isNotAuthenticated
};
